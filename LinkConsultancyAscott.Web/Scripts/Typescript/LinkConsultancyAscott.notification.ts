﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/kendo/kendo.all.d.ts"/>
/// <reference path="LinkConsultancyAscott.common.view.models.ts"/>
module LinkConsultancyAscottNotification
{
    export interface ILinkConsultancyAscottNotificationManager {
        processNotificationResponse: (notificationResponse: CommonViewModels.NotificationResponse) => void;
        displayNotificationResponse: (notification: kendo.ui.Notification, notificationResponse: CommonViewModels.NotificationResponse) => void;

        getNotification: () => kendo.ui.Notification;

        showInfo: (message: string) => void ;
        showError: (message: string) => void;
        showSuccess: (message: string) => void;
        showWarning: (message: string) => void;
    }

    export class LinkConsultancyAscottNotificationManager implements ILinkConsultancyAscottNotificationManager
    {
        getNotification(): kendo.ui.Notification {
            var element: any = window.parent.document.getElementById('notification');
            var jelm = $(element);
            var notification = jelm.data("kendoNotification");
            return notification;
        }

        processNotificationResponse(notificationResponse: CommonViewModels.NotificationResponse) : void {
            var notification = LinkConsultancyAscottNotification.manager.getNotification();

           LinkConsultancyAscottNotification.manager.displayNotificationResponse(notification, notificationResponse);
        }

        displayNotificationResponse(notification: kendo.ui.Notification, notificationResponse: CommonViewModels.NotificationResponse) : void {
            switch (notificationResponse.ResponseStatus) {
                case "Info":
                    this.showInfoNotification(notification, notificationResponse.ResponseMessage);
                    break;
                case "Error":
                    this.showErrorNotification(notification, notificationResponse.ResponseMessage);
                    break;
                case "Warning":
                    this.showWarningNotification(notification, notificationResponse.ResponseMessage);
                    break;
                case "Success":
                    this.showSuccessNotification(notification, notificationResponse.ResponseMessage);
                    break;
                default:
                    this.showErrorNotification(notification, notificationResponse.ResponseMessage);
                    break;
            }
        }

        showInfo(message: string): void {
            () => {
                this.showInfoNotification(this.getNotification(), message);
            }
        }

        showError(message: string): void {
            () => {
                this.showErrorNotification(this.getNotification(), message);
            }
        }

        showSuccess(message: string)  : void{
            () => {
                this.showSuccessNotification(this.getNotification(), message);
            }
        }

        showWarning(message: string) : void {
            () => {
                this.showWarningNotification(this.getNotification(), message);
            }
        }

        showInfoNotification(notification: kendo.ui.Notification, message: string) : void {
            notification.info(message);
        }

        showErrorNotification(notification: kendo.ui.Notification, message: string) : void {
            notification.error(message);
        }

        showSuccessNotification(notification: kendo.ui.Notification, message: string)  : void{
            notification.success(message);
        }

        showWarningNotification(notification: kendo.ui.Notification, message: string) : void {
            notification.warning(message);
        }

    }

    export var manager : ILinkConsultancyAscottNotificationManager = new LinkConsultancyAscottNotificationManager();
}