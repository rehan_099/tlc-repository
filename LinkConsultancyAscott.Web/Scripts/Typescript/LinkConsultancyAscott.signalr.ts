﻿"use strict";

interface SignalR {
    linkConsultancyAscottHub: ILinkConsultancyAscottHubProxy
}

interface ILinkConsultancyAscottHubProxy {
    client: ILinkConsultancyAscottClient;
}

interface ILinkConsultancyAscottClient {
    aclUpdated: () => void;
}

class LinkConsultancyAscottClient implements ILinkConsultancyAscottClient {
    aclUpdated(): void {
        infoLog("Entered SignalR: LinkConsultancyAscott.aclUpdated");
        LinkConsultancyAscott.application.refreshGrids();
        infoLog("Exited  SignalR: LinkConsultancyAscott.aclUpdated");
    }
    
}
