﻿// This variable is set in _Layout.cshtml via some Razor code.
// It needs to work with both IISExpress when running in VS AND when
// the app is deployed to a server IIS instance.
declare var appRootUrl: string; 