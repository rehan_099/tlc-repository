﻿"use strict";
module CommonViewModels {
    export enum ZippedDataPackType {
        CustomerRidDataPack = 1,
        SupplierResponseZippedDataPack = 2,
        WindChillPackZippedDataPack = 3
    }

    export enum DataPackStatusType {
        DataPackUploaded = 1,

        RidReviewOpen = 2,

        RidReviewClosed = 3,

        RidWithSupplier = 4,

        DataPackClosed = 5
    }

    export enum CommentItemType {
        DataPack = 1,
        DocumentationType = 2,
        Document = 3
    }

    export enum UploadResponseType {
        CustomerResponse = 1,
        SupplierResponse = 2
    }

    export class FilterListViewModel {
        Id: string;
        Name: string;
    }
    
    export class NotificationResponse {
        ResponseStatus: string;
        ResponseMessage: string;
    }

    export class NotificationResponseWithModel {
        ResponseStatus: string;
        ResponseMessage: string;
        ViewModel: any;
    }
}
