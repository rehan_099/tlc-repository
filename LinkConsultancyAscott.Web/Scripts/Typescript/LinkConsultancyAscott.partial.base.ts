﻿
"use strict";
module LinkConsultancyAscott {
    export interface IBase {
        refreshAjaxGrid: (gridName: string) => void;
        dataSourceErrorHandler: (e: kendo.data.DataSourceErrorEvent) => void;
        onRequestEndRefreshGrid: (e: kendo.data.DataSourceRequestEndEvent) => void;
    }

    export class Base implements IBase {
       refreshAjaxGrid(gridName: string) {
                // Ensure the incoming grid name is a valid jQuery Identifier
                if (gridName.length > 0) {
                    if (gridName[0] !== "#") {
                        gridName = "#" + gridName;
                    }
                }
                // Also need to update any other dependent grids
                var grid : kendo.ui.Grid = <kendo.ui.Grid>$(gridName).data('kendoGrid');

                if (grid) {
                    var dataSource = grid.dataSource;
                    dataSource.read();
                }
        }

        dataSourceErrorHandler(e: kendo.data.DataSourceErrorEvent): void {
                if (e.errors) {
                    var message = "Errors:\n";
                    $.each(e.errors, (key, value) => {
                        if ('errors' in value) {
                            $.each(value.errors, function () {
                                message += this + "\n";
                            });
                        }
                    });
                    alert(message);
                }
            }

        onRequestEndRefreshGrid(e: kendo.data.DataSourceRequestEndEvent): void {
                if (e.type === "update" || e.type === "destroy" || e.type === "create") {
                    e.sender.read();
                }
        }
    }

    export interface IPartialBase extends IBase {
        loadPartial: () => void;
        onPartialLoaded: (response, status, xhr ) => void;
        deleteKendoControls: () => void;   
    }
    
    export class PartialBase extends Base implements IPartialBase {
        // Ideally the next 3 methods would be abstract but Typescript doesn't support that yet
        loadPartial(): void {}

        onPartialLoaded(response, status, xhr): void {}

        deleteKendoControls(): void { }
    }
    
}