﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/kendo/kendo.all.d.ts"/>

function getBaseUrl() {
    return "";
}


function PadZeroes(num, size) {
    var s = num + "";
    while (s.length < size) s = `0${s}`;
    return s;
}

function infoLog(msg) {
    console.info(getCurrentDateTime() + msg);
}

function errorLog(msg) {
    console.error(getCurrentDateTime() + msg);
}

function GenerateDateTimeStringWithMs(date) {
    return PadZeroes(date.getDate(), 2) + "/" + PadZeroes(date.getMonth() + 1, 2) + "/" + date.getFullYear() + " " + PadZeroes(date.getHours(), 2) + ":" + PadZeroes(date.getMinutes(), 2) + ":" + PadZeroes(date.getSeconds(), 2) + "." + PadZeroes(date.getMilliseconds(), 3);
}

function getCurrentDateTime() {
    var now = new Date(Date.now());
    return GenerateDateTimeStringWithMs(now) + ' - ';
}

function InfoLog(msg) {
    console.info(getCurrentDateTime() + msg);
}

function ErrorLog(msg) {
    console.error(getCurrentDateTime() + msg);
}

function getString(key, value) {
    return "&" + key + "=" + value;
}

function loadViewerPartial(viewModel: any) {
    var parameterString = "";

    $.each(viewModel.Parameters, (key, value) => {
        parameterString += getString(key, value);
    });

    var reportQuery = "reportName=" + viewModel.ReportName + parameterString;

    $('#partialViewer').load(getBaseUrl() + '/ReportViewer/Viewer?' + reportQuery);
}
function error_handler(e) {
    if (e.errors) {
        var message = "Errors:\n";
        $.each(e.errors, (key, value) => {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        alert(message);
    }
}

function kendo_error_handler(gridName) {
    return e => {
        if (e.errors) {
            var grid = $('#' + gridName).data("kendoGrid");
            grid.one("dataBinding", ev => {
                ev.preventDefault();
                var message = "Errors:\n";
                $.each(e.errors, (key, value) => {
                    if ('errors' in value) {
                        $.each(value.errors, function () {
                            message += this + "\n";
                        });
                    }
                });
                $("#errorContainer").text(message);
            });
           
        }
        else {
            $("#errorContainer").text("");
        }
    }
}

function onRequestEndRefreshGrid(e) {
    if (e.type === "update" || e.type === "destroy" || e.type === "create") {
        this.read();
    }
}


function onRequestEndElement(e) {
    if (e.type === "update" || e.type === "destroy" || e.type === "create") {
        this.read();
    }
}

function refreshAjaxGrid(sGridName) {
    // Also need to update any other dependent grids
    var grid = $(sGridName).data('kendoGrid');

    if (grid) {
        var dataSource = grid.dataSource;
        dataSource.read();
    }
}

function mainMenuSelect(e) {
    var selectedItem = $(e.item).children(".k-link").text();
    console.log("Selected: " + selectedItem);
    
}

function onAttachmentSelect(e) {
    // Array with information about the uploaded files
    var files = e.files;

    // Check the extension of each file and abort the upload if it is not .jpg
    $.each(files, function () {
        if (!(this.extension.toLowerCase() === ".jpg" ||
            this.extension.toLowerCase() === ".bmp" ||
            this.extension.toLowerCase() === ".gif" ||
            this.extension.toLowerCase() === ".jpeg" ||
            this.extension.toLowerCase() === ".txt" ||
            this.extension.toLowerCase() === ".doc" ||
            this.extension.toLowerCase() === ".docx" ||
            this.extension.toLowerCase() === ".xls" ||
            this.extension.toLowerCase() === ".xlsx" ||
            this.extension.toLowerCase() === ".pdf" ||
            this.extension.toLowerCase() === ".png")) {
            alert("Only .gif, .jpg, .jpeg, .png, .txt, .doc, .docx, .xls, .xlsx and .pdf files can be uploaded");
            e.preventDefault();
            return false;
        } else {
            return false;
        }
    });
}
