"use strict";

module LinkConsultancyAscott {
    export interface IApplication {
        loadDataUrl: (element: any) => void;
        loadUrl: (url: string, onLoaded: any) => void;
        refreshAjaxGrid: (gridName: string) => void;
        listViewModeItemTemplate: (e: any) => string;
        refreshGrids: () => void;
        gridResizeHandler: (gridName: string) => void;

        setActiveNavLink: (linkName: string) => void;

        onUploadClick: (e: kendo.ui.ButtonClickEvent) => void;
        onIpAddressFileSelect: (e: kendo.ui.UploadSelectEvent) => void;
        onIpAddressFileUpload: (e: kendo.ui.UploadUploadEvent) => void;
        onIpAddressFileCancel: (e: kendo.ui.UploadCancelEvent) => void;
        onIpAddressFileClear: (e: kendo.ui.UploadClearEvent) => void;
        onIpAddressFileComplete: (e: kendo.ui.UploadUploadEvent) => void;
        onIpAddressFileError: (e: kendo.ui.UploadErrorEvent) => void;
        onIpAddressFileProgress: (e: kendo.ui.UploadProgressEvent) => void;
        onIpAddressFileRemove: (e: kendo.ui.UploadRemoveEvent) => void;
        onIpAddressFileSuccess: (e: kendo.ui.UploadSuccessEvent) => void;
    }

    export class Application implements IApplication {
        setActiveNavLink(linkName: string): void {

        }

        // Array of Grids to be updated
        private updateGridList : string[] = ["AclListGrid", "AclHistoryGrid", "CataleyaAclGrid"];

        refreshGrids() {
            for (let gridName of this.updateGridList) {
                infoLog("Refreshing Grid [" + gridName + "]");
                LinkConsultancyAscott.application.refreshAjaxGrid("#" + gridName);
            }
        }

        refreshAjaxGrid(gridName: string) {
            // Ensure the incoming grid name is a valid jQuery Identifier
            if (gridName.length > 0) {
                if (gridName[0] !== "#") {
                    gridName = "#" + gridName;
                }
            }
            // Also need to update any other dependent grids
            var grid: kendo.ui.Grid = <kendo.ui.Grid>$(gridName).data('kendoGrid');

            if (grid) {
                var dataSource = grid.dataSource;
                dataSource.read();
            }
        }

        loadDataUrl(element: any): void {
            var jelement = $(element);
            var url = jelement.data("request-url") + "?_=" + new Date().getTime();
            var anchor = jelement.data("anchor").toString();

           this.loadUrl(url, null);

            window.location.hash = anchor;
        }

        loadUrl(url: string, onLoaded: any): void {
            $('#mainBodyContainer').hide("fast", () => {
                $('#mainBodyContainer').load(url, '', () => {
                    $('#mainBodyContainer').show("fast", () => {
                        $('#load').fadeOut("fast");
                        // After all this is complete call any spec
                        if (onLoaded) {
                            onLoaded();
                        }
                    });
                });
            });

            $('#load').remove();
            $('#wrapper').append('<span id="load">LOADING...</span>');
            $('#load').fadeIn('normal');
        }

      
         listViewModeItemTemplate(e: any): string {
            if (e.field === "all") {
                //handle the check-all checkbox template
                return "<li class='k-item'><label class='k-label'><input class='k-check-all' type='checkbox' value='Select All'>Select All</label></li>";
            } else {
                //handle the other checkboxes
                return "<li class='k-item'><label class='k-label'><input type='checkbox' value='#=RequirementType.Name#'>#=RequirementType.Name#</label></li>";
            }
        }

        gridResizeHandler(gridName: string): void {
          infoLog("Entered gridResizeHandler");
          var gridElement = $(gridName);

          function resizeGrid() {
            var windowHeight = $(window).height();
            var headerHeight = $('#header-wrapper').outerHeight(true);
            var contentHeight = windowHeight - headerHeight - 80;

            
            gridElement.css("height", contentHeight + 'px');

              var grid = gridElement.data("kendoGrid");
              if (grid) {
                  grid.resize();
              }
            
          }

          $(window).resize(() => {
              resizeGrid();
          });

            resizeGrid();
        }

        onUploadClick(e: kendo.ui.ButtonClickEvent): void {
            infoLog("onUploadClick");

            var uploadWindow = $("#UploadIpAddressFileWindow").data("kendoWindow");

            uploadWindow.refresh(null);
 
           uploadWindow.open();
           uploadWindow.center();
        }

        onIpAddressFileSelect(e: kendo.ui.UploadSelectEvent): void {
            // Array with information about the uploaded files
            var files = e.files;

            // Check the extension of each file and abort the upload if it is not .jpg
            $.each(files, (index, file) => {
                if (!(file.extension.toLowerCase() === ".txt")) {
                    alert("Only .txt files can be uploaded");
                    e.preventDefault();
                    return false;
                } else {
                    return false;
                }
            });
        }

        onIpAddressFileUpload(e: kendo.ui.UploadUploadEvent): void {
            var listType = $("#ListType").val();


            if (listType) {
                e.data = {
                    ListType: listType
                };
            } else {
                e.preventDefault();
            }
        }

        onIpAddressFileCancel(e: kendo.ui.UploadCancelEvent): void {
            infoLog("Entered onIpAddressFileCancel");
        }

        onIpAddressFileClear(e: kendo.ui.UploadClearEvent): void {
            infoLog("Entered onIpAddressFileClear");
        }

        onIpAddressFileComplete(e: kendo.ui.UploadUploadEvent): void {
            infoLog("Entered onIpAddressFileComplete");
            // Close the window and redraw the grid
            var uploadWindow = $("#UploadIpAddressFileWindow").data("kendoWindow");
            refreshAjaxGrid("#AclListGrid");
            uploadWindow.close();
        }

        onIpAddressFileError(e: kendo.ui.UploadErrorEvent): void {
            infoLog("Entered onIpAddressFileError");
        }

        onIpAddressFileProgress(e: kendo.ui.UploadProgressEvent): void {
            infoLog("Entered onIpAddressFileProgress");
        }

        onIpAddressFileRemove(e: kendo.ui.UploadRemoveEvent): void {
            infoLog("Entered onIpAddressFileRemove");
        }

        onIpAddressFileSuccess(e: kendo.ui.UploadSuccessEvent): void {
            infoLog("Entered onIpAddressFileSuccess");
        }

    }

    // Instantiate an instance of the class
    export var application: IApplication = new Application();
}