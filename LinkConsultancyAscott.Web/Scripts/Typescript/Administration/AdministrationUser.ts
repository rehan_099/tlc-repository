﻿/// <reference path="../../typings/kendo/kendo.all.d.ts"/>
"use strict";
module AdministrationUser
{
    export interface IAdministrationUserApplication extends AdministrationBase.IAdministrationBase
    {
        onAdministrationUserRowBound: (e: any) => void;
        onClearUserCache: () => void;
    }

    export class AdministrationUserApplication extends AdministrationBase
        .AdministrationBaseApplication implements IAdministrationUserApplication
    {
        /**
         * Called when the user grid is data bound, and sets the icon of the custom toolbar button(s). 
         * @param {any} e The event args.
         */
        onAdministrationUserRowBound(e): void
        {
            // Customise the toolbar button icon!
            var span = $(".k-grid-administrationUserClearUserCacheButton").find("span");
            span.append("<i class='far fa-users'></i>");
            span.append("<i class='far fa-refresh text-danger'></i>");
        }
       
        public onUserGridEdit = (e: any): void => {
            // Change the appearance of the default buttons
           // setUpdateAndCancelButtons();
        }
        
        /**
         * Calls the server to clear the cache of user identities.
         * Displays a success or failure notification to the user when the call returns.
         */
        onClearUserCache()
        {
            var ajaxUrl = appRootUrl + "/Administration/AdministrationUser/ClearUserCache";
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                url: ajaxUrl,
                success: data =>
                {
                    // Tell user it worked! :-)
                   // OutputMessageServiceManager.outputMessageService.writeMessageLine(data, MessageType.Success, true);
                },
                error: (jqXhr, error, errorThrown) =>
                {
                    // Tell user it didn't work! :-(
                  //  OutputMessageServiceManager.outputMessageService.writeMessageLine(jqXhr.responseText, MessageType.Error);
                }
            });
        }
    }

    // Instantiate an instance of the class
    export var administrationUserApplication: IAdministrationUserApplication = new AdministrationUserApplication();
}