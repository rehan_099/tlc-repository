﻿namespace LinkConsultancyAscott.Data
{
    using System;

    /// <summary>
    /// The look up base.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="LinkConsultancyAscott.Data.ILookUp{T}" />
    public abstract class BasicLookUpBase<T> : IBasicLookUp<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public T Id
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether visible.
        /// </summary>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        public int DisplayOrder
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>The created by.</value>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created at.
        /// </summary>
        /// <value>The created at.</value>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the last modified by.
        /// </summary>
        /// <value>The last modified by.</value>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the last modified at.
        /// </summary>
        /// <value>The last modified at.</value>
        public DateTime? LastModifiedAt { get; set; }

        /// <summary>
        /// Gets or sets the deleted by.
        /// </summary>
        /// <value>The deleted by.</value>
        public string DeletedBy { get; set; }

        /// <summary>
        /// Gets or sets the deleted at.
        /// </summary>
        /// <value>The deleted at.</value>
        public DateTime? DeletedAt { get; set; }
    }
}
