// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAcl.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IAcl type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAcl LookUp. 
    /// </summary>
    public interface IAcl : IAuditedEntity<int>
    {
    } 
}
