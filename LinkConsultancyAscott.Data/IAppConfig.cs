// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAppConfig.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IAppConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAppConfig LookUp. 
    /// </summary>
    public interface IAppConfig : ILookUp<int>
    {
    } 
}
