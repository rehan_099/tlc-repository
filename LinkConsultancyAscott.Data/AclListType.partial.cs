﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclListType.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AclListType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The AclListType Type.
    /// </summary>
    public partial class AclListType : IAclListType
    {
    }
}
