﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBasicLookUp.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Interface IBasicLookUp
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    using System;

    /// <summary>
    /// Interface IBasicLookUp
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="LinkConsultancyAscott.Data.IAuditedEntity{T}" />
    public interface IBasicLookUp<T> : IAuditedEntity<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the i display order.
        /// </summary>
        /// <value>The i display order.</value>
        int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [b visible].
        /// </summary>
        /// <value><c>true</c> if [b visible]; otherwise, <c>false</c>.</value>
        bool Visible { get; set; }
    }
}
