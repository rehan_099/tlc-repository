// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclSourceType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IEmailType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAclSourceType LookUp. 
    /// </summary>
    public interface IAclSourceType : ILookUp<int>
    {
    } 
}
