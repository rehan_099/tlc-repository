﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailType.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Email type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The Email Type.
    /// </summary>
    public partial class EmailType : IEmailType
    {
    }
}
