﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclSourceType.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AclSourceType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The AclSourceType Type.
    /// </summary>
    public partial class AclSourceType : IAclSourceType
    {
    }
}
