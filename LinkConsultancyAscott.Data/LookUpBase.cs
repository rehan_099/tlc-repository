﻿namespace LinkConsultancyAscott.Data
{
    using System;

    /// <summary>
    /// The look up base.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="LinkConsultancyAscott.Data.BasicLookUpBase{T}" />
    /// <seealso cref="LinkConsultancyAscott.Data.ILookUp{T}" />
    public abstract class LookUpBase<T> : BasicLookUpBase<T>, ILookUp<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
