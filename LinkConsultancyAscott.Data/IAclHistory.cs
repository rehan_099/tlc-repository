// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclHistory.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IAclHistory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAclHistory LookUp. 
    /// </summary>
    public interface IAclHistory : IAuditedEntity<int>
    {
    } 
}
