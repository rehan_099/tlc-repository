﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTemplate.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the EmailTemplate type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The EmailTemplate Type.
    /// </summary>
    public partial class EmailTemplate : IEmailTemplate
    {
    }
}
