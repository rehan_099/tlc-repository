﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteActivityLog.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The user data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The user data.
    /// </summary>
    public partial class SiteActivityLog : IDataEntityBase<long>
    {
    }
}
