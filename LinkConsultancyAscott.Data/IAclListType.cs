// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclListType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IEmailType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAclListType LookUp. 
    /// </summary>
    public interface IAclListType : ILookUp<int>
    {
    } 
}
