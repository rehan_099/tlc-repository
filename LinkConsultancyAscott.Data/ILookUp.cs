﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILookUp.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Interface ILookUp
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    using System;

    /// <summary>
    /// Interface ILookUp
    /// </summary>
    public interface ILookUp<T> : IBasicLookUp<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the name of the s.
        /// </summary>
        /// <value>The name of the s.</value>
        string Name { get; set; }
    }
}
