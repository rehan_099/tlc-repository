﻿
using System;

namespace LinkConsultancyAscott.Data
{
    public interface IDataEntityBase<T>
        where T :
        struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        T Id { get; set; }
    }
}
