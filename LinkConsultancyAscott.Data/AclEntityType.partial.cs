﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclEntityType.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AclEntityType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The AclEntityType Type.
    /// </summary>
    public partial class AclEntityType : IAclEntityType
    {
    }
}
