// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclEntityType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IEmailType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The IAclEntityType LookUp. 
    /// </summary>
    public interface IAclEntityType : ILookUp<int>
    {
    } 
}
