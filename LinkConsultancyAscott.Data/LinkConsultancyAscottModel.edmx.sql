
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/04/2018 18:35:11
-- Generated from EDMX file: E:\TLC\TLCPortal\Software\Main\LinkConsultancyAscott\LinkConsultancyAscott.Data\LinkConsultancyAscottModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [LinkConsultancyAscott];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[Acl].[FK_AclHistory_Acl]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[AclHistory] DROP CONSTRAINT [FK_AclHistory_Acl];
GO
IF OBJECT_ID(N'[Acl].[FK_AclHistory_AclHistoryType]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[AclHistory] DROP CONSTRAINT [FK_AclHistory_AclHistoryType];
GO
IF OBJECT_ID(N'[Acl].[FK_Acls_AclEntityType]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[Acls] DROP CONSTRAINT [FK_Acls_AclEntityType];
GO
IF OBJECT_ID(N'[Acl].[FK_Acls_AclListType]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[Acls] DROP CONSTRAINT [FK_Acls_AclListType];
GO
IF OBJECT_ID(N'[Acl].[FK_Acls_AclSourceType]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[Acls] DROP CONSTRAINT [FK_Acls_AclSourceType];
GO
IF OBJECT_ID(N'[Acl].[FK_Acls_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[Acls] DROP CONSTRAINT [FK_Acls_AspNetUsers];
GO
IF OBJECT_ID(N'[Acl].[FK_Acls_InterfaceType]', 'F') IS NOT NULL
    ALTER TABLE [Acl].[Acls] DROP CONSTRAINT [FK_Acls_InterfaceType];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserRoles_dbo_AspNetRoles_RoleId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo_AspNetUserRoles_dbo_AspNetRoles_RoleId];
GO
IF OBJECT_ID(N'[dbo].[FK_dbo_AspNetUserRoles_dbo_AspNetUsers_UserId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo_AspNetUserRoles_dbo_AspNetUsers_UserId];
GO
IF OBJECT_ID(N'[Configuration].[FK_EmailTemplateRole_EmailTemplate]', 'F') IS NOT NULL
    ALTER TABLE [Configuration].[EmailTemplateRole] DROP CONSTRAINT [FK_EmailTemplateRole_EmailTemplate];
GO
IF OBJECT_ID(N'[Configuration].[FK_EmailTemplates_EmailType]', 'F') IS NOT NULL
    ALTER TABLE [Configuration].[EmailTemplates] DROP CONSTRAINT [FK_EmailTemplates_EmailType];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[Acl].[AclEntityTypes]', 'U') IS NOT NULL
    DROP TABLE [Acl].[AclEntityTypes];
GO
IF OBJECT_ID(N'[Acl].[AclHistory]', 'U') IS NOT NULL
    DROP TABLE [Acl].[AclHistory];
GO
IF OBJECT_ID(N'[Acl].[AclHistoryTypes]', 'U') IS NOT NULL
    DROP TABLE [Acl].[AclHistoryTypes];
GO
IF OBJECT_ID(N'[Acl].[AclListTypes]', 'U') IS NOT NULL
    DROP TABLE [Acl].[AclListTypes];
GO
IF OBJECT_ID(N'[Acl].[Acls]', 'U') IS NOT NULL
    DROP TABLE [Acl].[Acls];
GO
IF OBJECT_ID(N'[Acl].[AclSourceTypes]', 'U') IS NOT NULL
    DROP TABLE [Acl].[AclSourceTypes];
GO
IF OBJECT_ID(N'[Acl].[InterfaceTypes]', 'U') IS NOT NULL
    DROP TABLE [Acl].[InterfaceTypes];
GO
IF OBJECT_ID(N'[Configuration].[AppConfig]', 'U') IS NOT NULL
    DROP TABLE [Configuration].[AppConfig];
GO
IF OBJECT_ID(N'[Configuration].[EmailTemplateRole]', 'U') IS NOT NULL
    DROP TABLE [Configuration].[EmailTemplateRole];
GO
IF OBJECT_ID(N'[Configuration].[EmailTemplates]', 'U') IS NOT NULL
    DROP TABLE [Configuration].[EmailTemplates];
GO
IF OBJECT_ID(N'[Configuration].[EmailTypes]', 'U') IS NOT NULL
    DROP TABLE [Configuration].[EmailTypes];
GO
IF OBJECT_ID(N'[dbo].[__RefactorLog]', 'U') IS NOT NULL
    DROP TABLE [dbo].[__RefactorLog];
GO
IF OBJECT_ID(N'[dbo].[AspNetRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetRoles];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserClaims]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserClaims];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserLogins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserLogins];
GO
IF OBJECT_ID(N'[dbo].[AspNetUserRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUserRoles];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[CataleyaInformation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CataleyaInformation];
GO
IF OBJECT_ID(N'[Logging].[AuditLog]', 'U') IS NOT NULL
    DROP TABLE [Logging].[AuditLog];
GO
IF OBJECT_ID(N'[Logging].[Log]', 'U') IS NOT NULL
    DROP TABLE [Logging].[Log];
GO
IF OBJECT_ID(N'[Logging].[SiteActivityLog]', 'U') IS NOT NULL
    DROP TABLE [Logging].[SiteActivityLog];
GO
IF OBJECT_ID(N'[LinkConsultancyAscottModelStoreContainer].[vw_AclHistory]', 'U') IS NOT NULL
    DROP TABLE [LinkConsultancyAscottModelStoreContainer].[vw_AclHistory];
GO
IF OBJECT_ID(N'[LinkConsultancyAscottModelStoreContainer].[vw_Acls]', 'U') IS NOT NULL
    DROP TABLE [LinkConsultancyAscottModelStoreContainer].[vw_Acls];
GO
IF OBJECT_ID(N'[LinkConsultancyAscottModelStoreContainer].[vw_UsersInRoles]', 'U') IS NOT NULL
    DROP TABLE [LinkConsultancyAscottModelStoreContainer].[vw_UsersInRoles];
GO
IF OBJECT_ID(N'[LinkConsultancyAscottModelStoreContainer].[vw_AuditLog]', 'U') IS NOT NULL
    DROP TABLE [LinkConsultancyAscottModelStoreContainer].[vw_AuditLog];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'EmailTemplateRoles'
CREATE TABLE [dbo].[EmailTemplateRoles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EmailTemplateId] int  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'EmailTemplates'
CREATE TABLE [dbo].[EmailTemplates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL,
    [EmailTypeId] int  NOT NULL,
    [EmailSubject] varchar(2500)  NULL,
    [EmailText] nvarchar(max)  NULL
);
GO

-- Creating table 'EmailTypes'
CREATE TABLE [dbo].[EmailTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'AuditLogs'
CREATE TABLE [dbo].[AuditLogs] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [EntityId] bigint  NOT NULL,
    [EntityName] varchar(200)  NOT NULL,
    [UserName] varchar(200)  NULL,
    [EventType] varchar(500)  NULL,
    [EventDescription] varchar(500)  NULL,
    [EventDetail] varchar(max)  NULL,
    [Source] varchar(500)  NULL
);
GO

-- Creating table 'Logs'
CREATE TABLE [dbo].[Logs] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Thread] varchar(255)  NOT NULL,
    [Level] varchar(50)  NOT NULL,
    [Logger] varchar(255)  NOT NULL,
    [Message] varchar(4000)  NOT NULL,
    [Exception] varchar(2000)  NULL
);
GO

-- Creating table 'SiteActivityLogs'
CREATE TABLE [dbo].[SiteActivityLogs] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [ActivityTimestamp] datetime  NOT NULL,
    [UserName] nvarchar(256)  NULL,
    [Controller] nvarchar(256)  NULL,
    [Action] nvarchar(256)  NULL,
    [IPAddress] nvarchar(256)  NOT NULL,
    [Browser] nvarchar(256)  NOT NULL,
    [ActionDuration] time  NULL,
    [RouteInfo] nvarchar(1000)  NULL,
    [IsMobileDevice] bit  NULL,
    [Platform] nvarchar(256)  NULL,
    [Domain] nvarchar(2000)  NULL,
    [Area] nvarchar(256)  NULL,
    [HttpVerb] varchar(256)  NULL,
    [ResponseStatus] varchar(256)  NULL,
    [ResponseStatusDescription] varchar(1000)  NULL,
    [ResponseData] nvarchar(max)  NULL,
    [QueryParameters] varchar(5000)  NULL,
    [RequestUrl] varchar(5000)  NULL
);
GO

-- Creating table 'vw_AuditLog'
CREATE TABLE [dbo].[vw_AuditLog] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [EntityId] bigint  NOT NULL,
    [EntityName] varchar(200)  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [UserName] varchar(200)  NULL,
    [EventType] varchar(500)  NULL,
    [EventDescription] varchar(500)  NULL,
    [Source] varchar(500)  NULL
);
GO

-- Creating table 'AclEntityTypes'
CREATE TABLE [dbo].[AclEntityTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'AclListTypes'
CREATE TABLE [dbo].[AclListTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'AclSourceTypes'
CREATE TABLE [dbo].[AclSourceTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'AspNetRoles'
CREATE TABLE [dbo].[AspNetRoles] (
    [Id] nvarchar(128)  NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'AspNetUserClaims'
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] nvarchar(128)  NOT NULL,
    [ClaimType] nvarchar(max)  NULL,
    [ClaimValue] nvarchar(max)  NULL
);
GO

-- Creating table 'AspNetUserLogins'
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] nvarchar(128)  NOT NULL,
    [ProviderKey] nvarchar(128)  NOT NULL,
    [UserId] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(128)  NOT NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [LockoutEndDateUtc] datetime  NULL,
    [LockoutEnabled] bit  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'vw_UsersInRoles'
CREATE TABLE [dbo].[vw_UsersInRoles] (
    [UserId] nvarchar(128)  NOT NULL,
    [UserName] nvarchar(256)  NOT NULL,
    [UserRoles] varchar(max)  NULL,
    [LastActivityDate] datetime  NULL,
    [SipInterfaces] nvarchar(max)  NULL,
    [MediaInterfaces] nvarchar(max)  NULL
);
GO

-- Creating table 'AclHistories'
CREATE TABLE [dbo].[AclHistories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(5000)  NOT NULL,
    [AclHistoryTypeId] int  NOT NULL,
    [AclId] int  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedAt] datetime  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'AclHistoryTypes'
CREATE TABLE [dbo].[AclHistoryTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'Acls'
CREATE TABLE [dbo].[Acls] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [Prefix] int  NOT NULL,
    [AclListTypeId] int  NOT NULL,
    [AclEntityTypeId] int  NOT NULL,
    [AclSourceTypeId] int  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL,
    [AspNetUserId] nvarchar(128)  NOT NULL,
    [InterfaceId] int  NOT NULL,
    [InterfaceName] varchar(250)  NOT NULL,
    [InterfaceTypeId] int  NOT NULL,
    [CataleyaSecurityAclObjId] int  NULL,
    [CataleyaSecurityAclNodeId] int  NULL
);
GO

-- Creating table 'InterfaceTypes'
CREATE TABLE [dbo].[InterfaceTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'C__RefactorLog'
CREATE TABLE [dbo].[C__RefactorLog] (
    [OperationKey] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'vw_AclHistory'
CREATE TABLE [dbo].[vw_AclHistory] (
    [AclHistoryId] int  NOT NULL,
    [AclHistoryName] varchar(5000)  NOT NULL,
    [AclHistoryCreatedAt] datetime  NOT NULL,
    [AclHistoryCreatedBy] varchar(250)  NOT NULL,
    [AclHistoryTypeId] int  NOT NULL,
    [AclHistoryType] varchar(500)  NOT NULL,
    [Id] int  NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [AclListTypeId] int  NOT NULL,
    [AclListType] varchar(500)  NOT NULL,
    [AclEntityTypeId] int  NOT NULL,
    [AclEntityType] varchar(500)  NOT NULL,
    [AclSourceTypeId] int  NOT NULL,
    [AclSourceType] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [AspNetUserId] nvarchar(128)  NOT NULL,
    [OwnerEmail] nvarchar(256)  NULL,
    [OwnerUserName] nvarchar(256)  NOT NULL,
    [InterfaceId] int  NOT NULL,
    [InterfaceName] varchar(250)  NOT NULL,
    [InterfaceTypeId] int  NOT NULL,
    [InterfaceTypeName] varchar(500)  NOT NULL,
    [CataleyaSecurityAclObjId] int  NULL,
    [CataleyaSecurityAclNodeId] int  NULL,
    [Prefix] int  NOT NULL
);
GO

-- Creating table 'vw_Acls'
CREATE TABLE [dbo].[vw_Acls] (
    [Id] int  NOT NULL,
    [Name] varchar(500)  NOT NULL,
    [AclListTypeId] int  NOT NULL,
    [AclListType] varchar(500)  NOT NULL,
    [AclEntityTypeId] int  NOT NULL,
    [AclEntityType] varchar(500)  NOT NULL,
    [AclSourceTypeId] int  NOT NULL,
    [AclSourceType] varchar(500)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [AspNetUserId] nvarchar(128)  NOT NULL,
    [OwnerEmail] nvarchar(256)  NULL,
    [OwnerUserName] nvarchar(256)  NOT NULL,
    [InterfaceId] int  NOT NULL,
    [InterfaceName] varchar(250)  NOT NULL,
    [InterfaceTypeId] int  NOT NULL,
    [InterfaceTypeName] varchar(500)  NOT NULL,
    [CataleyaSecurityAclObjId] int  NULL,
    [CataleyaSecurityAclNodeId] int  NULL,
    [Prefix] int  NOT NULL
);
GO

-- Creating table 'AppConfigs'
CREATE TABLE [dbo].[AppConfigs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(2500)  NOT NULL,
    [Value] varchar(max)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [Visible] bit  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [LastModifiedAt] datetime  NULL,
    [DeletedAt] datetime  NULL,
    [CreatedBy] varchar(250)  NOT NULL,
    [LastModifiedBy] varchar(250)  NULL,
    [DeletedBy] varchar(250)  NULL
);
GO

-- Creating table 'CataleyaInformations'
CREATE TABLE [dbo].[CataleyaInformations] (
    [CataleyaId] int IDENTITY(1,1) NOT NULL,
    [TLCId] nvarchar(128)  NOT NULL,
    [TLCRole] int  NOT NULL,
    [CataleyaUserName] varchar(500)  NULL,
    [CataleyaPassword] nvarchar(max)  NULL,
    [PartitionId] int  NULL
);
GO

-- Creating table 'AspNetUserRoles'
CREATE TABLE [dbo].[AspNetUserRoles] (
    [AspNetRoles_Id] nvarchar(128)  NOT NULL,
    [AspNetUsers_Id] nvarchar(128)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'EmailTemplateRoles'
ALTER TABLE [dbo].[EmailTemplateRoles]
ADD CONSTRAINT [PK_EmailTemplateRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailTemplates'
ALTER TABLE [dbo].[EmailTemplates]
ADD CONSTRAINT [PK_EmailTemplates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailTypes'
ALTER TABLE [dbo].[EmailTypes]
ADD CONSTRAINT [PK_EmailTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AuditLogs'
ALTER TABLE [dbo].[AuditLogs]
ADD CONSTRAINT [PK_AuditLogs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Logs'
ALTER TABLE [dbo].[Logs]
ADD CONSTRAINT [PK_Logs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SiteActivityLogs'
ALTER TABLE [dbo].[SiteActivityLogs]
ADD CONSTRAINT [PK_SiteActivityLogs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id], [EntityId], [EntityName], [Timestamp] in table 'vw_AuditLog'
ALTER TABLE [dbo].[vw_AuditLog]
ADD CONSTRAINT [PK_vw_AuditLog]
    PRIMARY KEY CLUSTERED ([Id], [EntityId], [EntityName], [Timestamp] ASC);
GO

-- Creating primary key on [Id] in table 'AclEntityTypes'
ALTER TABLE [dbo].[AclEntityTypes]
ADD CONSTRAINT [PK_AclEntityTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AclListTypes'
ALTER TABLE [dbo].[AclListTypes]
ADD CONSTRAINT [PK_AclListTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AclSourceTypes'
ALTER TABLE [dbo].[AclSourceTypes]
ADD CONSTRAINT [PK_AclSourceTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetRoles'
ALTER TABLE [dbo].[AspNetRoles]
ADD CONSTRAINT [PK_AspNetRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [PK_AspNetUserClaims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [LoginProvider], [ProviderKey], [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [PK_AspNetUserLogins]
    PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey], [UserId] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [UserId], [UserName] in table 'vw_UsersInRoles'
ALTER TABLE [dbo].[vw_UsersInRoles]
ADD CONSTRAINT [PK_vw_UsersInRoles]
    PRIMARY KEY CLUSTERED ([UserId], [UserName] ASC);
GO

-- Creating primary key on [Id] in table 'AclHistories'
ALTER TABLE [dbo].[AclHistories]
ADD CONSTRAINT [PK_AclHistories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AclHistoryTypes'
ALTER TABLE [dbo].[AclHistoryTypes]
ADD CONSTRAINT [PK_AclHistoryTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Acls'
ALTER TABLE [dbo].[Acls]
ADD CONSTRAINT [PK_Acls]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InterfaceTypes'
ALTER TABLE [dbo].[InterfaceTypes]
ADD CONSTRAINT [PK_InterfaceTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [OperationKey] in table 'C__RefactorLog'
ALTER TABLE [dbo].[C__RefactorLog]
ADD CONSTRAINT [PK_C__RefactorLog]
    PRIMARY KEY CLUSTERED ([OperationKey] ASC);
GO

-- Creating primary key on [AclHistoryId], [AclHistoryName], [AclHistoryCreatedAt], [AclHistoryCreatedBy], [AclHistoryTypeId], [AclHistoryType], [Id], [Name], [AclListTypeId], [AclListType], [AclEntityTypeId], [AclEntityType], [AclSourceTypeId], [AclSourceType], [DisplayOrder], [Visible], [CreatedAt], [CreatedBy], [AspNetUserId], [OwnerUserName], [InterfaceId], [InterfaceName], [InterfaceTypeId], [InterfaceTypeName], [Prefix] in table 'vw_AclHistory'
ALTER TABLE [dbo].[vw_AclHistory]
ADD CONSTRAINT [PK_vw_AclHistory]
    PRIMARY KEY CLUSTERED ([AclHistoryId], [AclHistoryName], [AclHistoryCreatedAt], [AclHistoryCreatedBy], [AclHistoryTypeId], [AclHistoryType], [Id], [Name], [AclListTypeId], [AclListType], [AclEntityTypeId], [AclEntityType], [AclSourceTypeId], [AclSourceType], [DisplayOrder], [Visible], [CreatedAt], [CreatedBy], [AspNetUserId], [OwnerUserName], [InterfaceId], [InterfaceName], [InterfaceTypeId], [InterfaceTypeName], [Prefix] ASC);
GO

-- Creating primary key on [Id], [Name], [AclListTypeId], [AclListType], [AclEntityTypeId], [AclEntityType], [AclSourceTypeId], [AclSourceType], [DisplayOrder], [Visible], [CreatedAt], [CreatedBy], [AspNetUserId], [OwnerUserName], [InterfaceId], [InterfaceName], [InterfaceTypeId], [InterfaceTypeName], [Prefix] in table 'vw_Acls'
ALTER TABLE [dbo].[vw_Acls]
ADD CONSTRAINT [PK_vw_Acls]
    PRIMARY KEY CLUSTERED ([Id], [Name], [AclListTypeId], [AclListType], [AclEntityTypeId], [AclEntityType], [AclSourceTypeId], [AclSourceType], [DisplayOrder], [Visible], [CreatedAt], [CreatedBy], [AspNetUserId], [OwnerUserName], [InterfaceId], [InterfaceName], [InterfaceTypeId], [InterfaceTypeName], [Prefix] ASC);
GO

-- Creating primary key on [Id] in table 'AppConfigs'
ALTER TABLE [dbo].[AppConfigs]
ADD CONSTRAINT [PK_AppConfigs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [CataleyaId] in table 'CataleyaInformations'
ALTER TABLE [dbo].[CataleyaInformations]
ADD CONSTRAINT [PK_CataleyaInformations]
    PRIMARY KEY CLUSTERED ([CataleyaId] ASC);
GO

-- Creating primary key on [AspNetRoles_Id], [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [PK_AspNetUserRoles]
    PRIMARY KEY CLUSTERED ([AspNetRoles_Id], [AspNetUsers_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [EmailTemplateId] in table 'EmailTemplateRoles'
ALTER TABLE [dbo].[EmailTemplateRoles]
ADD CONSTRAINT [FK_EmailTemplateRole_EmailTemplate]
    FOREIGN KEY ([EmailTemplateId])
    REFERENCES [dbo].[EmailTemplates]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmailTemplateRole_EmailTemplate'
CREATE INDEX [IX_FK_EmailTemplateRole_EmailTemplate]
ON [dbo].[EmailTemplateRoles]
    ([EmailTemplateId]);
GO

-- Creating foreign key on [EmailTypeId] in table 'EmailTemplates'
ALTER TABLE [dbo].[EmailTemplates]
ADD CONSTRAINT [FK_EmailTemplates_EmailType]
    FOREIGN KEY ([EmailTypeId])
    REFERENCES [dbo].[EmailTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmailTemplates_EmailType'
CREATE INDEX [IX_FK_EmailTemplates_EmailType]
ON [dbo].[EmailTemplates]
    ([EmailTypeId]);
GO

-- Creating foreign key on [UserId] in table 'AspNetUserClaims'
ALTER TABLE [dbo].[AspNetUserClaims]
ADD CONSTRAINT [FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserClaims_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserClaims]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'AspNetUserLogins'
ALTER TABLE [dbo].[AspNetUserLogins]
ADD CONSTRAINT [FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId'
CREATE INDEX [IX_FK_dbo_AspNetUserLogins_dbo_AspNetUsers_UserId]
ON [dbo].[AspNetUserLogins]
    ([UserId]);
GO

-- Creating foreign key on [AspNetRoles_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetRole]
    FOREIGN KEY ([AspNetRoles_Id])
    REFERENCES [dbo].[AspNetRoles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AspNetUsers_Id] in table 'AspNetUserRoles'
ALTER TABLE [dbo].[AspNetUserRoles]
ADD CONSTRAINT [FK_AspNetUserRoles_AspNetUser]
    FOREIGN KEY ([AspNetUsers_Id])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUserRoles_AspNetUser'
CREATE INDEX [IX_FK_AspNetUserRoles_AspNetUser]
ON [dbo].[AspNetUserRoles]
    ([AspNetUsers_Id]);
GO

-- Creating foreign key on [AclHistoryTypeId] in table 'AclHistories'
ALTER TABLE [dbo].[AclHistories]
ADD CONSTRAINT [FK_AclHistory_AclHistoryType]
    FOREIGN KEY ([AclHistoryTypeId])
    REFERENCES [dbo].[AclHistoryTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AclHistory_AclHistoryType'
CREATE INDEX [IX_FK_AclHistory_AclHistoryType]
ON [dbo].[AclHistories]
    ([AclHistoryTypeId]);
GO

-- Creating foreign key on [AclEntityTypeId] in table 'Acls'
ALTER TABLE [dbo].[Acls]
ADD CONSTRAINT [FK_Acls_AclEntityType]
    FOREIGN KEY ([AclEntityTypeId])
    REFERENCES [dbo].[AclEntityTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acls_AclEntityType'
CREATE INDEX [IX_FK_Acls_AclEntityType]
ON [dbo].[Acls]
    ([AclEntityTypeId]);
GO

-- Creating foreign key on [AclId] in table 'AclHistories'
ALTER TABLE [dbo].[AclHistories]
ADD CONSTRAINT [FK_AclHistory_Acl]
    FOREIGN KEY ([AclId])
    REFERENCES [dbo].[Acls]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AclHistory_Acl'
CREATE INDEX [IX_FK_AclHistory_Acl]
ON [dbo].[AclHistories]
    ([AclId]);
GO

-- Creating foreign key on [AclListTypeId] in table 'Acls'
ALTER TABLE [dbo].[Acls]
ADD CONSTRAINT [FK_Acls_AclListType]
    FOREIGN KEY ([AclListTypeId])
    REFERENCES [dbo].[AclListTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acls_AclListType'
CREATE INDEX [IX_FK_Acls_AclListType]
ON [dbo].[Acls]
    ([AclListTypeId]);
GO

-- Creating foreign key on [AclSourceTypeId] in table 'Acls'
ALTER TABLE [dbo].[Acls]
ADD CONSTRAINT [FK_Acls_AclSourceType]
    FOREIGN KEY ([AclSourceTypeId])
    REFERENCES [dbo].[AclSourceTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acls_AclSourceType'
CREATE INDEX [IX_FK_Acls_AclSourceType]
ON [dbo].[Acls]
    ([AclSourceTypeId]);
GO

-- Creating foreign key on [AspNetUserId] in table 'Acls'
ALTER TABLE [dbo].[Acls]
ADD CONSTRAINT [FK_Acls_AspNetUsers]
    FOREIGN KEY ([AspNetUserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acls_AspNetUsers'
CREATE INDEX [IX_FK_Acls_AspNetUsers]
ON [dbo].[Acls]
    ([AspNetUserId]);
GO

-- Creating foreign key on [InterfaceTypeId] in table 'Acls'
ALTER TABLE [dbo].[Acls]
ADD CONSTRAINT [FK_Acls_InterfaceType]
    FOREIGN KEY ([InterfaceTypeId])
    REFERENCES [dbo].[InterfaceTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acls_InterfaceType'
CREATE INDEX [IX_FK_Acls_InterfaceType]
ON [dbo].[Acls]
    ([InterfaceTypeId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------