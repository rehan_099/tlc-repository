﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfig.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AppConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The AppConfig Type.
    /// </summary>
    public partial class AppConfig : IAppConfig
    {
    }
}
