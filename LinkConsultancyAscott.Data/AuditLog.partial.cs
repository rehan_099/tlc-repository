﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditLog.partial.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AuditLog type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Data
{
    /// <summary>
    /// The AuditLog.
    /// </summary>
    public partial class AuditLog : IAuditLog
    {
    }
}
