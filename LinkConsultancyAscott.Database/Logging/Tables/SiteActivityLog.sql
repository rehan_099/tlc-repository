﻿CREATE TABLE [Logging].[SiteActivityLog] (
    [Id]                BIGINT          IDENTITY (1, 1) NOT NULL,
    [ActivityTimestamp] DATETIME2       NOT NULL CONSTRAINT [DF_SiteActivityLog_CreatedAt]  DEFAULT (SYSUTCDATETIME()),
    [UserName]          NVARCHAR (256)  NULL,
    [Controller]        NVARCHAR (256)  NULL,
    [Action]            NVARCHAR (256)  NULL,
    [IPAddress]         NVARCHAR (256)  NOT NULL,
    [Browser]           NVARCHAR (256)  NOT NULL,
    [ActionDuration]    TIME (7)        NULL,
    [RouteInfo]         NVARCHAR (1000) NULL,
    [IsMobileDevice]    BIT             NULL,
    [Platform]          NVARCHAR (256)  NULL,
    [Domain]            NVARCHAR (2000) NULL,
    [Area]              NVARCHAR (256)  NULL,
	[HttpVerb]                  VARCHAR (256)  NULL,
	[ResponseStatus]            VARCHAR (256)  NULL,
	[ResponseStatusDescription] VARCHAR (1000)  NULL,
	[ResponseData]              NVARCHAR (MAX)  NULL,
	[QueryParameters]           VARCHAR (5000)  NULL,
	[RequestUrl]                VARCHAR (5000)  NULL
    CONSTRAINT [PK_SiteActivityLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

