﻿CREATE TABLE [Logging].[AuditLog] (
    [Id]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [Timestamp]        DATETIME2        NOT NULL CONSTRAINT [DF_AuditLog_CreatedAt]  DEFAULT (SYSUTCDATETIME()),
    [EntityId]         BIGINT           NOT NULL,
    [EntityName]       VARCHAR (200)    NOT NULL,
    [UserName]         VARCHAR (200)    NULL,
    [EventType]        VARCHAR (500) NULL,
    [EventDescription] VARCHAR (500) NULL,
    [EventDetail]      VARCHAR (MAX) NULL,
    [Source]           VARCHAR (500) NULL,
    CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

