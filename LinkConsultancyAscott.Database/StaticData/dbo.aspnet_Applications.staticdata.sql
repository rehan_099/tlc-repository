/***************************************
*** Static data management script ***
***************************************/
-- This script will manage the static data from
-- your Team Database project for [dbo].[aspnet_Applications].

PRINT 'Updating static data table [dbo].[aspnet_Applications]'

-- Set to your region's date format to ensure dates are updated correctly
SET DATEFORMAT dmy

-- Turn off affected rows being returned
SET NOCOUNT ON

-- Change this to 1 to delete missing records in the target
-- WARNING: Setting this to 1 can cause damage to your database
-- and cause failed deployment if there are any rows referencing
-- a record which has been deleted.
DECLARE @DeleteMissingRecords BIT
SET @DeleteMissingRecords = 0 

-- 1: Define table variable
DECLARE @tblTempTable TABLE (
[ApplicationName] nvarchar(256),
[LoweredApplicationName] nvarchar(256),
[ApplicationId] uniqueidentifier,
[Description] nvarchar(256)
)

-- 2: Populate the table variable with data
-- This is where you manage your data in source control. You
-- can add and modify entries, but because of potential foreign
-- key contraint violations this script will not delete any
-- removed entries. If you remove an entry then it will no longer
-- be added to new databases based on your schema, but the entry
-- will not be deleted from databases in which the value already exists.
INSERT INTO @tblTempTable ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES ('LinkConsultancyAscott', 'LinkConsultancyAscott', '4116e90d-22d9-42ff-8446-23315a1fec3f', NULL)


-- 3: Update existing records that have changed, add missing and delete missing in source
-- SET IDENTITY_INSERT [dbo].[aspnet_Applications] ON  -- PD -- There is no identity field in the table
MERGE INTO [dbo].[aspnet_Applications] AS T
USING @tblTempTable AS S
  ON T.ApplicationId = S.ApplicationId
WHEN MATCHED
AND (T.LoweredApplicationName <> S.LoweredApplicationName
OR T.ApplicationId <> S.ApplicationId
OR T.Description <> S.Description
) THEN
UPDATE SET T.LoweredApplicationName = S.LoweredApplicationName,
T.ApplicationId = S.ApplicationId,
T.Description = S.Description
WHEN NOT MATCHED THEN
    INSERT ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description])
    VALUES (S.[ApplicationName], S.[LoweredApplicationName], S.[ApplicationId], S.[Description])
WHEN NOT MATCHED BY SOURCE THEN
  DELETE;
-- SET IDENTITY_INSERT [dbo].[aspnet_Applications] OFF -- PD -- There is no identity field in the table


PRINT 'Finished updating static data table '

-- Note: If you are not using the new GDR version of DBPro -- then remove this go command.

GO