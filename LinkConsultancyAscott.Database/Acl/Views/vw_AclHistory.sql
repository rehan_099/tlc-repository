﻿CREATE VIEW [Acl].[vw_AclHistory]
	AS 
SELECT h.[Id] AS AclHistoryId,
	   h.[Name] AS AclHistoryName,
	   h.[CreatedAt] AS AclHistoryCreatedAt,
	   h.[CreatedBy] AS AclHistoryCreatedBy,
	   h.[AclHistoryTypeId],
	   ht.[Name] AS [AclHistoryType],
       a.*
  FROM [Acl].[vw_Acls] a
  JOIN [Acl].[AclHistory] h ON h.[AclId] = a.[Id]
  JOIN [Acl].[AclHistoryTypes] ht ON h.[AclHistoryTypeId] = ht.[Id]
  
  WHERE h.[DeletedBy] IS NULL;
GO
