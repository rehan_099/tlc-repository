﻿CREATE TABLE [Acl].[AclListTypes]
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Name] [VARCHAR](500) NOT NULL,
	[DisplayOrder] [INT] NOT NULL CONSTRAINT [DF_AclListTypes_DisplayOrder]  DEFAULT ((10)),
	[Visible] [BIT] NOT NULL CONSTRAINT [DF_AclListTypes_Visible]  DEFAULT ((1)),
	[CreatedAt] [DATETIME] NOT NULL CONSTRAINT [DF_AclListTypes_CreatedAt]  DEFAULT (GETDATE()),
	[LastModifiedAt] [DATETIME] NULL,
	[DeletedAt] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](250) NOT NULL CONSTRAINT [DF_AclListTypes_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedBy] [VARCHAR](250) NULL,
	[DeletedBy] [VARCHAR](250) NULL
	CONSTRAINT [PK_AclListTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
)
