﻿CREATE TABLE [Acl].[AclHistory]
(
	[Id]                     INT IDENTITY(1,1) NOT NULL,
	[Name]                   VARCHAR(5000) NOT NULL,
	[AclHistoryTypeId]       INT NOT NULL,
	[AclId]                  INT NOT NULL,
	[CreatedAt]              DATETIME2 NOT NULL CONSTRAINT [DF_AclHistory_CreatedAt]  DEFAULT (SYSUTCDATETIME()),
	[CreatedBy]              VARCHAR(250) NOT NULL CONSTRAINT [DF_AclHistory_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedAt]         DATETIME2 NULL,
	[LastModifiedBy]         VARCHAR(250) NULL,
	[DeletedAt]              DATETIME2 NULL,
	[DeletedBy]              VARCHAR(250) NULL,
    CONSTRAINT [PK_AclHistory] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_AclHistory_Acl] FOREIGN KEY([AclId]) REFERENCES [Acl].[Acls] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [FK_AclHistory_AclHistoryType] FOREIGN KEY([AclHistoryTypeId]) REFERENCES [Acl].[AclHistoryTypes] ([Id]) ON DELETE CASCADE
)
GO
