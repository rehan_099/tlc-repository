﻿CREATE TABLE [Acl].[InterfaceTypes]
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Name] [VARCHAR](500) NOT NULL,
	[DisplayOrder] [INT] NOT NULL CONSTRAINT [DF_InterfaceTypes_DisplayOrder]  DEFAULT ((10)),
	[Visible] [BIT] NOT NULL CONSTRAINT [DF_InterfaceTypes_Visible]  DEFAULT ((1)),
	[CreatedAt] [DATETIME] NOT NULL CONSTRAINT [DF_InterfaceTypes_CreatedAt]  DEFAULT (GETDATE()),
	[LastModifiedAt] [DATETIME] NULL,
	[DeletedAt] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](250) NOT NULL CONSTRAINT [DF_InterfaceTypes_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedBy] [VARCHAR](250) NULL,
	[DeletedBy] [VARCHAR](250) NULL
	CONSTRAINT [PK_InterfaceTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
)
