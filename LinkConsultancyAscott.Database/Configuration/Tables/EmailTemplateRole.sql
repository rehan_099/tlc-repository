﻿CREATE TABLE [Configuration].[EmailTemplateRole]
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[EmailTemplateId] [INT] NOT NULL,
	[DisplayOrder] [INT] NOT NULL CONSTRAINT [DF_EmailTemplateRole_DisplayOrder]  DEFAULT ((10)),
	[Visible] [BIT] NOT NULL CONSTRAINT [DF_EmailTemplateRole_Visible]  DEFAULT ((1)),
	[CreatedAt] [DATETIME] NOT NULL CONSTRAINT [DF_EmailTemplateRole_CreatedAt]  DEFAULT (GETDATE()),
	[LastModifiedAt] [DATETIME] NULL,
	[DeletedAt] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](250) NOT NULL CONSTRAINT [DF_EmailTemplateRole_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedBy] [VARCHAR](250) NULL,
	[DeletedBy] [VARCHAR](250) NULL,
	CONSTRAINT [PK_EmailTemplateRole] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_EmailTemplateRole_EmailTemplate] FOREIGN KEY([EmailTemplateId]) REFERENCES [Configuration].[EmailTemplates] ([Id]) ON DELETE CASCADE
)
