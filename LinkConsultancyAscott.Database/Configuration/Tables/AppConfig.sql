﻿CREATE TABLE [Configuration].[AppConfig]
(
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[Name] [VARCHAR](2500) NOT NULL,
	[Value] [VARCHAR](MAX) NOT NULL,
	[DisplayOrder] [INT] NOT NULL CONSTRAINT [DF_AppConfig_DisplayOrder]  DEFAULT ((10)),
	[Visible] [BIT] NOT NULL CONSTRAINT [DF_AppConfig_Visible]  DEFAULT ((1)),
	[CreatedAt] [DATETIME] NOT NULL CONSTRAINT [DF_AppConfig_CreatedAt]  DEFAULT (GETDATE()),
	[LastModifiedAt] [DATETIME] NULL,
	[DeletedAt] [DATETIME] NULL,
	[CreatedBy] [VARCHAR](250) NOT NULL CONSTRAINT [DF_AppConfig_CreatedBy]  DEFAULT ('N/A'),
	[LastModifiedBy] [VARCHAR](250) NULL,
	[DeletedBy] [VARCHAR](250) NULL
	CONSTRAINT [PK_AppConfig] PRIMARY KEY CLUSTERED ([Id] ASC)
)
