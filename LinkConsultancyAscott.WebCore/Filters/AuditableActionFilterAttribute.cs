﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditableActionFilterAttribute.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AuditableActionFilterAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Filters
{
    using System;
    using System.Web;
    using System.Web.Mvc;

    using Service.ViewModels;

    /// <summary>
    /// The Auditable Action Filter Attribute.
    /// </summary>
    [System.AttributeUsageAttribute(System.AttributeTargets.All, AllowMultiple = false)]
    public class AuditableActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Called when [action executing].
        /// </summary>
        /// <param name="filterContext">The action context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // So got a view model now update it
            var user = filterContext.HttpContext.Request.GetUserFromRequest();

            foreach (var values in filterContext.ActionParameters)
            {
                // Log.InfoFormat(" Parameter: [{0}]", values.Key);
                if (values.Value is IAuditableViewModel<int>)
                {
                    var viewModel = values.Value as IAuditableViewModel<int>;
                    this.UpdateViewModel(
                        viewModel,
                        filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                        filterContext.ActionDescriptor.ActionName,
                        user);
                }
                else if (values.Value is IAuditableViewModel<long>)
                {
                    var viewModel = values.Value as IAuditableViewModel<long>;
                    this.UpdateViewModel(
                        viewModel,
                        filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                        filterContext.ActionDescriptor.ActionName,
                        user);
                }
            }

            // Call the base method
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// The update view model.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <param name="user">
        /// The user.
        /// </param>
        private void UpdateViewModel<T>(IAuditableViewModel<T> viewModel, string controller, string action, string user)
             where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        {
            if (action.StartsWith("Create", StringComparison.InvariantCultureIgnoreCase) || action.StartsWith("GridCreate", StringComparison.InvariantCultureIgnoreCase))
            {
                viewModel.LastModifiedBy = user;
                viewModel.CreatedBy = user;
                viewModel.CreatedAt = DateTime.UtcNow;
                viewModel.LastModifiedAt = DateTime.UtcNow;
            }
            else if (action.StartsWith("Update", StringComparison.InvariantCultureIgnoreCase))
            {
                if (string.IsNullOrEmpty(viewModel.CreatedBy))
                {
                    viewModel.CreatedBy = user;
                }

                viewModel.LastModifiedBy = user;
                viewModel.LastModifiedAt = DateTime.UtcNow;
            }
            else if (action.StartsWith("Destroy", StringComparison.InvariantCultureIgnoreCase))
            {
                if (string.IsNullOrEmpty(viewModel.CreatedBy))
                {
                    viewModel.CreatedBy = user;
                }

                viewModel.DeletedBy = user;
                viewModel.DeletedAt = DateTime.UtcNow;
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}