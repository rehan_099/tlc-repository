﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclUploadController.cs" company="The Link Consultancy - Ascott">
// Copyright (c) The Link Consultancy - Ascott. 2017 </copyright>
// <summary>
//   Defines the AclUploadController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Diagnostics;

    using System.Net;
    using System.Net.Http;
    using System.Runtime.ExceptionServices;
    using System.Threading.Tasks;
    using System.Web.Http;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Services.AclUploadService;
    using LinkConsultancyAscott.WebCore.Hubs;

    /// <inheritdoc />
    /// <summary> The Transform Validator Controller. </summary>
    [AllowAnonymous]
    public class AclUploadController : AclControllerBase
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:AclListUploadController" /> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public AclUploadController(IAclUploadService service) : base(service)
        {
        }

        /// <summary>
        /// Uploads this instance.
        /// </summary>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        [HttpPost]
        [Route("api/aclupload")]
        public async Task<HttpResponseMessage> Upload()
        {
            try
            {
                HttpResponseMessage response;
                var validationRequest = await BuildRequest<AclFileUploadRequest>(this.Request).ConfigureAwait(false);
                Logger.Info("Entered TransformValidatorController::Upload");

                if (validationRequest.StatusCode.Equals(HttpStatusCode.OK))
                {
                    var result = await this.service.UploadAclFileAsync(validationRequest).ConfigureAwait(false);

                    response = this.Request.CreateResponse(validationRequest.StatusCode, result);
                    LinkConsultancyAscottHub.AclUpdated(string.Empty);
                }
                else
                {
                    response = this.Request.CreateResponse(validationRequest.StatusCode, validationRequest.Message);
                }

                Logger.Info("Exiting  TransformValidatorController::Upload");
                return response;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                Debug.WriteLine(ex.ToString());
                ExceptionDispatchInfo.Capture(ex).Throw();
                // ReSharper disable once HeuristicUnreachableCode : needed to avoid compilation error since compiler doesn't know Throw method generates an exception.
                throw;
            }
        }
    }
}
