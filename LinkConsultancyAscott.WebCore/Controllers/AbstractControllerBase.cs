﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractControllerBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AbstractControllerBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Web.Mvc;

    using Service.ViewModels;

    using LinkConsultancyAscott.WebCore.Utilites;

    /// <summary>
    /// Class AbstractControllerBase.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class AbstractControllerBase : Controller
    {
        /// <summary>
        /// The log.
        /// </summary>
        protected static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The message
        /// </summary>
        protected string Message;

        /// <summary>
        /// Gets the base URL.
        /// </summary>
        /// <value>The base URL.</value>
        protected string BaseUrl
        {
            get
            {
                var baseUrl = string.Empty;

                if (this.Request != null && this.Url != null)
                {
                    baseUrl = RuntimeUtils.BuildBaseUrl(this.Request.Url, this.Url);
                }

                return baseUrl;
            }
        }

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        protected string UserName
        {
            get
            {
                var userName = string.Empty;

                if (this.User != null)
                {
                    userName = this.User.Identity.Name;
                }

                return userName;
            }
        }

        /// <summary>
        /// Called when an unhandled exception occurs in the action.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
             filterContext.ExceptionHandled = true;
        
             // Redirect on error:
             filterContext.Result = this.RedirectToAction("Index", "Error");
        
             // OR set the result without redirection:
             filterContext.Result = new ViewResult
             {
                 ViewName = "~/Views/Error/Index.cshtml"
             };
        }

        /// <summary>
        /// Gets the empty select list.
        /// </summary>
        /// <returns> Selection List. </returns>
        protected IList<SelectListItem> GetEmptySelectList()
        {
            IList<SelectListItem> list = new List<SelectListItem>
            {
                new SelectListItem { Text = string.Empty, Value = string.Empty }
            };
            return list;
        }

        /// <summary>
        /// Logs the view model properties.
        /// </summary>
        /// <param name="vm">The view model. </param>
        protected void LogViewModelProperties(ViewModelBase vm)
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            var currentMethodName = sf.GetMethod();

            var prop = vm.GetPropertyValues();

            Logger.InfoFormat(
                "{0}.{1}: Incoming ViewModel : {2} User : {3}",
                this.GetType().Name,
                currentMethodName.Name,
                vm.GetType().Name, 
                this.UserName);

            foreach (var p in prop)
            {
                Logger.InfoFormat("Prop Value - {0}", p);
            }
        }

        /// <summary>
        /// Gets the home URL.
        /// </summary>
        /// <returns> Home URL. </returns>
        protected string GetHomeUrl()
        {
            string homeurl = string.Empty;
            if (this.Request?.Url != null)
            {
                var urlHelper = new UrlHelper(this.Request.RequestContext);
                homeurl = $"{this.Request.Url.Authority}{urlHelper.Content("~")}";
            }

            return homeurl;
        }

        /// <summary>
        /// Reads to end.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>System Byte[].</returns>
        protected byte[] ReadToEnd(Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }

                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
