﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportsController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Reports Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System.IO;
    using System.Web;
    using System.Web.Mvc;

    using LinkConsultancyAscott.Service.Models;

    using Telerik.Reporting.Cache.File;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.Engine;
    using Telerik.Reporting.Services.WebApi;

    [Authorize(Roles = ApplicationRoles.Users)]
    [HandleError]
    public class ReportsController : ReportsControllerBase
    {
        /// <summary>
        /// The preserved configuration.
        /// </summary>
        private static ReportServiceConfiguration preservedConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportsController"/> class.
        /// </summary>
        public ReportsController()
        {
            this.ReportServiceConfiguration = PreservedConfiguration;
        }

        /// <summary>
        /// Gets the preserved configuration.
        /// </summary>
        private static IReportServiceConfiguration PreservedConfiguration
        {
            get
            {
                if (null == preservedConfiguration)
                {
                    preservedConfiguration = new ReportServiceConfiguration
                    {
                        HostAppId = "LinkConsultancyAscott.App",
                        Storage = new FileStorage(),
                        ReportResolver = CreateResolver(),
                        // ReportSharingTimeout = 0,
                        // ClientSessionTimeout = 15,
                    };
                }

                return preservedConfiguration;
            }
        }



        /// <summary>
        /// Creates the resolver.
        /// </summary>
        /// <returns>IReportResolver.</returns>
        public static IReportResolver CreateResolver()
        {
            var appPath = HttpContext.Current.Server.MapPath("~/");
            var reportsPath = Path.Combine(appPath, @"Reports");

            return new ReportFileResolver(reportsPath)
                .AddFallbackResolver(new ReportTypeResolver());
        }
    }
}
