﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class HomeController.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Web.Mvc;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Services.HomeService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels.Home;

    /// <summary>
    /// Class HomeController.
    /// </summary>
    /// <seealso cref="Controller" />
    [Authorize(Roles = ApplicationRoles.Users)]
    public class HomeController : AbstractControllerBase
    {
        /// <summary>
        /// The home service
        /// </summary>
        private readonly IHomeService homeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="homeService">The home service.</param>
        public HomeController(
            IHomeService homeService)
        {
            this.homeService = homeService;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        /// <returns>Action Result.</returns>
        public ActionResult Start()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "Start";
            return this.View();
        }

        /// <summary>
        /// Robots this instance.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [AllowAnonymous]
        public ActionResult Robots()
        {
            Response.ContentType = "text/plain";
            return View();
        }

        /// <summary>
        /// Errors this instance.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        public ActionResult Error()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "Error";
            Exception exception = this.Server.GetLastError();
            System.Diagnostics.Debug.WriteLine(exception);
            return this.View();

        }
    }
}