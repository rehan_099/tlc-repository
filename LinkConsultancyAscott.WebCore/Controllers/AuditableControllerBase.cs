﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditableControllerBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Coloured Controller Base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Data;


    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Service.Infrastructure;
    using Service.Repository;
    using Service.ViewModels;

    /// <summary>
    /// The look up controller base.
    /// </summary>
    /// <typeparam name="T"> The DTO Type </typeparam>
    /// <typeparam name="TM">The Type</typeparam>
    /// <typeparam name="TVm">The View Model</typeparam>
    /// <typeparam name="TR">The Type Repository</typeparam>
    /// <seealso cref="LinkConsultancyAscott.WebCore.Controllers.AbstractControllerBase" />
    [HandleError]
    public class AuditableControllerBase<T, TM, TVm, TR> : AbstractControllerBase
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
                                   where TM : class, IAuditedEntity<T>
                                   where TVm : AuditableViewModelBase<T, TM>, new()
                                   where TR : IAuditedEntityRepositoryBase<T, TM>
    {
        /// <summary>
        /// The _repository
        /// </summary>
        protected readonly TR Repository;

        /// <summary>
        /// The _unit of work
        /// </summary>
        protected readonly IUnitOfWork UnitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditableControllerBase{T, TM, TVm, TR}"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public AuditableControllerBase(
            TR repository,
            IUnitOfWork unitOfWork)
        {
            this.Repository = repository;
            this.UnitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Action Result.</returns>
        public virtual ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var dtoList = this.Repository.GetAll();

            var list = dtoList.Select(dto => new TVm { DataObject = dto }).ToList();

            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the view model.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>The view model.</returns>
        public virtual TVm GetVM(TM dto)
        {
            var vm = this.GetNewViewModel();
            vm.DataObject = dto;
            return vm;
        }

        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult Create([DataSourceRequest] DataSourceRequest request, TVm viewModel)
        {
            // Perform model binding (fill the product properties and validate it).           
            if (viewModel != null && this.ModelState.IsValid)
            {
                // The model is valid - insert the product. 
                var dto = this.Repository.GetNewDataObject();
                viewModel.UpdateDataObject(dto);
                this.Repository.Add(dto);
                this.UnitOfWork.Commit();
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Updates the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult Update([DataSourceRequest] DataSourceRequest request, TVm viewModel) 
        {
            if (viewModel != null && this.ModelState.IsValid)
            {
                TM dto = this.Repository.Get(viewModel.Id);

                viewModel.Details = dto;

                viewModel.UpdateDataObject(dto);

                this.Repository.Edit(dto);
                this.UnitOfWork.Commit();
            }

            return this.Json(this.ModelState.ToDataSourceResult());
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult Destroy([DataSourceRequest] DataSourceRequest request, TVm viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                TM dto = this.Repository.Get(viewModel.Id);

                viewModel.Details = dto;

                viewModel.UpdateDataObject(dto);
      
                this.Repository.Edit(dto);
                this.UnitOfWork.Commit();
            }

            // Rebind the grid       
            return this.Json(this.ModelState.ToDataSourceResult());
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult DestroyDeleted([DataSourceRequest] DataSourceRequest request, TVm viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                var dto = this.Repository.Get(viewModel.Id);

                this.Repository.Delete(dto);
                this.UnitOfWork.Commit();
            }

            // Rebind the grid       
            return this.Json(this.ModelState.ToDataSourceResult());
        }

        /// <summary>
        /// Gets the new view model.
        /// </summary>
        /// <returns>The new view Model.</returns>
        protected TVm GetNewViewModel()
        {
            return (TVm)Activator.CreateInstance(typeof(TVm));
        }
    }
}
