﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StaticDataController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Static Data Controller base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;


    using Service.Models;
    using Service.Services.StaticDataService;

    /// <summary>
    /// Class StaticDataController.
    /// </summary>
    /// <seealso cref="AbstractControllerBase" />
    [Authorize(Roles = ApplicationRoles.Users)]
    [HandleError]
    public class StaticDataController : AbstractControllerBase
    {
        /// <summary>
        /// The static data service
        /// </summary>
        private readonly IStaticDataService staticDataService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticDataController"/> class.
        /// </summary>
        /// <param name="staticDataService">The static data service.</param>
        public StaticDataController(IStaticDataService staticDataService)
        {
            this.staticDataService = staticDataService;
        }

        /// <summary>
        /// Gets the sip interfaces list.
        /// </summary>
        /// <returns>Task&lt;JsonResult&gt;.</returns>
        [HttpPost]
        public async Task<JsonResult> GetSipInterfacesList()
        {
            var viewModelList = await this.staticDataService.GetSipInterfacesList().ConfigureAwait(false);

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the media interfaces list.
        /// </summary>
        /// <returns>Task&lt;JsonResult&gt;.</returns>
        [HttpPost]
        public async Task<JsonResult> GetMediaInterfacesList()
        {
            var viewModelList = await this.staticDataService.GetMediaInterfacesList().ConfigureAwait(false);

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the user interfaces list.
        /// </summary>
        /// <returns>Task&lt;JsonResult&gt;.</returns>
        [HttpPost]
        public async Task<JsonResult> GetUserInterfacesList()
        {
            var viewModelList = await this.staticDataService.GetUserInterfacesListAsync(this.UserName).ConfigureAwait(false);

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }
    }
}