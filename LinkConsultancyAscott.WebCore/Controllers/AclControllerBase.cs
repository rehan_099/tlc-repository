﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclControllerBase.cs" company="The Link Consultancy - Ascott">
// Copyright (c) The Link Consultancy - Ascott. 2017 </copyright>
// <summary>
//   Defines the AclUploadController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Services.AclUploadService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.WebCore.Utilites;

    /// <inheritdoc />
    /// <summary> The Transform Validator Controller. </summary>
    [AllowAnonymous]
    public abstract class AclControllerBase : AbstractApiControllerBase
    {
        /// <summary>
        /// The service
        /// </summary>
        protected readonly IAclUploadService service;
        
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:AclListUploadController" /> class.
        /// </summary>
        /// <param name="service">The service.</param>
        protected AclControllerBase(IAclUploadService service)
        {
            this.service = service;
        }
        
        /// <summary>
        /// Builds the request.
        /// </summary>
        /// <typeparam name="T"> Type of the request </typeparam>
        /// <param name="request">The request.</param>
        /// <returns>Returns Task&lt;T&gt;.</returns>
        protected static async Task<T> BuildRequest<T>(HttpRequestMessage request)
            where T : AclRequestBase, new()
        {
            // Read the file and form data.
            var provider = new MultipartFormDataMemoryStreamProvider();
            await request.Content.ReadAsMultipartAsync(provider).ConfigureAwait(false);

            // Get the ACL List Type
            AclListType aclListType;
            var aclType = provider.FormData[AppConstants.AclTypeName];

            if (!string.IsNullOrEmpty(aclType))
            {
                if (!Enum.TryParse(aclType, true, out aclListType))
                {
                    return new T
                               {
                                   StatusCode = HttpStatusCode.UnsupportedMediaType,
                                   Message = "Invalid ACL List Type."
                               };
                }
            }
            else
            {
                return new T
                {
                    StatusCode = HttpStatusCode.UnsupportedMediaType,
                    Message = "Missing ACL List Type."
                };
            }

            // Get the ACL Source 
            AclSourceType aclSourceType;
            var aclSource = provider.FormData[AppConstants.AclSourceName];

            if (!string.IsNullOrEmpty(aclSource))
            {
                if (!Enum.TryParse(aclSource, true, out aclSourceType))
                {
                    return new T
                    {
                        StatusCode = HttpStatusCode.UnsupportedMediaType,
                        Message = "Invalid ACL Source."
                    };
                }
            }
            else
            {
                return new T
                {
                    StatusCode = HttpStatusCode.UnsupportedMediaType,
                    Message = "Missing ACL Source."
                };
            }

            // Extract the Web Token
            Guid webApiToken;
            var webToken = provider.FormData[AppConstants.ApiTokenName];

            if (!string.IsNullOrEmpty(webToken))
            {
                if (!Guid.TryParse(webToken, out webApiToken))
                {
                    return new T
                               {
                                   StatusCode = HttpStatusCode.UnsupportedMediaType,
                                   Message = "Invalid Web Token."
                    };
                }
            }
            else
            {
                return new T
                {
                    StatusCode = HttpStatusCode.UnsupportedMediaType,
                    Message = "Missing Web Token."
                };
            }

            // Extract the Web Token
            var accessToken = provider.FormData[AppConstants.AccessTokenName];

            if (string.IsNullOrEmpty(accessToken))
            {
                return new T
                {
                    StatusCode = HttpStatusCode.UnsupportedMediaType,
                    Message = "Missing Access Token."
                };
            }

            var interfaceName = provider.FormData[AppConstants.InterfaceName];

            if (aclListType.Equals(AclListType.WhiteList))
            {
                if (string.IsNullOrEmpty(interfaceName))
                {
                    return new T
                               {
                                   StatusCode = HttpStatusCode.UnsupportedMediaType,
                                   Message = "Missing Interface Name."
                               };
                }
            }


            var retVal = new T
            {
                AclType = aclListType,
                AclSource = aclSourceType,
                WebToken = webApiToken,
                AccessToken = accessToken,
                InterfaceName = interfaceName
            };

            if (retVal is AclCreateRequest)
            {
                var aclCreateRequest = retVal as AclCreateRequest;

                var ipAddress = provider.FormData[AppConstants.IpAddressName];

                if (string.IsNullOrEmpty(ipAddress))
                {
                    return new T
                    {
                        StatusCode = HttpStatusCode.UnsupportedMediaType,
                        Message = "Missing IP Address."
                    };
                }

                var prefix = provider.FormData[AppConstants.PrefixName];

                if (string.IsNullOrEmpty(prefix))
                {
                    return new T
                    {
                        StatusCode = HttpStatusCode.UnsupportedMediaType,
                        Message = "Missing Prefix."
                    };
                }

                aclCreateRequest.IpAddress = ipAddress;
                aclCreateRequest.Prefix = prefix;
            }
            else if (retVal is AclFileUploadRequest)
            {
                var aclFileUploadRequest = retVal as AclFileUploadRequest;

                // There should only be a single file
                if (!provider.FileStreams.Any())
                {
                    return new T { StatusCode = HttpStatusCode.UnsupportedMediaType, Message = "No file uploaded." };
                }

                if (provider.FileStreams.Count != 1)
                {
                    return new T { StatusCode = HttpStatusCode.UnsupportedMediaType, Message = "Only one file at a time." };
                }

                var file = provider.FileStreams.FirstOrDefault();

                aclFileUploadRequest.Filename = file.Key.Trim('"');
                aclFileUploadRequest.FileData = new MemoryStream();

                // Get the source file stream
                await file.Value.CopyToAsync(aclFileUploadRequest.FileData).ConfigureAwait(false);
                await aclFileUploadRequest.FileData.FlushAsync().ConfigureAwait(false);
            }

            // Must be successful to reach here
            retVal.StatusCode = HttpStatusCode.OK;

            return retVal;
        }
    }
}
