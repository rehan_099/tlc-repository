﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportViewerController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Report Viewer controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Services.ReportViewerService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels.ReportViewer;

    /// <summary>
    /// The home controller.
    /// </summary>
    [Authorize(Roles = ApplicationRoles.Users)]
    [HandleError]
    public class ReportViewerController : AbstractControllerBase
    {
        /// <summary>
        /// The report viewer service.
        /// </summary>
        private readonly IReportViewerService reportViewerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportViewerController"/> class.
        /// </summary>
        /// <param name="reportViewerService"> The report viewer service </param>
        public ReportViewerController(IReportViewerService reportViewerService)
        {
            this.reportViewerService = reportViewerService;
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Viewer()
        {
            var queryValues = this.Request.QueryString;
            var viewModel = new ViewerViewModel();

            // Attempt to get the report name 
            foreach (var key in queryValues.AllKeys)
            {
                if (key == "reportName")
                {
                    viewModel.ReportName = queryValues["reportName"];
                }
                else
                {
                    viewModel.Parameters[key] = queryValues[key];
                }
            }

            var appPath = this.Server.MapPath("~/Content/Images/");
            viewModel.Parameters["ImageRootPath"] = appPath;

            return this.PartialView("Viewer", viewModel);
        }

        /// <summary>
        /// The viewer full.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ViewerFull()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "Report Viewer";
            var queryValues = this.Request.QueryString;
            var viewModel = new ViewerViewModel();

            // Attempt to get the report name 
            foreach (var key in queryValues.AllKeys)
            {
                if (key == "reportName")
                {
                    viewModel.ReportName = queryValues["reportName"];
                }
                else
                {
                    viewModel.Parameters[key] = queryValues[key];
                }
            }

            return this.View("ViewerFull", viewModel);
        }

        /// <summary>
        /// The download report.
        /// </summary>
        /// <returns>
        /// The <see cref="FileContentResult"/>.
        /// </returns>
        public FileContentResult DownloadReport()
        {
            var queryValues = this.Request.QueryString;
            var reportName = string.Empty;
            var exportFormat = string.Empty;
            var parameters = new Dictionary<string, string>();
            foreach (var key in queryValues.AllKeys)
            {
                if (key == "reportName")
                {
                    reportName = queryValues["reportName"];
                }
                else if (key == "exportFormat")
                {
                    exportFormat = queryValues[key];
                }
                else if (key == "requirementId")
                {
                    parameters["id"] = queryValues[key];
                }
                else
                {
                    parameters[key] = queryValues[key];
                }
            }

            var reportType = (RenderingExtensions)Enum.Parse(typeof(RenderingExtensions), exportFormat);

            var reportResult = this.reportViewerService.ExportToPdf(reportName, reportType, parameters);

            var fileName = string.Format("attachment;filename=ReportFile{0}{1:yyyyMMdd}.{2}", reportResult.DocumentName, DateTime.Today, reportResult.Extension);

            this.Response.AddHeader("content-disposition", fileName);

            return new FileContentResult(reportResult.DocumentBytes, reportResult.MimeType);
        }
    }
}
