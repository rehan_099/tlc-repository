﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestExtensions.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace System.Web
{
    /// <summary>
    /// The Request Extensions extensions.
    /// </summary>
    public static class RequestExtensions
    {
        /// <exception cref="NotImplementedException">Always.</exception>
        public static string GetUserFromRequest(this HttpRequestBase request)
        {
            var retVal = "No User";
            if (request.IsAuthenticated)
            {
                if (request.LogonUserIdentity != null)
                {
                    retVal = request.LogonUserIdentity.Name;
                }
            }
            else
            {
                if (request.LogonUserIdentity != null)
                {
                    retVal = request.LogonUserIdentity.Name;
                }
            }

            return retVal;
        }
    }
}