﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteActivityLogController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Site Activity Log Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using Service.Services.SiteActivityLogService;

    using LinkConsultancyAscott.Service.Models;

    using WebCore.Controllers;

    /// <summary>
    /// Class AdministrationUserController.
    /// </summary>
    [HandleError]
    [Authorize(Roles = ApplicationRoles.Developers)]
    public class SiteActivityLogController : AbstractControllerBase
    {
        /// <summary>
        /// The Site Activity Log service
        /// </summary>
        private readonly ISiteActivityLogService siteActivityLogService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteActivityLogController"/> class. 
        /// </summary>
        /// <param name="siteActivityLogService">
        /// The Site Activity Log Service.
        /// </param>
        public SiteActivityLogController(ISiteActivityLogService siteActivityLogService)
        {
            this.siteActivityLogService = siteActivityLogService;
        }

        /// <summary>
        /// The log list.
        /// </summary>
        /// <returns>The <see cref="ActionResult" />.</returns>
        public ActionResult Log()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Action Result.</returns>
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return this.Json(this.siteActivityLogService.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}
