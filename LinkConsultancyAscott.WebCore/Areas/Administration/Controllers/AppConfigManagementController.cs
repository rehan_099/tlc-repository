﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfigManagementController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the App Config Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Web.Mvc;
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.ViewModels.Administration;
    using LinkConsultancyAscott.WebCore.Controllers;

    /// <summary>
    /// The App Config controller.
    /// </summary>
    [HandleError]
    [Authorize(Roles = Service.Models.ApplicationRoles.Administrators + "," + Service.Models.ApplicationRoles.Developers)]
    public class AppConfigManagementController :
        LookUpControllerBase<int, AppConfig, AppConfigViewModel, IAppConfigRepository>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigManagementController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public AppConfigManagementController(IAppConfigRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
            this.Message = "Edit App config";
        }

        /// <summary>
        /// Gets the visible drop down list.
        /// </summary>
        /// <returns>Returns JsonResult.</returns>
        public override JsonResult GetVisibleDropDownList()
        {
            return null;
        }
    }
}
