﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoggingController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Logging Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Services.LoggingService;
    using LinkConsultancyAscott.WebCore.Controllers;

    /// <summary>
    /// Class AdministrationUserController.
    /// </summary>
    [HandleError]
    [Authorize(Roles = ApplicationRoles.Developers)]
    public class LoggingController : AbstractControllerBase
    {
        /// <summary>
        /// The Logging service
        /// </summary>
        private readonly ILoggingService loggingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingController"/> class.
        /// </summary>
        /// <param name="loggingService">The logging service.</param>
        public LoggingController(
            ILoggingService loggingService)
        {
            this.loggingService = loggingService;
        }

        /// <summary>
        /// The trace list.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Log()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Action Result.</returns>
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return this.Json(this.loggingService.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Uniques the specified field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Unique(string field)
        {
            var result = this.loggingService.Unique(field);

            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
