﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdministrationController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AdministrationController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Web.Mvc;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Types;

    using WebCore.Controllers;

    /// <summary>
    /// Class AdministrationController.
    /// </summary>
    [Authorize(Roles = ApplicationRoles.Administrators + "," + ApplicationRoles.Developers)]
    public class AdministrationController : AbstractControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdministrationController"/> class.
        /// </summary>
        public AdministrationController()
        {
        }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>Action Result.</returns>
        public ActionResult Index()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "Administration";
            return this.View();
        }

        /// <summary>
        /// Logses this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Logs()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Userses this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Users()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Developments this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Development()
        {
            return this.PartialView();
        }

        /// <summary>
        /// Configurations this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult Configuration()
        {
            return this.PartialView();
        }
    }
}
