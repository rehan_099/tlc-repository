﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTemplateManagementController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Requirement Email Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.WebCore.Areas.Administration.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Repository.EmailTemplateRepository;
    using LinkConsultancyAscott.Service.ViewModels;
    using LinkConsultancyAscott.Service.ViewModels.Administration;
    using LinkConsultancyAscott.WebCore.Controllers;

    /// <summary>
    /// The Requirement Email controller.
    /// </summary>
    [HandleError]
    [Authorize(Roles = ApplicationRoles.Administrators + "," + ApplicationRoles.Developers)]
    public class EmailTemplateManagementController : 
        LookUpControllerBase<int, EmailTemplate, EmailTemplateViewModel, IEmailTemplateRepository>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTemplateManagementController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public EmailTemplateManagementController(IEmailTemplateRepository repository, IUnitOfWork unitOfWork)
            : base(repository, unitOfWork)
        {
            this.Message = "Edit Email Template";
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public override ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            var viewModelList = this.Repository.GetAll().Select(log =>
              new EmailTemplateViewModel
              {
                  Id = log.Id,
                  Name = log.Name,
                  CreatedAt = log.CreatedAt,
                  CreatedBy = log.CreatedBy,
                  DeletedAt = log.DeletedAt,
                  DeletedBy = log.DeletedBy,
                  DisplayOrder = log.DisplayOrder,
                  LastModifiedAt = log.LastModifiedAt,
                  LastModifiedBy = log.LastModifiedBy,
                  Visible = log.Visible,
                  EmailText = log.EmailText,
                  EmailSubject = log.EmailSubject,
              });

            return this.Json(viewModelList.ToDataSourceResult(request));
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public override ActionResult GetVisible([DataSourceRequest] DataSourceRequest request)
        {
            var viewModelList = this.Repository.GetAllVisible().Select(log =>
              new EmailTemplateViewModel
              {
                  Id = log.Id,
                  Name = log.Name,
                  CreatedAt = log.CreatedAt,
                  CreatedBy = log.CreatedBy,
                  DeletedAt = log.DeletedAt,
                  DeletedBy = log.DeletedBy,
                  DisplayOrder = log.DisplayOrder,
                  LastModifiedAt = log.LastModifiedAt,
                  LastModifiedBy = log.LastModifiedBy,
                  Visible = log.Visible,
                  EmailText = log.EmailText,
                  EmailSubject = log.EmailSubject
              });

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the visible drop down list.
        /// </summary>
        /// <returns>JsonResult.</returns>
        [HttpPost]
        public override JsonResult GetVisibleDropDownList()
        {
            var viewModelList = this.Repository.GetAllVisible().Select(log =>
              new FilterListViewModel
              {
                  Id = log.Id,
                  Name = log.Name
              });

            return this.Json(viewModelList, JsonRequestBehavior.AllowGet);
        }
    }
}
