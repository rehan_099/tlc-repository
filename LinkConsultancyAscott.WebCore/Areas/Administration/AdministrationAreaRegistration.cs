﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdministrationAreaRegistration.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class AdministrationAreaRegistration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.WebCore.Areas.Administration
{
    using System.Web.Mvc;

    /// <summary>
    /// Class AdministrationAreaRegistration.
    /// </summary>
    /// <seealso cref="AreaRegistration" />
    public class AdministrationAreaRegistration : AreaRegistration
    {
        /// <summary>
        /// Gets the name of the area to register.
        /// </summary>
        /// <value>The name of the area.</value>
        public override string AreaName => "Administration";

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Administration_default",
                "Administration/{controller}/{action}/{id}",
                new { controller = "Administration", action = "Index", id = UrlParameter.Optional });
        }
    }
}