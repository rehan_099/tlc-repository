﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclController.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the ACL Controller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Areas.AccessManagement.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Kendo.Mvc.Extensions;
    using Kendo.Mvc.UI;

    using LinkConsultancyAscott.Service.Extensions;
    using LinkConsultancyAscott.Service.Services.AclService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    using Service.Infrastructure;
    using Service.Models;

    using WebCore.Controllers;

    /// <summary>
    /// Class CDR Data Pack Upload Controller.
    /// </summary>
    [Authorize(Roles = ApplicationRoles.Users)]
    public class AclController : AbstractControllerBase
    {
        /// <summary>
        /// The CDR data pack upload service
        /// </summary>
        private readonly IAclService aclService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AclController"/> class.
        /// </summary>
        /// <param name="aclService">The service.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public AclController(IAclService aclService, IUnitOfWork unitOfWork)
        {
            this.aclService = aclService;
        }

        /// <summary>
        /// Index Page
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [HttpGet]
        public ActionResult AclHistory()
        {
            this.ViewBag.Title = AppConstants.AppTitle + "ACL History";
            return this.View();
        }

        /// <summary>
        /// Lists the specified ACL list type.
        /// </summary>
        /// <param name="aclListType">Type of the ACL list.</param>
        /// <returns>Return ActionResult.</returns>
        public ActionResult AclList(AclListType aclListType)
        {
            this.ViewBag.Title = AppConstants.AppTitle + aclListType.GetDescription();
            return this.View(aclListType);
        }

        /// <summary>
        /// Cataleyas the ACL list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaAclList()
        {
            return this.View();
        }

        /// <summary>
        /// Cataleyas the sip interface list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaSipInterfaceList()
        {
            return this.View();
        }

        /// <summary>
        /// Cataleyas the IP interface list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaIpInterfaceList()
        {
            return this.View();
        }

        /// <summary>
        /// Cataleyas the media interface list.
        /// </summary>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public ActionResult CataleyaMediaInterfaceList()
        {
            return this.View();
        }

        /// <summary>
        /// Reads the cataleya acl list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns ActionResult.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaAclList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaAclList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the cataleya sip interface list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns Task&lt;ActionResult&gt;.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaSipInterfaceList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaSipInterfaceList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the cataleya IP interface list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns Task&lt;ActionResult&gt;.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaIpInterfaceList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaIpInterfaceList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the cataleya media interface list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Task&lt;ActionResult&gt;.</returns>
        [Authorize(Roles = ApplicationRoles.Administrators)]
        public async Task<ActionResult> ReadCataleyaMediaInterfaceList([DataSourceRequest] DataSourceRequest request)
        {
            var list = await this.aclService.ReadCataleyaMediaInterfaceList();
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Returns ActionResult.</returns>
        public virtual ActionResult ReadAclHistory([DataSourceRequest] DataSourceRequest request)
        {
            var showAllRows = this.User.IsInRole(ApplicationRoles.Administrators) || this.User.IsInRole(ApplicationRoles.Developers);

            var list = this.aclService.ReadAclHistory(this.UserName, showAllRows);
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// Destroys the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Returns ActionResult.</returns>
        [HttpPost]
        public ActionResult DestroyAclHistory([DataSourceRequest] DataSourceRequest request, AclHistoryViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                this.aclService.DestroyAclHistory(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <returns>Returns ActionResult.</returns>
        public ActionResult ReadAclList([DataSourceRequest] DataSourceRequest request, AclListType aclListType)
        {
            var showAllRows = this.User.IsInRole(ApplicationRoles.Administrators) || this.User.IsInRole(ApplicationRoles.Developers);

            var list = this.aclService.ReadAclList(aclListType, this.UserName, showAllRows);
            return this.Json(list.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Inserts the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual async Task<ActionResult> CreateAclList([DataSourceRequest] DataSourceRequest request, AclListViewModel viewModel)
        {
            AclListViewModel newViewModel;

            // Perform model binding (fill the product properties and validate it).           
            if (viewModel != null && this.ModelState.IsValid)
            {
                // The model is valid - insert the product. 
                newViewModel = await this.aclService.CreateAclList(viewModel, this.UserName).ConfigureAwait(false);
            }
            else
            {
                newViewModel = viewModel;
            }

            // Rebind the grid       
            return this.Json(new[] { newViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Updates the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public virtual ActionResult UpdateAclList([DataSourceRequest] DataSourceRequest request, AclListViewModel viewModel)
        {
            AclListViewModel updatedViewModel;

            if (viewModel != null && this.ModelState.IsValid)
            {
                updatedViewModel = this.aclService.UpdateAclList(viewModel);
            }
            else
            {
                updatedViewModel = viewModel;
            }

            return this.Json(new[] { updatedViewModel }.ToDataSourceResult(request, this.ModelState));
        }

        /// <summary>
        /// Deletes the look up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult DestroyAclList([DataSourceRequest] DataSourceRequest request, AclListViewModel viewModel)
        {
            // Create a new instance of the EditableProduct class.          
            if (viewModel != null)
            {
                this.aclService.DestroyAclList(viewModel);
            }

            // Rebind the grid       
            return this.Json(new[] { viewModel }.ToDataSourceResult(request, this.ModelState));
        }


        [HttpGet]
        public ActionResult UploadIpAddressFile(AclListType listType)
        {
            var viewModel = new UploadIpAddressFileViewModel
            {
                ListType = listType,
                ListTypeName = listType.GetDescription()
            };

            return this.PartialView(viewModel);
        }


        /// <summary>
        /// Saves the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveIpAddressFile(UploadIpAddressFileViewModel viewModel)
        {
            // The Name of the Upload component is "files"
            if (viewModel != null)
            {
                this.aclService.SaveIpAddressFile(viewModel, this.UserName, this.BaseUrl);
            }

            // Return an empty string to signify success
            return this.Content(string.Empty);
        }

        public ActionResult RemoveIpAddressFile(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {

                }
            }

            // Return an empty string to signify success
            return Content(string.Empty);
        }

    }
}
