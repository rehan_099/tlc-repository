﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RuntimeUtils.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Runtime Utils class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore.Utilites
{
    using System;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// The Runtime Utils class.
    /// </summary>
    public static class RuntimeUtils
    {
        /// <summary>
        /// The log
        /// </summary>
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gets the assembly configuration string.
        /// </summary>
        /// <returns>System String.</returns>
        public static string GetAssemblyConfiguration()
        {
            string assemblyDescription = string.Empty;
            var asm = Assembly.GetAssembly(typeof(RuntimeUtils));

            var assemblyConfigurationAttribute = asm.GetCustomAttribute<AssemblyConfigurationAttribute>();

            if (assemblyConfigurationAttribute != null)
            {
                assemblyDescription = assemblyConfigurationAttribute.Configuration;
            }

            return assemblyDescription;
        }

        /// <summary>
        /// Determines whether [is web application deployed to production] [the specified request].
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><c>true</c> if [is web application deployed to production] [the specified request]; otherwise, <c>false</c>.</returns>
        public static bool IsWebAppDeployedToProduction(HttpRequestBase request)
        {
            bool deployedToProduction;
            if (request.Url != null)
            {
                var host = request.Url.Host.ToLower();
                deployedToProduction = Regex.IsMatch(host, ".*(pthengopwebprod|pthengopwebpreprod|pthvmengop603).*");
            }
            else
            {
                deployedToProduction = false;
            }

            return deployedToProduction;
        }

        /// <summary>
        /// Builds the base URL.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="url">The URL.</param>
        /// <returns>System String.</returns>
        public static string BuildBaseUrl(Uri uri, System.Web.Mvc.UrlHelper url)
        {
            var content = string.Empty;
            if (url != null)
            {
                content = url.Content("~");
            }

            return BuildBaseUrl(uri, content);
        }

        /// <summary>
        /// Builds the base URL.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns>System String.</returns>
        public static string BuildBaseUrl(Uri uri)
        {
            string baseUrl;
            var leftPart = new Uri(uri.GetLeftPart(UriPartial.Authority)).ToString();
            var pathPart = new Uri(uri.GetLeftPart(UriPartial.Path)).ToString();

            var webApiCall = pathPart.Replace(leftPart, string.Empty);

            // Check if this is an MVC or WebAPI call
            if (!webApiCall.StartsWith("api/"))
            {
                // On a sub site
                // Get the sub site part
                var domainEnd = webApiCall.IndexOf("/", StringComparison.InvariantCultureIgnoreCase);
                var subSite = webApiCall.Substring(0, domainEnd);
                baseUrl = leftPart + "/" + subSite;
            }
            else
            {
                // Non-sub site- just use the URL
                baseUrl = leftPart;
            }

            return baseUrl;
        }

        /// <summary>
        /// Builds the base URL.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="content">The content.</param>
        /// <returns>System String.</returns>
        private static string BuildBaseUrl(Uri uri, string content)
        {
            // Grab REAL app root URL address and setup Ajax to prepend this to all called URLs...
            // This needs to work both when debugging in IISExpress from Visual Studio AND when deployed to IIS on a server!
            var baseUrl = (uri != null && !string.IsNullOrEmpty(content))
              ? new Uri(new Uri(uri.GetLeftPart(UriPartial.Authority)), content).ToString()
              : string.Empty;

            return baseUrl;
        }
    }
}