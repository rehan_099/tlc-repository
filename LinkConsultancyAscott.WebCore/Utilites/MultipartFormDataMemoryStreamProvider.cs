﻿namespace LinkConsultancyAscott.WebCore.Utilites
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    public class MultipartFormDataMemoryStreamProvider : MultipartMemoryStreamProvider
    {
        /// <summary>
        /// The _is form data.
        /// </summary>
        private readonly Collection<bool> _isFormData = new Collection<bool>();

        /// <summary>
        /// The _form data.
        /// </summary>
        private readonly NameValueCollection _formData = new NameValueCollection(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The _file streams.
        /// </summary>
        private readonly Dictionary<string, Stream> _fileStreams = new Dictionary<string, Stream>();

        /// <summary>
        /// Gets the form data.
        /// </summary>
        /// <value>The form data.</value>
        public NameValueCollection FormData => this._formData;

        /// <summary>
        /// Gets the file streams.
        /// </summary>
        /// <value>The file streams.</value>
        public Dictionary<string, Stream> FileStreams => this._fileStreams;

        /// <summary>
        /// Returns the <see cref="T:System.IO.Stream" /> for the <see cref="T:System.Net.Http.MultipartMemoryStreamProvider" />.
        /// </summary>
        /// <param name="parent">A <see cref="T:System.Net.Http.HttpContent" /> object.</param>
        /// <param name="headers">The HTTP content headers.</param>
        /// <returns>The <see cref="T:System.IO.Stream" /> for the <see cref="T:System.Net.Http.MultipartMemoryStreamProvider" />.</returns>
        /// <exception cref="ArgumentNullException">
        /// parent
        /// or
        /// headers
        /// </exception>
        /// <exception cref="InvalidOperationException">Did not find required 'Content-Disposition' header field in MIME multipart body part.</exception>
        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }

            var contentDisposition = headers.ContentDisposition;
            if (contentDisposition == null)
            {
                throw new InvalidOperationException("Did not find required 'Content-Disposition' header field in MIME multipart body part.");
            }

            this._isFormData.Add(string.IsNullOrEmpty(contentDisposition.FileName));
            return base.GetStream(parent, headers);
        }

        /// <summary>
        /// execute post processing as an asynchronous operation.
        /// </summary>
        /// <returns>The asynchronous task for this operation.</returns>
        public override async Task ExecutePostProcessingAsync()
        {
            for (var index = 0; index < this.Contents.Count; index++)
            {
                HttpContent formContent = this.Contents[index];
                if (this._isFormData[index])
                {
                    // Field
                    string formFieldName = UnquoteToken(formContent.Headers.ContentDisposition.Name) ?? string.Empty;
                    string formFieldValue = await formContent.ReadAsStringAsync();
                    this.FormData.Add(formFieldName, formFieldValue);
                }
                else
                {
                    // File
                    string fileName = UnquoteToken(formContent.Headers.ContentDisposition.FileName);
                    Stream stream = await formContent.ReadAsStreamAsync();
                    this.FileStreams.Add(fileName, stream);
                }
            }
        }

        /// <summary>
        /// Unquotes the token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>System.String.</returns>
        private static string UnquoteToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return token;
            }

            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1)
            {
                return token.Substring(1, token.Length - 2);
            }

            return token;
        }
    }
}