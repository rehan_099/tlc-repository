﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the MvcApplication type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore
{
    using System;
    using System.Threading;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;

    /// <summary>
    /// Class MvcApplication.
    /// </summary>
    /// <seealso cref="System.Web.HttpApplication" />
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Applications the start.
        /// </summary>
        protected void Application_Start()
        {
            Logger.Info("Entered Application_Start application on Thread [" + Thread.CurrentThread.ManagedThreadId + "]");


            this.Error += this.MvcApplicationError;

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Logger.Info("Exited Application_Start  application on Thread [" + Thread.CurrentThread.ManagedThreadId + "]");
        }

        /// <summary>
        /// MVCs the application error.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MvcApplicationError(object sender, EventArgs e)
        {
            var exception = this.Server.GetLastError();
            Logger.Error("Entered MvcApplicationError application on Thread [" + Thread.CurrentThread.ManagedThreadId + "]");
            Logger.Error(exception.Message);
            Logger.Error(exception.ToString());
            Logger.Error(exception.StackTrace);
            Logger.Error("Exited  MvcApplicationError application on Thread [" + Thread.CurrentThread.ManagedThreadId + "]");
        }


        /// <summary>
        /// Application_s the end.
        /// </summary>
        protected void Application_End()
        {
            var endReason = System.Web.Hosting.HostingEnvironment.ShutdownReason;

            Logger.Info("Ending application on Thread [" + Thread.CurrentThread.ManagedThreadId + "] Reason [" + endReason + "]");
        }

        /// <summary>
        /// Application_s the error.
        /// </summary>
        protected void Application_Error()
        {
            var exception = this.Server.GetLastError();
            Logger.Error("Entered Application_Error LinkConsultancyAscott application on Thread [" + Thread.CurrentThread.ManagedThreadId + "]");
            Logger.Error(exception.Message);
            Logger.Error(exception.ToString());
            Logger.Error(exception.StackTrace);
            Logger.Error("Exited  Application_Error LinkConsultancyAscott application on Thread [" + Thread.CurrentThread.ManagedThreadId + "]");
        }
    }
}
