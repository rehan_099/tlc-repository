﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Startup.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Startup type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.Owin;

using LinkConsultancyAscott.WebCore;

[assembly: OwinStartup(typeof(Startup))]
namespace LinkConsultancyAscott.WebCore
{
    using Owin;

    /// <summary>
    /// Class Startup.
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Configurations the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app)
        {
            Logger.Info("Starting SignalR...");
            // Initialise SignalR
            app.MapSignalR();
            Logger.Info("...SignalR started!");

            ConfigureAuth(app);
        }
    }
}
