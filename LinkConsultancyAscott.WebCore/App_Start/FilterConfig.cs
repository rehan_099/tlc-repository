﻿namespace LinkConsultancyAscott.WebCore
{
    using System.Web.Mvc;

    using LinkConsultancyAscott.WebCore.Filters;

    /// <summary>
    /// Class FilterConfig.
    /// </summary>
    public static class FilterConfig
    {
        /// <summary>
        /// Registers the global filters.
        /// </summary>
        /// <param name="filters">The filters.</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuditableActionFilterAttribute());
            filters.Add(new UserTrackingActionFilterAttribute());
        }
    }
}
