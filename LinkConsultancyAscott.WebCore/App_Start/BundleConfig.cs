﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BundleConfig.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class BundleConfig.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.WebCore
{
    using System.Web.Optimization;

    /// <summary>
    /// Class BundleConfig.
    /// </summary>
    public static class BundleConfig
    {
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Style Bundles
            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                                        "~/Content/bootstrap.min.css"));

            bundles.Add(new StyleBundle($"~/Content/kendo/{KendoSettings.KendoVersion}/css").Include(
                $"~/Content/kendo/{KendoSettings.KendoVersion}/kendo.common.min.css",
                $"~/Content/kendo/{KendoSettings.KendoVersion}/kendo.common-{KendoSettings.KendoTheme}.core.min.css",
                $"~/Content/kendo/{KendoSettings.KendoVersion}/kendo.common-{KendoSettings.KendoTheme}.min.css",
                $"~/Content/kendo/{KendoSettings.KendoVersion}/kendo.{KendoSettings.KendoTheme}.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/fonts/Roboto/roboto.css",
                      "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/logincss").Include(
                      "~/fonts/Roboto/roboto.css",
                      "~/Content/login.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/foolproof/mvcfoolproof.unobtrusive*",
                        "~/Scripts/foolproof/MvcFoolproofJQueryValidation*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/umd/popper.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/respond.min.js",
                        "~/Scripts/typescript/RootUrlHelper.js"));

            bundles.Add(new ScriptBundle("~/bundles/fontawesome").Include(
                       "~/Scripts/fontawesome-all*"));

            bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
                    "~/Scripts/jquery.signalR-2.2.2.min.js",
                    "~/Scripts/typescript/LinkConsultancyAscott.signalr.js",
                    "~/Scripts/typescript/LinkConsultancyAscott.signalr.manager.js"));

            bundles.Add(new ScriptBundle("~/bundles/global").Include(
                 "~/Scripts/globalize/globalize.js",
                 "~/Scripts/keypress*",
                 "~/Scripts/humanize.min.js",
                 "~/Scripts/typescript/LinkConsultancyAscott.notification.js",
                 "~/Scripts/typescript/LinkConsultancyAscott.common.js",
                 "~/Scripts/typescript/LinkConsultancyAscott.common.view.models.js",
                 "~/Scripts/typescript/LinkConsultancyAscott.app.js",
                 "~/Scripts/typescript/LinkConsultancyAscott.report.viewer.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                    "~/Scripts/kendo/" + KendoSettings.KendoVersion + "/jszip.min.js",
                    "~/Scripts/kendo/" + KendoSettings.KendoVersion + "/kendo.all.min.js",
                    "~/Scripts/kendo/" + KendoSettings.KendoVersion + "/kendo.aspnetmvc.min.js",
                    "~/Scripts/kendo/" + KendoSettings.KendoVersion + "/cultures/kendo.culture.en-GB.min.js",
                    "~/Scripts/typescript/LinkConsultancyAscott.errors.js"));

            bundles.Add(new ScriptBundle("~/bundles/administration").Include(
                                       "~/Scripts/Typescript/LinkConsultancyAscott.partial.base.js",
                                       "~/Scripts/Typescript/Administration/AdministrationBase.js",
                                       "~/Scripts/Typescript/Administration/Administration.js",
                                       "~/Scripts/Typescript/Administration/AdministrationUser.js"));
        }
    }
}