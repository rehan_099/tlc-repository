// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityMvcActivator.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Provides the bootstrapping for integrating Unity with ASP.NET MVC.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using LinkConsultancyAscott.WebCore;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(UnityWebActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(UnityWebActivator), "Shutdown")]

namespace LinkConsultancyAscott.WebCore
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.Mvc;

    using LinkConsultancyAscott.WebCore.Utilities;

    using Microsoft.AspNet.SignalR;

    using Unity.AspNet.Mvc;

    /// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
    public static class UnityWebActivator
    {
        /// <summary>Integrates Unity when the application starts.</summary>
        public static void Start() 
        {
            var container = UnityConfig.GetConfiguredContainer();

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            // used for SignalR
            GlobalHost.DependencyResolver = new SignalRUnityDependencyResolver(container);

            // Plug in the WebAPI IoC container
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }

        /// <summary>Disposes the Unity container when the application is shut down.</summary>
        public static void Shutdown()
        {
            var container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }
    }
}