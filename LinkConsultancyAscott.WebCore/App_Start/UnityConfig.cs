// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityConfig.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Specifies the Unity configuration for the main container.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.WebCore
{
    using System;
    using System.Configuration;

    using LinkConsultancyAscott.Service.Repository.AclEntityTypeRepository;

    using Service.Infrastructure;
    using Service.Repository.AuditLogRepository;
    using Service.Repository.AclRepository;

    using Service.Repository.EmailTemplateRepository;
    using Service.Repository.EmailTypeRepository;

    using Service.Repository.LogRepository;
    using Service.Repository.RoleRepository;
    using Service.Repository.SiteActivityLogRepository;
    using Service.Repository.UserRepository;
    using Service.Services.AuditLogService;
    using Service.Services.CacheService;
    using Service.Services.EmailService;
    using Service.Services.HomeService;
    using Service.Services.LoggingService;
    using Service.Services.MessengerService;
    using Service.Services.SiteActivityLogService;
    using Service.Services.StaticDataService;
    using Service.Services.UserService;

    using Unity;
    using Unity.AspNet.Mvc;
    using Unity.Injection;
    using Unity.Lifetime;
    using LinkConsultancyAscott.Service.Repository.AclSourceTypeRepository;
    using LinkConsultancyAscott.Service.Repository.AclListTypeRepository;
    using LinkConsultancyAscott.Service.Services.AclService;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.AclUploadService;
    using LinkConsultancyAscott.Service.Repository.AclHistoryRepository;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.WebCore.Services.CurrentUserService;

    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        /// <summary>
        /// The container
        /// </summary>
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterDatabaseFactory(container);
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        /// <returns>Returns IUnityContainer.</returns>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="unityContainer">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer unityContainer)
        {
            // Repositories
            unityContainer
                .RegisterType<IAppConfigRepository, AppConfigRepository>()
                .RegisterType<IAuditLogRepository, AuditLogRepository>()

                .RegisterType<IAclEntityTypeRepository, AclEntityTypeRepository>()
                .RegisterType<IAclListTypeRepository, AclListTypeRepository>()
                .RegisterType<IAclSourceTypeRepository, AclSourceTypeRepository>()
                .RegisterType<IAclRepository, AclRepository>()

                .RegisterType<IAclHistoryRepository, AclHistoryRepository>()
                .RegisterType<IEmailTemplateRepository, EmailTemplateRepository>()
                .RegisterType<IEmailTypeRepository, EmailTypeRepository>()
                
                .RegisterType<ILogRepository, LogRepository>()
                .RegisterType<IRoleRepository, RoleRepository>()
                .RegisterType<ISiteActivityLogRepository, SiteActivityLogRepository>()
                .RegisterType<IUserRepository, UserRepository>()

                // Services
                .RegisterType<IAclUploadService, AclUploadService>()
                .RegisterType<IAuditLogService, AuditLogService>()
                .RegisterType<ICacheService, CacheService>(new ContainerControlledLifetimeManager())
                //.RegisterType<ICataleyaService, CataleyaDummyService>()
                .RegisterType<ICataleyaService, CataleyaService>()
                .RegisterType<ICurrentUserService, CurrentUserService>()
                .RegisterType<IAclService, AclService>()
                .RegisterType<IEmailService, Service.Services.EmailService.EmailService>()
                .RegisterType<IHomeService, HomeService>()
                .RegisterType<ILoggingService, LoggingService>()
                .RegisterType<IMessengerService, MessengerService>()
                .RegisterType<ISiteActivityLogService, SiteActivityLogService>()
                .RegisterType<IStaticDataService, StaticDataService>()
                .RegisterType<IUserService, UserService>();
        }

        /// <summary>
        /// Registers the database factory.
        /// </summary>
        /// <param name="container">The container.</param>
        private static void RegisterDatabaseFactory(IUnityContainer container)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["LinkConsultancyAscottEntities"].ConnectionString;

            container
                .RegisterType<IDatabaseFactory, DatabaseFactory>(
                    new PerRequestLifetimeManager(),
                    new InjectionConstructor(connectionString))
                .RegisterType<IUnitOfWork, UnitOfWork>(new PerRequestLifetimeManager());
        }
    }
}
