// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RazorGeneratorMvcStart.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the RazorGeneratorMvcStart type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using RazorGenerator.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(LinkConsultancyAscott.WebCore.RazorGeneratorMvcStart), "Start")]

namespace LinkConsultancyAscott.WebCore
{
    /// <summary>
    /// Class RazorGeneratorMvcStart.
    /// </summary>
    public static class RazorGeneratorMvcStart
    {
        /// <summary>
        /// Starts this instance.
        /// </summary>
        public static void Start()
        {
            var engine = new PrecompiledMvcEngine(typeof(RazorGeneratorMvcStart).Assembly)
            {
                UsePhysicalViewsIfNewer = HttpContext.Current.Request.IsLocal
            };

            ViewEngines.Engines.Insert(0, engine);

            // StartPage lookups are done by WebPages. 
            VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);
        }
    }
}
