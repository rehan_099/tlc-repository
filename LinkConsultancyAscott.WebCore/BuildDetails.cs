﻿namespace LinkConsultancyAscott.WebCore
{
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Class to enable _Layout.cshtml page to read the embedded resource BuildDetails.xml file.
    /// </summary>
    public static class BuildDetails
    {
        private static XElement xml;

        /// <summary>
        /// Initializes static members of the <see cref="BuildDetails"/> class.
        /// </summary>
        static BuildDetails()
        {
            xml =
                XElement.Load(
                    Assembly.GetExecutingAssembly().GetManifestResourceStream("LinkConsultancyAscott.WebCore.BuildDetails.xml"));

        }

        public static string Html
        {
            get
            {
                var stringBuilder = new StringBuilder("<h5 id='buildLabel'>" + BuildLabel);
                stringBuilder.Append(string.IsNullOrEmpty(BuildDateTime) ? string.Empty : "<br/><span style='font-size: 9pt'>Build Date: " + BuildDateTime + "</span>");
                stringBuilder.Append(string.IsNullOrEmpty(SourceVersion) ? string.Empty : "<br/><span style='font-size: 9pt'>Changeset: " + SourceVersion + "</span>");
                stringBuilder.Append(SnapshotName == "NONE" ? string.Empty : "<br/><span style='font-size: 9pt'>Snapshot: " + SnapshotName + "</span>");
                stringBuilder.Append("</h5>");

                return stringBuilder.ToString();
            }
        }

        public static string BuildLabel
        {
            get
            {
                var node = xml.Descendants("BuildLabel").SingleOrDefault();
                return node?.Value ?? string.Empty;
            }
        }

        public static string BuildDateTime
        {
            get
            {
                var node = xml.Descendants("BuildDateTime").SingleOrDefault();
                return node?.Value ?? string.Empty;
            }
        }

        public static string BuildConfiguration
        {
            get
            {
                var node = xml.Descendants("BuildConfiguration").SingleOrDefault();
                return node == null ? string.Empty : node.Value;
            }
        }

        public static string SourceVersion
        {
            get
            {
                var node = xml.Descendants("SourceVersion").SingleOrDefault();
                return node == null ? string.Empty : node.Value;
            }
        }

        public static string SnapshotName
        {
            get
            {
                var node = xml.Descendants("SnapshotName").SingleOrDefault();
                return node == null ? string.Empty : node.Value;
            }
        }
    }
}