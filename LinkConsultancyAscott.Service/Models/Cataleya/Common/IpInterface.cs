﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{
    public class IpInterface
    {
        public CompoundKey compoundKey  { get; set; }
        public string ipaddress { get; set; }
        public int ipSubnetId { get; set; }
        public int ver { get; set; }
        public string vlanName { get; set; }
        public int? vlanId { get; set; }
        public string addressType { get; set; }
        public int nodeId { get; set; }
        public int? operatorId { get; set; }
        public int id { get; set; }
    }
}