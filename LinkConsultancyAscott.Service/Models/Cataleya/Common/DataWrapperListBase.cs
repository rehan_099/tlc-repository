﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataWrapperListBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the DataWrapperListBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{
    using System.Collections.Generic;

    /// <summary>
    /// Class DataWrapperListBase.
    /// </summary>
    /// <typeparam name="T"> The data type </typeparam>
    public class DataWrapperListBase<T> : DataWrapperBase<IList<T>>
    {
    }
}
