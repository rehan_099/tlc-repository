﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataWrapperBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the DataWrapperBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{
    using System.Collections.Generic;

    /// <summary>
    /// Class DataWrapperListBase.
    /// </summary>
    /// <typeparam name="T"> Type of list element</typeparam>
    public class DataWrapperBase<T>
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DataWrapperListBase{T}"/> is success.
        /// </summary>
        /// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
        public bool success { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string message { get; set; }

        /// <summary>
        /// Gets or sets the detail error message.
        /// </summary>
        /// <value>The detail error message.</value>
        public string detailErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>The total.</value>
        public int? total { get; set; }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>The page number.</value>
        public int? pageNumber { get; set; }

        /// <summary>
        /// Gets or sets the results per page.
        /// </summary>
        /// <value>The results per page.</value>
        public int? resultsPerPage { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        public T data { get; set; }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>The object identifier.</value>
        public int? objId { get; set; }
    }
}
