﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{
    using System.Collections.Generic;

    public class Operator
    {
        public int id { get; set; }
        public string name { get; set; }
        public object description { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public object partitionCacId { get; set; }
        public int sdrCtrlParamsId { get; set; }
        public object contactDetails { get; set; }
        public IList<object> mediations { get; set; }
        public object nodeId { get; set; }
        public int operatorId { get; set; }
    }
}