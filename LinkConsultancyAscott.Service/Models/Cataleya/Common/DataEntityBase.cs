﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataEntityBase.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the DataEntityBase.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Common
{

    /// <summary>
    /// Class DataEntityBase.
    /// </summary>
    public class DataEntityBase
    {
        public CompoundKey compoundKey { get; set; }

        public int ver { get; set; }

        public int? operatorId { get; set; }

        public int nodeId { get; set; }

        public int id { get; set; }
    }
}
