﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class TransparencyProfile : DataEntityBase
    {

        public string name { get; set; }

        public string description { get; set; }


        public string topology { get; set; }

        public string callId { get; set; }

        public string header { get; set; }

        public string body { get; set; }

        public Operator @operator { get; set; }

    }
}