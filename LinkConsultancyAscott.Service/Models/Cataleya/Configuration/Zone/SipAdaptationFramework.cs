﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class SipAdaptationFramework : DataEntityBase
    {

        public string name { get; set; }

        public string description { get; set; }


        public object sipAllowHeaders { get; set; }

        public string sipRule { get; set; }

        public int chmod { get; set; }
        public Operator @operator { get; set; }
    }
}