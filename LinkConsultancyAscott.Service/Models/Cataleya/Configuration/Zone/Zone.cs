﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using System.Collections.Generic;

    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class Zone : DataEntityBase
    {
        public string name { get; set; }

        public string description { get; set; }

        public string customer { get; set; }

        public int? egressZone { get; set; }

        public int? routingPolicy { get; set; }

        public int? registrationPolicy { get; set; }

        public string redirectMode { get; set; }

        public int maxReroutes { get; set; }

        public object localDomain { get; set; }

        public string zoneType { get; set; }

        public object ioi { get; set; }

        public string tgId { get; set; }

        public string trunkGroupType { get; set; }

        public string timeZone { get; set; }

        public int? siprulesId { get; set; }

        public string serviceLevel { get; set; }

        public object signalingTos { get; set; }

        public int? mediaTos { get; set; }

        public string trustLevel { get; set; }

        public int blkInterval { get; set; }

        public int blkRegInterval { get; set; }

        public int blkThInvSessionPct { get; set; }

        public int blkThMalformedMsgRate { get; set; }

        public int blkThNonInvTxnPct { get; set; }

        public int blkThRegRejCount { get; set; }

        public int blkThRegTxnPct { get; set; }

        public object tlsprofileId { get; set; }

        public string privacy { get; set; }

        public string callingPtyIdHdr { get; set; }

        public string qosAnalysis { get; set; }

        public int networkRtDelay { get; set; }

        public string operationalState { get; set; }

        public string adminState { get; set; }

        public int sipprofileId { get; set; }

        public int mediaprofileId { get; set; }

        public int cacprofileId { get; set; }

        public string xcodEnabled { get; set; }

        public object xcodprofileId { get; set; }

        public string traceOn { get; set; }

        public int? transparencyprofileId { get; set; }

        public int maxCallDuration { get; set; }

        public int? gracefulPeriod { get; set; }

        public int? chmod { get; set; }

        public string siptEnabled { get; set; }

        public string isupBase { get; set; }

        public string isupVer { get; set; }

        public object servicePolicyId { get; set; }

        public string ldcHandlerType { get; set; }

        public int? mediaInactivityTimer { get; set; }

        public string mediaInactivityPolicy { get; set; }

        public int? byeTimer { get; set; }

        public string trustDomain { get; set; }

        public string qosPreconditionEnabled { get; set; }

        public object segmentedPreconditions { get; set; }

        public IList<object> e2ePreconditions { get; set; }

        public object iwfProfileId { get; set; }

        public string sendQosReport { get; set; }

        public object qosAnalysisRptDuration { get; set; }

        public object qosAnalysisGap { get; set; }

        public object iDmRulesId { get; set; }

        public object eDmRulesId { get; set; }

        public object ueCacprofileId { get; set; }

        public string localRingbackType { get; set; }

        public object localRingbackAnnId { get; set; }

        public object allowedPrefixList { get; set; }

        public TransparencyProfile transparencyProfile { get; set; }

        public object tlsProfile { get; set; }

        public object egressZoneObj { get; set; }

        public object registrationPolicyObj { get; set; }

        public object routingPolicyObj { get; set; }

        public SipAdaptationFramework sipAdaptationFramework { get; set; }

        public SipProfile sipProfile { get; set; }

        public MediaProfile mediaProfile { get; set; }

        public CacProfile cacProfile { get; set; }

        public object xcodProfile { get; set; }

        public IList<RemoteEndPoint> remoteEndPoints { get; set; }

        public IList<SourceList> sourceLists { get; set; }

        public IList<MediaInterface.MediaInterface> mediaInterfaces { get; set; }

        public IList<SipInterface.SipInterface> sipInterfaces { get; set; }

        public object servicePolicy { get; set; }

        public object iwfProfile { get; set; }

        public object iDmRules { get; set; }

        public object eDmRules { get; set; }

        public object ueCacProfile { get; set; }

        public object localRingbackAnn { get; set; }

        public string recordDtmfDigits { get; set; }
    }
}


