﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class SipProfile :  DataEntityBase
    {

        public string name { get; set; }


        public string description { get; set; }

        public string methodsallowed { get; set; }

        public int maxRetxCount { get; set; }

        public int timerT1 { get; set; }

        public int timerT2 { get; set; }

        public int timerC { get; set; }

        public int minSessionTimer { get; set; }

        public int sessionTimer { get; set; }

        public string reliableReq { get; set; }

        public string reliableResp { get; set; }

        public int optionsTimer { get; set; }

        public string optionsForwarding { get; set; }

        public int minRegInterval { get; set; }

        public int maxRegInterval { get; set; }

        public string feNatMode { get; set; }

        public int dummyPktInterval { get; set; }

        public string regThrotteling { get; set; }

        public int ueRegInterval { get; set; }

        public int networkRegInterval { get; set; }

        public string refresherType { get; set; }


        public Operator @operator { get; set; }

        public int? chmod { get; set; }

        public string enableSessionTimer { get; set; }
    }
}