﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Zones.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Zones type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// Class Zones.
    /// </summary>
    public class Zones : DataWrapperListBase<Zone>
    {
    }
}
