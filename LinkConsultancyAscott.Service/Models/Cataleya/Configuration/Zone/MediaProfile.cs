﻿namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone
{
    using System.Collections.Generic;

    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    public class MediaProfile : DataEntityBase
    {
        public string name { get; set; }

        public string description { get; set; }

        public string mediaProxy { get; set; }

        public string mediaLatch { get; set; }

        public string codecSelection { get; set; }

        public string dtmfMode { get; set; }

        public Operator @operator {
            get;
            set;
        }

        public IList<object> audioCodecs { get; set; }

        public IList<object> videoCodecs { get; set; }

        public IList<object> imageCodecs { get; set; }

        public string srtpMode { get; set; }

        public string srtpSrtcpEnabled { get; set; }

        public string srtpAllowPassThru { get; set; }

        public string srtpCryptoSuiteList { get; set; }

        public string srtpAuthEnabled { get; set; }

        public int? chmod { get; set; }

        public string keyExchangeMode { get; set; }

        public object dtlsProfileId { get; set; }

        public object dtlsProfile { get; set; }
    }
}