﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MediaInterface.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the MediaInterface type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.MediaInterface
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// The MediaInterface.
    /// </summary>
    public class MediaInterface : DataEntityInterfaceBase
    {
        public int startPort { get; set; }

        public int endPort { get; set; }

        public int bandwidth { get; set; }

        public string bandwidthReserved { get; set; }
    }
}
