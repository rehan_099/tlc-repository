﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SecurityAcl.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the SecurityAcl type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// Class SecurityAcl.
    /// </summary>
    public class SecurityAcl : DataWrapperListBase<Acl>
    {
    }
}
