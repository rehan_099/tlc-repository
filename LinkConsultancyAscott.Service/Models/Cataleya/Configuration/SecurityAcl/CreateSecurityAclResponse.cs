﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateSecurityAclResponse.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the CreateSecurityAclResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// Class CreateSecurityAclResponse.
    /// </summary>
    public class CreateSecurityAclResponse : DataWrapperEntityBase<Acl>
    {
    }
}
