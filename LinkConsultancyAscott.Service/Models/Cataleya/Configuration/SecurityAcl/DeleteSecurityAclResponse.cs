﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeleteSecurityAclResponse.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the DeleteSecurityAclResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// Class DeleteSecurityAclResponse.
    /// </summary>
    public class DeleteSecurityAclResponse : DataWrapperListBase<Acl>
    {
    }
}
