﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SipInterface.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the SipInterface View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface
{
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;

    /// <summary>
    /// The SipInterface.
    /// </summary>
    public class SipInterface : DataEntityInterfaceBase
    {
        public int port { get; set; }
        public string transport { get; set; }
        public string shared { get; set; }
    }
}