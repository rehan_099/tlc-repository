﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessIpAddressResponse.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the ProcessIpAddressResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models.Cataleya.Response
{
    /// <summary>
    /// Class ProcessIpAddressResponse.
    /// </summary>
    public class ProcessIpAddressResponse
    {
        /// <summary>
        /// Gets or sets the dto identifier.
        /// </summary>
        /// <value>The dto identifier.</value>
        public int? DtoId { get; set; }
    }
}
