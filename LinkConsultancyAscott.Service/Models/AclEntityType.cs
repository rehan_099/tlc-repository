﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclEntityType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum EmailType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using System.ComponentModel;

    /// <summary>
    /// Enum AclEntityType
    /// </summary>
    public enum AclEntityType
    {
        [Description("IP Address")]
        IpAddress = 1,

        [Description("SubNet")]
        SubNet = 2
    }
}
