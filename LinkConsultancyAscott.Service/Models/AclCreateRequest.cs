﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclCreateRequest.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Class AclCreateRequest Request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    /// <summary>
    /// Class AclCreateRequest Request.
    /// </summary>
    public class AclCreateRequest : AclRequestBase
    {
        /// <summary>
        /// Gets or sets the filename.
        /// </summary>
        /// <value>The filename.</value>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>The prefix.</value>
        public string Prefix { get; set; }
    }
}
