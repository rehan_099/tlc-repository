﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclSourceType.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum AclSourceType
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Models
{
    using System.ComponentModel;

    /// <summary>
    /// Enum AclSourceType
    /// </summary>
    public enum AclSourceType
    {
        [Description("ISTRA")]
        Istra = 1,

        [Description("Cataleya")]
        Cataleya = 2,

        [Description("ITSPA Snitch")]
        ITSPASnitch = 3,

        [Description("User Defined")]
        UserDefined = 4
    }
}
