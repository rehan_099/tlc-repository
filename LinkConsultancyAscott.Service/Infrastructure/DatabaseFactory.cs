﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseFactory.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the DatabaseFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Infrastructure
{
    /// <summary>
    /// The database factory.
    /// </summary>
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        /// <summary>
        /// The _connection string.
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// The _database.
        /// </summary>
        private Database database;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseFactory"/> class.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        public DatabaseFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="IDatabase"/>.
        /// </returns>
        public IDatabase Get()
        {
            return this.database ?? (this.database = new Database(this.connectionString));
        }

        /// <summary>
        /// Connections the string.
        /// </summary>
        /// <returns>System String.</returns>
        public string ConnectionString()
        {
            return this.connectionString;
        }

        /// <summary>
        /// The dispose core.
        /// </summary>
        protected override void DisposeCore()
        {
            this.database?.Dispose();
        }
    }
}
