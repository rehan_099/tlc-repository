﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the CATALEYA Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CataleyaService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.Extensions;
    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.MediaInterface;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone;
    using LinkConsultancyAscott.Service.Models.Cataleya.Token;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using Newtonsoft.Json;

    /// <summary>
    /// Class CATALEYA Service.
    /// </summary>
    public class CataleyaService : ServiceBase, ICataleyaService
    {
        /// <summary>
        /// The CATALEYA API URL
        /// </summary>
        private readonly string cataleyaApiUrl;

        /// <summary>
        /// The CATALEYA username
        /// </summary>
        private readonly string cataleyaUsername;

        /// <summary>
        /// The CATALEYA password
        /// </summary>
        private readonly string cataleyaPassword;

        /// <summary>
        /// The default node identifier
        /// </summary>
        private readonly int defaultNodeId;

        /// <summary>
        /// The security ACL route
        /// </summary>
        private readonly string securityAclRoute = "api/configuration/security_acl/";

        /// <summary>
        /// The configuration interface route
        /// </summary>
        private readonly string configurationInterfaceRoute = "api/configuration/{0}_interface/";

        /// <summary>
        /// The configuration IP interface route
        /// </summary>
        private readonly string configurationIpInterfaceRoute = "api/configuration/ip_subnet/ip_interface/";

        /// <summary>
        /// The software about route
        /// </summary>
        private readonly string softwareAboutRoute = "api/software/about";

        /// <summary>
        /// The zone route
        /// </summary>
        private readonly string zoneRoute = "api/configuration/zone/";

        /// <summary>
        /// Initializes a new instance of the <see cref="CataleyaService" /> class.
        /// </summary>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        public CataleyaService(
            ICurrentUserService currentUserService,
            IAppConfigRepository appConfigRepository)
        {
            this.cataleyaApiUrl = currentUserService.CataleyaApiUrl;
            this.cataleyaUsername = currentUserService.CataleyaUsername;
            this.cataleyaPassword = currentUserService.CataleyaPassword;
            this.defaultNodeId = currentUserService.CataleyaDefaultNodeId;

            // If the paramters aren't set fall back to defaults
            if (string.IsNullOrEmpty(this.cataleyaApiUrl))
            {
                this.cataleyaApiUrl = appConfigRepository.GetValue(AppConstants.CataleyaApiUrlKey);
                this.cataleyaUsername = appConfigRepository.GetValue(AppConstants.CataleyaUsernameKey);
                this.cataleyaPassword = appConfigRepository.GetValue(AppConstants.CataleyaPasswordKey);
                this.defaultNodeId = Convert.ToInt32(appConfigRepository.GetValue(AppConstants.CataleyaDefaultNodeId));
            }
        }

        /// <summary>
        /// Creates the specified acl entity model.
        /// </summary>
        /// <param name="aclEntityModel">The ACL entity model.</param>
        /// <returns>Returns Task.</returns>
        public async Task<CreateSecurityAclResponse> CreateSecurityAcl(AclEntityModel aclEntityModel)
        {
            var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "create").ConfigureAwait(false);
            CreateSecurityAclResponse retVal = null;

            // Convert to the ACL Entity Model to the Cataleya version
            var aclEntity = this.BuildAcl(aclEntityModel);

            string serializedObject = JsonConvert.SerializeObject(aclEntity);
            var jsonContent = new StringContent(serializedObject, Encoding.UTF8, "application/json");
            var response = await this.PostAsync(requestString, jsonContent).ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                Log.Error("Create failed");
                var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                Log.Error(jsonString);
            }
            else
            {
                // Process the returned result
                var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                retVal = JsonConvert.DeserializeObject<CreateSecurityAclResponse>(jsonString);
            }

            return retVal;
        }

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Returns Task.</returns>
        public async Task<DeleteSecurityAclResponse> DeleteSecurityAcl(CompoundKey key)
        {
            DeleteSecurityAclResponse retVal = null;

            var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "delete").ConfigureAwait(false);
            requestString += $"&id={key.id}&nodeId={key.nodeid}";

            var response = await this.DeleteAsync(requestString).ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                Log.Error("Delete failed");
            }
            else
            {
                // Process the returned result
                var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                retVal = JsonConvert.DeserializeObject<DeleteSecurityAclResponse>(jsonString);
            }

            return retVal;
        }

        /// <summary>
        /// Lists all.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns data.</returns>
        public async Task<SecurityAcl> SecurityAclListAsync(int? nodeId)
        {
            // Build the request string
            var requestString = await this.BuildWebApiUrl(this.securityAclRoute + "get").ConfigureAwait(false);
            requestString += "&nodeId=" + (nodeId ?? this.defaultNodeId);

            var retVal = await this.GetRequest<SecurityAcl>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// zone list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;Zones&gt;.</returns>
        public async Task<Zones> ZoneListAsync(int? nodeId)
        {
            var requestString = await this.BuildWebApiUrl(this.zoneRoute + "get").ConfigureAwait(false);
            requestString += "&nodeId=" + (nodeId ?? this.defaultNodeId);

            var retVal = await this.GetRequest<Zones>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// public ip interfaces from zones list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;IEnumerable&lt;IpInterface&gt;&gt;.</returns>
        public async Task<IEnumerable<IpInterface>> PublicIpInterfacesFromZonesListAsync(int? nodeId)
        {
            var allZones = await this.ZoneListAsync(nodeId).ConfigureAwait(false);

            // Get the public zones
            // TODO It would be nice to be able to filter on zoneType
            var publicZones = allZones.data.Where(
                    z => z.zoneType.Equals(AppConstants.AccessPublicKey, StringComparison.InvariantCultureIgnoreCase))
                .ToList();

            var interfaceList = new Dictionary<int, IpInterface>();

            foreach (var zone in publicZones)
            {
                foreach (var sipInterface in zone.sipInterfaces)
                {
                    if (!interfaceList.ContainsKey(sipInterface.ipInterfaceId))
                    {
                        interfaceList.Add(sipInterface.ipInterfaceId, sipInterface.ipInterface);
                    }
                }
            }

            return interfaceList.Values.ToList();
        }

        /// <summary>
        /// Sips the interface list.
        /// </summary>
        /// <typeparam name="T"> Entity Type </typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns Task&lt;SipInterfaces&gt;.</returns>
        public async Task<DataWrapperListBase<T>> InterfaceList<T>(InterfaceType interfaceType, int? nodeId)
        {
            // Build the request string
            string routePath;
            if (interfaceType.Equals(InterfaceType.Media) || interfaceType.Equals(InterfaceType.Sip))
            {
                routePath = string.Format(this.configurationInterfaceRoute, interfaceType.GetDescription().ToLower());
            }
            else
            {
                routePath = this.configurationIpInterfaceRoute;
            }

            var requestString = await this.BuildWebApiUrl(routePath + "get").ConfigureAwait(false);
            requestString += "&nodeId=" + (nodeId ?? this.defaultNodeId);

            var retVal = await this.GetRequest<DataWrapperListBase<T>>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Gets the interface.
        /// </summary>
        /// <typeparam name="T"> Entity Type </typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns Task&lt;DataWrapperEntityBase&lt;MediaInterface&gt;&gt;.</returns>
        public async Task<DataWrapperEntityBase<T>> GetInterface<T>(InterfaceType interfaceType, int nodeId, int id)
        {
            // Build the request string
            var routePath = string.Format(this.configurationInterfaceRoute, interfaceType.GetDescription().ToLower());
            var parameter = nodeId + "/" + id;
            var requestString = await this.BuildWebApiUrl(routePath + parameter).ConfigureAwait(false);

            var retVal = await this.GetRequest<DataWrapperEntityBase<T>>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Gets the ip interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;DataWrapperBase&lt;T&gt;&gt;.</returns>
        public async Task<DataWrapperBase<T>> GetIpInterface<T>(int nodeId, int id)
        {
            // Build the request string
            var routePath = this.configurationIpInterfaceRoute;
            var parameter = nodeId + "/" + id;
            var requestString = await this.BuildWebApiUrl(routePath + parameter).ConfigureAwait(false);

            var retVal = await this.GetRequest<DataWrapperBase<T>>(requestString).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <returns>Returns Task&lt;System.String&gt;.</returns>
        public async Task<string> GetAccessToken()
        {
            Log.Info("Entered CataleyaService.GetAccessToken");
            var accessToken = "30bebe9c-4221-4579-b480-f71e2187d64b";// string.Empty;

            try
            {
                var webApiUrl = this.cataleyaApiUrl + 
                                "oauth/token?grant_type=password&client_id=restapp&client_secret=restapp&username=" + 
                                this.cataleyaUsername + "&password=" + 
                                this.cataleyaPassword;

                var response = await this.PostAsync(webApiUrl, null).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    // Get the response
                    var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                    var result = JsonConvert.DeserializeObject<Authentication>(jsonString);

                    if (result != null)
                    {
                        accessToken = result.access_token;
                    }
                }
                else
                {
                    Log.Error("CataleyaService.GetAccessToken [" + response.StatusCode + "] [" + response.ReasonPhrase + "]");
                }

            }
            catch (Exception e)
            {
                Log.Error("Error CataleyaService.GetAccessToken [" + e.Message + "]");
                Log.Error(e.ToString());
                Log.Error(e.StackTrace);
            }

            Log.Info("Exited CataleyaService.GetAccessToken [" + accessToken + "]");
            return accessToken;
        }

        /// <summary>
        /// Validates the access token.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <returns>Returns Task&lt;HttpStatusCode&gt;.</returns>
        public async Task<HttpStatusCode> ValidateAccessToken(string accessToken)
        {
            Log.Info("CataleyaService.ValidateAccessToken: accessToken [" + accessToken + "]");
            HttpStatusCode retVal;
            if (!string.IsNullOrEmpty(accessToken))
            {
                var webApiUrl = this.cataleyaApiUrl + this.softwareAboutRoute + "?access_token=" + accessToken;

                var response = await this.GetAsyncWithAccessHeader(webApiUrl).ConfigureAwait(false);

                // Check the result
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    Log.Info("CataleyaService.ValidateAccessToken: jsonString [" + jsonString + "]");
                    dynamic result = JsonConvert.DeserializeObject(jsonString);

                    retVal = result.success == true ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
                }
                else
                {
                    Log.Error("CataleyaService.ValidateAccessToken: Failted Status Code [" + response.StatusCode + "] [" + response.ReasonPhrase + "]");
                    retVal = response.StatusCode;
                }
            }
            else
            {
                retVal = HttpStatusCode.BadRequest;
            }

            Log.Info("CataleyaService.ValidateAccessToken: retVal [" + retVal + "]");
            return retVal;
        }

        /// <summary>
        /// Builds the web API URL.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns>Return Task&lt;System.String&gt;.</returns>
        public async Task<string> BuildWebApiUrl(string route)
        {
            var webApiUrl = string.Empty;

            // Get the access token
            var accessToken = await this.GetAccessToken().ConfigureAwait(false);

            if (!string.IsNullOrEmpty(accessToken))
            {
                webApiUrl = this.cataleyaApiUrl + route + "?access_token=" + accessToken;
            }

            return webApiUrl;
        }


        /// <summary>
        /// Gets the target interface.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IpInterface&gt;.</returns>
        public async Task<IpInterface> GetTargetInterface(InterfaceType interfaceType,
                                                           int nodeId,
                                                           int id)
        {
            DataEntityInterfaceBase parentInterface;
            IpInterface retVal = null;
            if (interfaceType.Equals(InterfaceType.Sip))
            {
                var sipWrapper = await this.GetInterface<SipInterface>(interfaceType, nodeId, id)
                                          .ConfigureAwait(false);
                parentInterface = sipWrapper.data;
                retVal = parentInterface.ipInterface;
            }
            else if (interfaceType.Equals(InterfaceType.Media))
            {
                var mediaWrapper = await this.GetInterface<MediaInterface>(interfaceType, nodeId, id)
                                                          .ConfigureAwait(false);
                parentInterface = mediaWrapper.data;
                retVal = parentInterface.ipInterface;
            }
            else if (interfaceType.Equals(InterfaceType.Ip))
            {
                var mediaWrapper = await this.GetIpInterface<IpInterface>(nodeId, id)
                                                          .ConfigureAwait(false);
                retVal = mediaWrapper.data;
            }
            else
            {
                throw new InvalidOperationException();
            }

            return retVal;
        }

        /// <summary>
        /// Builds the acl.
        /// </summary>
        /// <param name="aclEntityModel">The acl entity model.</param>
        /// <returns>Task&lt;Acl&gt;.</returns>
        private CreateAcl BuildAcl(AclEntityModel aclEntityModel)
        {
            return new CreateAcl
            {
                                 ipInterface = aclEntityModel.IpInterface,
                                 nodeId = this.defaultNodeId,
                                 action = aclEntityModel.AclType.Equals(AclListType.BlackList) ? "Drop" : "Accept",
                                 appType = aclEntityModel.InterfaceType.ToString(),
                                 remoteIpAddress = aclEntityModel.IpAddress,
                                 localPort = 0,
                                 remotePort = 0,
                                 remotePrefix = aclEntityModel.Prefix,
                                 transport = "UDP",
                                 ver = 1,
                                 ipinterfaceId = aclEntityModel.IpInterface.id,
                                 compoundKey = new CompoundKey { nodeid = this.defaultNodeId, id = 0 }
                             };
        }

        /// <summary>
        /// get as an asynchronous operation.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> GetAsync(string requestString)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.GetAsync(requestString).ConfigureAwait(false);

                if (result != null)
                {
                    Log.Info("CataleyaService.GetAsync: Request [" + requestString  + "] Response [" + result.StatusCode + "]");
                }
                else
                {
                    Log.Error("CataleyaService.GetAsync: Request[" + requestString  + "] returned null result");
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the request.
        /// </summary>
        /// <typeparam name="T"> Data type </typeparam>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;T&gt;.</returns>
        private async Task<T> GetRequest<T>(string requestString)
            where T : class, new()
        {
            var response = await this.GetAsyncWithAccessHeader(requestString).ConfigureAwait(false);
            T retVal = null;

            if (response.IsSuccessStatusCode)
            {
                retVal = await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
            }

            return retVal;
        }

        /// <summary>
        /// Gets the asynchronous with access header.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> GetAsyncWithAccessHeader(string requestString)
        {
            HttpResponseMessage result;
            Log.Info("Entered CataleyaService.GetAsyncWithAccessHeader: Request [" + requestString + "]");
            using (var client = this.GetHttpClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestString);
                requestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                result = await client.SendAsync(requestMessage).ConfigureAwait(false);

                if (result != null)
                {
                    Log.Info("CataleyaService.GetAsyncWithAccessHeader: Request [" + requestString + "] Response [" + result.StatusCode + "]");
                }
                else
                {
                    Log.Error("CataleyaService.GetAsyncWithAccessHeader: Request[" + requestString + "] returned null result");
                }
            }

            Log.Info("Exited  CataleyaService.GetAsyncWithAccessHeader: Request [" + requestString + "]");

            return result;
        }

        /// <summary>
        /// post as an asynchronous operation.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <param name="content">The content.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> PostAsync(string requestString, HttpContent content)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.PostAsync(requestString, content).ConfigureAwait(false);

                return result;
            }
        }

        /// <summary>
        /// delete as an asynchronous operation.
        /// </summary>
        /// <param name="requestString">The request string.</param>
        /// <returns>Returns Task&lt;HttpResponseMessage&gt;.</returns>
        private async Task<HttpResponseMessage> DeleteAsync(string requestString)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.DeleteAsync(requestString).ConfigureAwait(false);

                return result;
            }
        }

        /// <summary>
        /// Gets the HTTP client.
        /// </summary>
        /// <returns>Returns HttpClient.</returns>
        private HttpClient GetHttpClient()
        {
            HttpClientHandler handler = new HttpClientHandler()
            {
                MaxConnectionsPerServer = 20,
                Proxy = null,
                UseDefaultCredentials = true
            };

            // Set the default connection limit
            ServicePointManager.DefaultConnectionLimit = 20;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            var client = new HttpClient(handler, true);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}