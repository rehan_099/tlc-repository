﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICataleyaService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Cataleya Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CataleyaService
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone;

    /// <summary>
    /// Interface ICataleyaService
    /// </summary>
    public interface ICataleyaService
    {
        /// <summary>
        /// Creates the specified acl entity model.
        /// </summary>
        /// <param name="aclEntityModel">The acl entity model.</param>
        /// <returns>Returns Task.</returns>
        Task<CreateSecurityAclResponse> CreateSecurityAcl(AclEntityModel aclEntityModel);

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Returns Task.</returns>
        Task<DeleteSecurityAclResponse> DeleteSecurityAcl(CompoundKey key);

        /// <summary>
        /// Lists all.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns Task&lt;SecurityAcl&gt;.</returns>
        Task<SecurityAcl> SecurityAclListAsync(int? nodeId);

        /// <summary>
        /// Zones the list.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;Zones&gt;.</returns>
        Task<Zones> ZoneListAsync(int? nodeId);

        /// <summary>
        /// Publics the ip interfaces from zones list asynchronous.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;IEnumerable&lt;IpInterface&gt;&gt;.</returns>
        Task<IEnumerable<IpInterface>> PublicIpInterfacesFromZonesListAsync(int? nodeId);

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <returns>Task&lt;System.String&gt;.</returns>
        Task<string> GetAccessToken();

        /// <summary>
        /// Validates the access token.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <returns>Task&lt;HttpStatusCode&gt;.</returns>
        Task<HttpStatusCode> ValidateAccessToken(string accessToken);

        /// <summary>
        /// Builds the web API URL.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns>Task&lt;System.String&gt;.</returns>
        Task<string> BuildWebApiUrl(string route);

        /// <summary>
        /// Interfaces the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns Task&lt;DataWrapperListBase&lt;T&gt;&gt;.</returns>
        Task<DataWrapperListBase<T>> InterfaceList<T>(InterfaceType interfaceType, int? nodeId);

        /// <summary>
        /// Gets the interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns Task&lt;DataWrapperEntityBase&lt;T&gt;&gt;.</returns>
        Task<DataWrapperEntityBase<T>> GetInterface<T>(InterfaceType interfaceType, int nodeId, int id);

        /// <summary>
        /// Gets the target interface.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IpInterface&gt;.</returns>
        Task<IpInterface> GetTargetInterface(InterfaceType interfaceType, int nodeId, int id);
    }
}
