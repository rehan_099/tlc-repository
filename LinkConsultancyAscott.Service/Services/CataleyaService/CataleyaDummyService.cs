﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaDummyService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the CATALEYA Dummy Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CataleyaService
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone;
    
    /// <summary>
    /// Class CATALEYA Dummy Service.
    /// </summary>
    public class CataleyaDummyService : ServiceBase, ICataleyaService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CataleyaDummyService" /> class.
        /// </summary>
        public CataleyaDummyService()
        {
        }

        /// <summary>
        /// Creates the specified acl entity model.
        /// </summary>
        /// <param name="aclEntityModel">The acl entity model.</param>
        /// <returns>Returns Task.</returns>
        public async Task<CreateSecurityAclResponse> CreateSecurityAcl(AclEntityModel aclEntityModel)
        {
            return await Task.Run(() => new CreateSecurityAclResponse()).ConfigureAwait(false);
        }

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Returns Task.</returns>
        public async Task<DeleteSecurityAclResponse> DeleteSecurityAcl(CompoundKey key)
        {
            return await Task.Run(() => new DeleteSecurityAclResponse()).ConfigureAwait(false);
        }

        /// <summary>
        /// security acl list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns Task&lt;SecurityAcl&gt;.</returns>
        public async Task<SecurityAcl> SecurityAclListAsync(int? nodeId)
        {
            return await Task.Run(() => new SecurityAcl());
        }

        /// <summary>
        /// zone list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;Zones&gt;.</returns>
        public async Task<Zones> ZoneListAsync(int? nodeId)
        {
            return await Task.Run(() => new Zones());
        }

        /// <summary>
        /// public ip interfaces from zones list as an asynchronous operation.
        /// </summary>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Task&lt;IEnumerable&lt;IpInterface&gt;&gt;.</returns>
        public async Task<IEnumerable<IpInterface>> PublicIpInterfacesFromZonesListAsync(int? nodeId)
        {
            return await Task.Run(() => new List<IpInterface>()).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <returns>Task&lt;System.String&gt;.</returns>
        public async Task<string> GetAccessToken()
        {
            return await Task.Run(() => string.Empty).ConfigureAwait(false);
        }

        /// <summary>
        /// Validates the access token.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <returns>Task&lt;HttpStatusCode&gt;.</returns>
        public async Task<HttpStatusCode> ValidateAccessToken(string accessToken)
        {
            return await Task.Run(() => HttpStatusCode.OK).ConfigureAwait(false);
        }

        /// <summary>
        /// Builds the web API URL.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns>Task&lt;System.String&gt;.</returns>
        public async Task<string> BuildWebApiUrl(string route)
        {
            return await Task.Run(() => string.Empty);
        }

        /// <summary>
        /// Interfaces the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <returns>Returns Task&lt;DataWrapperListBase&lt;T&gt;&gt;.</returns>
        public async Task<DataWrapperListBase<T>> InterfaceList<T>(InterfaceType interfaceType, int? nodeId)
        {
            return await Task.Run(() => new DataWrapperListBase<T>());
        }

        /// <summary>
        /// Gets the interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns Task&lt;DataWrapperEntityBase&lt;T&gt;&gt;.</returns>
        public async Task<DataWrapperEntityBase<T>> GetInterface<T>(InterfaceType interfaceType, int nodeId, int id)
        {
            return await Task.Run(() => new DataWrapperEntityBase<T>());
        }

        /// <summary>
        /// Gets the target interface.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="nodeId">The node identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;IpInterface&gt;.</returns>
        public async Task<IpInterface> GetTargetInterface(InterfaceType interfaceType, int nodeId, int id)
        {
            return await Task.Run(() => new IpInterface());
        }
    }
}