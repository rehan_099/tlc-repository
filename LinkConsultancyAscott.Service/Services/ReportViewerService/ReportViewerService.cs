﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportViewerService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Report Viewer Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.ReportViewerService
{
    using System.Collections.Generic;
    using System.IO;
    using System.Web;

    using LinkConsultancyAscott.Service.Services.ReportViewerService;

    using Telerik.Reporting.Cache.File;
    using Telerik.Reporting.Processing;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.Engine;
    using Telerik.Reporting.Services.WebApi;
    using Types;

    /// <summary>
    /// The Report Viewer Service.
    /// </summary>
    public class ReportViewerService : ServiceBase, IReportViewerService
    {
        /// <summary>
        /// The preserved configuration.
        /// </summary>
        private static ReportServiceConfiguration preservedConfiguration;

        /// <summary>
        /// Gets the preserved configuration.
        /// </summary>
        private static IReportServiceConfiguration PreservedConfiguration
        {
            get
            {
                if (null == preservedConfiguration)
                {
                    preservedConfiguration = new ReportServiceConfiguration
                    {
                        HostAppId = "Tlc.App",
                        Storage = new FileStorage(),
                        ReportResolver = CreateResolver(),
                        // ReportSharingTimeout = 0,
                        // ClientSessionTimeout = 15,
                    };
                }

                return preservedConfiguration;
            }
        }

        /// <summary>
        /// The create resolver.
        /// </summary>
        /// <returns>
        /// The <see cref="IReportResolver"/>.
        /// </returns>
        static IReportResolver CreateResolver()
        {
            var appPath = HttpContext.Current.Server.MapPath("~/");
            var reportsPath = Path.Combine(appPath, @"Reports");

            return new ReportFileResolver(reportsPath)
                .AddFallbackResolver(new ReportTypeResolver());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportViewerService"/> class.
        /// </summary>
        public ReportViewerService()
        {
        }

        public RenderingResult ExportToPdf(string reportName, RenderingExtensions extension, Dictionary<string, string> parameters)
        {
            ReportProcessor reportProcessor = new ReportProcessor();
            IReportResolver reportResolver = PreservedConfiguration.ReportResolver;

            var reportSource = reportResolver.Resolve(reportName);

            foreach (var parameter in parameters)
            {
                reportSource.Parameters.Add(parameter.Key, parameter.Value);
            }
            
            RenderingResult result = reportProcessor.RenderReport(extension.Description(), reportSource, null);

            return result;
        }
    }
}
