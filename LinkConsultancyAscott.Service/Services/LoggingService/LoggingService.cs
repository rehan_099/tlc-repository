﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoggingService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Logging Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.LoggingService
{
    using System.Collections.Generic;
    using System.Linq;


    using LinkConsultancyAscott.Service.Repository.LogRepository;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// Class LoggingService.
    /// </summary>
    public class LoggingService : ILoggingService
    {
        /// <summary>
        /// The Web Chilli Logging DAL.
        /// </summary>
        private ILogRepository repository;

        public LoggingService(ILogRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <returns>
        /// The lit of view models.
        /// </returns>
        public IQueryable<LogViewModel> Read()
        {
            var viewModelList = this.repository.GetAll().Select(
                   n => new LogViewModel
                   {
                       Id = n.Id,
                       Date = n.Date,
                       Exception = n.Exception,
                       Level = n.Level,
                       Logger = n.Logger,
                       Message = n.Message,
                       Thread = n.Message
                   });


            return viewModelList;
        }

        /// <summary>
        /// Uniques the specified field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns> Returns IQueryable&lt;LogViewModel&gt;.</returns>
        public IQueryable<LogViewModel> Unique(string field)
        {
            /*var result = this.repository.GetAll().Select(
                   n => new LogViewModel
                   {
                       Id = n.Id,
                       Date = n.Date,
                       Exception = n.Exception,
                       Level = n.Level,
                       Logger = n.Logger,
                       Message = n.Message,
                       Thread = n.Message
                   }).Distinct(new LogViewModelComparer(field));*/
            List<LogViewModel> list = new List<LogViewModel>
                                          {
                                              new LogViewModel
                                                  {
                                                      Level = "DEBUG",
                                                      Id = 1,
                                                      Message = string.Empty
                                                  },
                                              new LogViewModel
                                                  {
                                                      Level = "INFO",
                                                      Id = 2,
                                                      Message = string.Empty
                                                  },
                                              new LogViewModel
                                                  {
                                                      Level = "ERROR",
                                                      Id = 3,
                                                      Message = string.Empty
                                                  },
                                              new LogViewModel
                                                  {
                                                      Level = "WARNING",
                                                      Id = 3,
                                                      Message = string.Empty
                                                  }
                                          };

            return list.AsQueryable();
        }
    }
}