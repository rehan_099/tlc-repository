﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the IEmailService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.EmailService
{
    using System.Collections.Generic;
    using System.Net.Mail;

    /// <summary>
    /// Interface IEmailService
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="recipients">The recipients.</param>
        /// <param name="attachments">The attachments.</param>
        void SendEmail(string from, string subject, string body, List<string> recipients = null, List<Attachment> attachments = null);
    }
}
