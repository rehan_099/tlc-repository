﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class EmailService.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.EmailService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;
    using System.Runtime.ExceptionServices;
    using System.Text;
    using System.Threading;
    
    /// <summary>
    /// Class EmailService.
    /// </summary>
    /// <seealso cref="ServiceBase" />
    /// <seealso cref="IEmailService" />
    public class EmailService : ServiceBase, IEmailService
    {
        /// <summary>
        /// The SMTP host
        /// </summary>
        private readonly string smtpHost = "uk-app-viruswalls.uk.astrium.corp";

        /// <summary>
        /// Sends an email out to recipients.
        /// </summary>
        /// <param name="from">Address information of sender.</param>
        /// <param name="subject">Message subject.</param>
        /// <param name="body">Message body.</param>
        /// <param name="recipients">Optional list of email recipients.</param>
        /// <param name="attachments">The attachments.</param>
        /// <exception cref="ArgumentException">
        /// Email from field must contain address information of sender.
        /// or
        /// Email subject cannot be blank.
        /// or
        /// Email body cannot be blank.
        /// </exception>
        /// <exception cref="System.ArgumentException">Email from field must contain address information of sender.
        /// or
        /// Email subject cannot be blank.
        /// or
        /// Email body cannot be blank.</exception>
        /// <remarks>If no recipients are supplied as a parameter, recipients will be taken from the stored list in the database.</remarks>
        public void SendEmail(string from, string subject, string body, List<string> recipients = null, List<Attachment> attachments = null)
        {
            if (recipients != null && recipients.Any())
            {
                var sb = new StringBuilder("<p><u>Sending Message:</u></p>");
                sb.Append("<p><u>To:</u> " + string.Join(",", recipients) + "</p><hr>");
                sb.Append("<p><u>Subject:</u> " + subject + "</p><hr>");
                sb.Append(body);
                Log.Info(sb);

                if (string.IsNullOrEmpty(from))
                {
                    throw new ArgumentException("Email from field must contain address information of sender.");
                }

                if (string.IsNullOrEmpty(subject))
                {
                    throw new ArgumentException("Email subject cannot be blank.");
                }

                if (string.IsNullOrEmpty(body))
                {
                    throw new ArgumentException("Email body cannot be blank.");
                }

                // Only send when outside visual studio to save unnecessary logging
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    using (var smtpServer = new SmtpClient { Host = this.smtpHost })
                    {
                        foreach (var recipient in recipients)
                        {
                            try
                            {
                                var mail = new MailMessage(from, recipient, subject, body)
                                               {
                                                   IsBodyHtml = true,
                                                   Priority = MailPriority.High
                                               };

                                // Add the attachments if there are any
                                if (attachments != null && attachments.Any())
                                {
                                    foreach (var attachment in attachments)
                                    {
                                        mail.Attachments.Add(attachment);
                                    }
                                }

                                // Send the email
                                smtpServer.Send(mail);
                            }
                            catch (Exception exception)
                            {
                                // Standard exception
                                if (exception is ThreadAbortException || exception is StackOverflowException
                                                                      || exception is OutOfMemoryException)
                                {
                                    ExceptionDispatchInfo.Capture(exception).Throw();
                                }

                                Log.Error("Error sending message", exception);
                            }
                        }
                    }
                }
            }
        }
    }
}