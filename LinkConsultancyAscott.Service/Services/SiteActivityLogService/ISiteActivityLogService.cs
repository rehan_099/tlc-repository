﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISiteActivityLogService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Site Activity Log Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.SiteActivityLogService
{
    using System.Collections.Generic;

    using ViewModels.Administration;

    /// <summary>
    /// Interface ISiteActivityLogService
    /// </summary>
    public interface ISiteActivityLogService
    {
        /// <summary>
        /// Reads this instance.
        /// </summary>
        /// <returns> List of Site Logs.</returns>
        IEnumerable<SiteActivityLogViewModel> Read();
    }
}
