﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2015
// </copyright>
// <summary>
//   Defines the CacheService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LinkConsultancyAscott.Service.Services.CacheService
{
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// The cache service.
    /// </summary>
    public class CacheService : CachingProviderBase, ICacheService
    {
        /// <summary>
        /// The collection name separator.
        /// </summary>
        private readonly string collectionNameSeparator = "::";

        /// <summary>
        /// Separate store of cache keys used - there are performance warnings about using GetEnumerator on the MemoryCache object,
        /// e.g. see: http://stackoverflow.com/questions/4183270/how-to-clear-the-net-4-memorycache
        /// </summary>
        private readonly Dictionary<string, List<string>> cacheKeys;

        /// <summary>
        /// Lock object to control access to the keys dictionary.
        /// </summary>
        private readonly object Padlock = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheService"/> class.
        /// </summary>
        public CacheService() : base()
        {
            // Log.Info("CacheService ctor - this should only be called once ObjId [" + this.GetHashCode() + "] ThreadId [" + Thread.CurrentThread.ManagedThreadId + "]");
            this.cacheKeys = new Dictionary<string, List<string>>();
        }

        /// <summary>
        /// The contains key.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ContainsKey(string collection, string key)
        {
            var searchResult = this.ItemExists(this.GetCollectionKeyName(collection, key));
            return searchResult;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Get(string collection, string key)
        {
            var result = this.GetItem(this.GetCollectionKeyName(collection, key), false);

            /*if (result != null)
            {
                this.logger.LogMessage("CacheService.Get ObjId [" + this.GetHashCode() + "] ThreadId [" + Thread.CurrentThread.ManagedThreadId + "] collection [" + collection + "] key [" + key + "] found ObjId [" + result.GetHashCode() + "]");
            }
            else
            {
                this.logger.LogMessage("CacheService.Get ObjId [" + this.GetHashCode() + "] ThreadId [" + Thread.CurrentThread.ManagedThreadId + "] collection [" + collection + "] key [" + key + "] not found");
            }*/
            return result;
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        /// The data.
        public void Set(string collection, string key, object data)
        {
            // Log.Info("CacheService.Set ObjId [" + this.GetHashCode() + "] ThreadId [" + Thread.CurrentThread.ManagedThreadId + "] collection [" + collection + "] key [" + key + "] ObjId [" + data.GetHashCode() + "]");
            this.AddItem(this.GetCollectionKeyName(collection, key), data);
            this.RememberCacheKey(collection, key);
        }

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="collection">The cache collection name</param>
        /// <param name="key">The key.</param>
        /// <param name="data">The data.</param>
        public void SetOrReplace(string collection, string key, object data)
        {
            // Log.Info("CacheService.SetOrReplace ObjId [" + this.GetHashCode() + "] ThreadId [" + Thread.CurrentThread.ManagedThreadId + "] collection [" + collection + "] key [" + key + "] ObjId [" + data.GetHashCode() + "]");
            this.AddOrReplaceItem(this.GetCollectionKeyName(collection, key), data);
            this.RememberCacheKey(collection, key);
        }

        /// <summary>
        /// Removes the specified collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="key">The key.</param>
        public void Remove(string collection, string key)
        {
            if (this.ContainsKey(collection, key))
            {
                this.RemoveItem(this.GetCollectionKeyName(collection, key));
                this.ForgetCacheKey(collection, key);
            }
        }

        /// <summary>
        /// Removes all the cache entries belonging to the given collection.
        /// </summary>
        /// <param name="collection">The collection to remove.</param>
        /// <returns>Number of items in the collection which were removed.</returns>
        /// <remarks>
        /// To (hopefully) avoid performance problems iterating the MemoryCache to remove certain keys, we remember the keys
        /// we use in a separate Dictionary. There is no quick way of finding out what the keys are in a MemoryCache object!
        /// </remarks>
        public int RemoveCollection(string collection)
        {
            lock (this.Padlock)
            {
                var removedItems = 0;
                if (this.cacheKeys.ContainsKey(collection))
                {
                    var keys = new List<string>(this.cacheKeys[collection]);
                    foreach (var key in keys)
                    {
                        this.Remove(collection, key);
                        removedItems++;
                    }

                    this.cacheKeys.Remove(collection);
                }

                return removedItems;
            }
        }

        /// <summary>
        /// The get collection key name.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetCollectionKeyName(string collection, string key)
        {
            return $"{collection.ToUpper()}{this.collectionNameSeparator}{key.ToUpper()}";
        }

        /// <summary>
        /// Remember the cache key formed out of the given collection and key.
        /// </summary>
        /// <param name="collection">The collection name.</param>
        /// <param name="key">The key name.</param>
        private void RememberCacheKey(string collection, string key)
        {
            lock (this.Padlock)
            {
                if (this.cacheKeys.ContainsKey(collection) && !this.cacheKeys[collection].Contains(key))
                {
                    this.cacheKeys[collection].Add(key);
                }
                else if (!this.cacheKeys.ContainsKey(collection))
                {
                    this.cacheKeys[collection] = new List<string> { key };
                }
            }

            // Otherwise, key is already there - do nothing.
        }

        /// <summary>
        /// Forget the remembered cache key formed out of the given collection and key.
        /// </summary>
        /// <param name="collection">The collection name.</param>
        /// <param name="key">The key name.</param>
        private void ForgetCacheKey(string collection, string key = null)
        {
            lock (this.Padlock)
            {
                if (this.cacheKeys.ContainsKey(collection) && key == null)
                {
                    this.cacheKeys.Remove(collection);
                }
                else if (this.cacheKeys.ContainsKey(collection) && this.cacheKeys[collection].Contains(key))
                {
                    this.cacheKeys[collection].Remove(key);
                }
            }

            // Collection and key doesn't exist, so silently do nothing
        }
    }
}
