﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheServiceKeys.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Keys for the Cache Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CacheService
{
    /// <summary>
    /// Class CacheServiceKeys.
    /// </summary>
    public static class CacheServiceKeys
    {
        /// <summary>
        /// The User Identity Key
        /// </summary>
        public static readonly string UserIdentityKey = "UserIdentity";
    }
}