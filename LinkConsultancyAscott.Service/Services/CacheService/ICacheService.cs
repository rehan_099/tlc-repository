﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICacheService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2015
// </copyright>
// <summary>
//   The CacheService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CacheService
{
    /// <summary>
    /// The CacheService interface.
    /// </summary>
    public interface ICacheService
    {
        /// <summary>
        /// The contains key.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ContainsKey(string collection, string key);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="collection"> The cache collection name </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        object Get(string collection, string key);

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="collection">
        ///  The cache collection name
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void Set(string collection, string key, object data);

        /// <summary>
        /// The set.
        /// </summary>
        /// <param name="collection">
        ///  The cache collection name
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        void SetOrReplace(string collection, string key, object data);

        /// <summary>
        /// Removes the specified collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="key">The key.</param>
        void Remove(string collection, string key);

        /// <summary>
        /// Removes the collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>System.Int32.</returns>
        int RemoveCollection(string collection);
    }
}
