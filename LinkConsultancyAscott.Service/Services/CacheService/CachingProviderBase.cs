﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CachingProviderBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2015
// </copyright>
// <summary>
//   Defines the CachingProviderBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CacheService
{
    using System;
    using System.Runtime.Caching;
    using System.Threading;

    /// <summary>
    /// The caching provider base.
    /// </summary>
    public abstract class CachingProviderBase : ServiceBase
    {
        /// <summary>
        /// The padlock.
        /// </summary>
        private static readonly object Padlock = new object();

        /// <summary>
        /// The cache.
        /// </summary>
        private readonly MemoryCache cache = new MemoryCache("CachingProvider");

        /// <summary>
        /// Initializes a new instance of the <see cref="CachingProviderBase"/> class.
        /// </summary>
        protected CachingProviderBase()
        {
            Log.Info("CachingProviderBase ctor - this should only be called once ObjId [" + this.GetHashCode() + "] ThreadId [" + Thread.CurrentThread.ManagedThreadId + "]");
        }

        /// <summary>
        /// The add item.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        protected virtual void AddItem(string key, object value)
        {
            lock (Padlock)
            {
                this.cache.Add(key, value, DateTimeOffset.MaxValue);
            }
        }

        /// <summary>
        /// Adds the or replace item.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        protected virtual void AddOrReplaceItem(string key, object value)
        {
            lock (Padlock)
            {
                if (this.cache.Contains(key))
                {
                    this.cache[key] = value;
                }
                else
                {
                    this.cache.Add(key, value, DateTimeOffset.MaxValue);
                }
            }
        }

        /// <summary>
        /// The remove item.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        protected virtual void RemoveItem(string key)
        {
            lock (Padlock)
            {
                this.cache.Remove(key);
            }
        }

        /// <summary>
        /// The get item.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="remove">
        /// The remove.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        protected virtual object GetItem(string key, bool remove)
        {
            lock (Padlock)
            {
                var res = this.cache[key];

                if (res != null)
                {
                    if (remove)
                    {
                        this.cache.Remove(key);
                    }
                }
                else
                {
                    Log.Info("CachingProvider-GetItem: Does not contain key: " + key);
                }

                return res;
            }
        }

        /// <summary>
        /// The item exists.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected virtual bool ItemExists(string key)
        {
            lock (Padlock)
            {
                var res = this.cache[key];

                return res != null;
            }
        }
    }
}
