﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ServiceBase base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The service base.
    /// </summary>
    public abstract class ServiceBase : IServiceBase
    {
        /// <summary>
        /// The log.
        /// </summary>
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected IEnumerable<int> StringToIntList(string intListString)
        {
            var intArray = (intListString ?? string.Empty).Split(',').Select<string, int>(int.Parse);
            return intArray;
        }
    }

}
