﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHomeService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Home Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.HomeService
{

    /// <summary>
    /// Interface IHomeService
    /// </summary>
    public interface IHomeService
    {

    }
}
