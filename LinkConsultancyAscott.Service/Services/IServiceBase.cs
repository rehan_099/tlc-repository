﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The IServiceBase interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services
{
    /// <summary>
    /// The IServiceBase interface.
    /// </summary>
    public interface IServiceBase
    {
    }
}
