﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IUserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.UserService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// The User Service Interface.
    /// </summary>
    public interface IUserService 
    {
        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        UserViewModel CreateUser(UserViewModel viewModel);

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        UserViewModel UpdateUser(UserViewModel viewModel);

        /// <summary>
        /// The add user role.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        void AddUserRole(string userName, string role);

        /// <summary>
        /// The remove user role.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        void RemoveUserRole(string userName, string role);

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns>IQueryable&lt;UserViewModel&gt;.</returns>
        IQueryable<UserViewModel> GetAllUsers();

        /// <summary>
        /// The get all roles.
        /// </summary>
        /// <returns>
        /// The List of Roles.
        /// </returns>
        IEnumerable<AspNetRole> GetAllRoles();

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        UserViewModel GetUserByName(string userName);

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        UserViewModel GetUserById(string id);

        /// <summary>
        /// The delete user.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        void DeleteUser(string userName);

        /// <summary>
        /// The get roles.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetRoles(string userName);

        /// <summary>
        /// The is user in role.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsUserInRole(string userName, string role);

        /// <summary>
        /// Gets the user claims.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>IQueryable&lt;UserClaimViewModel&gt;.</returns>
        IQueryable<UserClaimViewModel> GetUserClaims(string userId);

        /// <summary>
        /// Updates the user claim.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>UserClaimViewModel.</returns>
        UserClaimViewModel UpdateUserClaim(UserClaimViewModel viewModel);
    }
}
