﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the User Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.UserService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Security;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.RoleRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.CacheService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    using Newtonsoft.Json;

    /// <summary>
    /// The account membership service.
    /// </summary>
    public class UserService : IUserService
    {
        /// <summary>
        /// The application configuration repository
        /// </summary>
        private readonly ICurrentUserService currentUserService;

        /// <summary>
        /// The _provider.
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// The role repository.
        /// </summary>
        private readonly IRoleRepository roleRepository;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// The cache service
        /// </summary>
        private readonly ICacheService cacheService;

        /// <summary>
        /// The application configuration repository
        /// </summary>
        private readonly IAppConfigRepository appConfigRepository;

        /// <summary>
        /// The default node identifier
        /// </summary>
        private readonly int defaultNodeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="cacheService">The cache service.</param>
        public UserService(
            IAppConfigRepository appConfigRepository,
            ICurrentUserService currentUserService,
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            IUnitOfWork unitOfWork,
            ICacheService cacheService)
        {
            this.appConfigRepository = appConfigRepository;
            this.currentUserService = currentUserService;
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.unitOfWork = unitOfWork;
            this.cacheService = cacheService;
            this.defaultNodeId = this.currentUserService.CataleyaDefaultNodeId;
        }

        /// <summary>
        /// The create user.
        /// </summary>
        /// <param name="viewModel">
        /// The user view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        public UserViewModel CreateUser(UserViewModel viewModel)
        {
            var dto = new AspNetUser { UserName = viewModel.UserName };

            var userRolesList = new List<string>();

            if (viewModel.UserRoles != null && viewModel.UserRoles.Any())
            {
                userRolesList.AddRange(viewModel.UserRoles.Select(role => $"{role.Id}:{role.Name}").ToList());
            }

            dto.UserName = viewModel.UserName;
            var userRoles = string.Join(",", userRolesList);

            this.userRepository.Add(dto, userRoles);

            this.unitOfWork.Commit();

            return this.GetUserByName(viewModel.UserName);
        }

        /// <summary>
        /// The update user.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        public UserViewModel UpdateUser(UserViewModel viewModel)
        {
            var dto = this.userRepository.Get(viewModel.UserId.ToString());
            
            if (dto != null)
            {
                var originalUserName = dto.UserName;

                var userRolesList = new List<string>();

                if (viewModel.UserRoles != null && viewModel.UserRoles.Any())
                {
                    userRolesList.AddRange(viewModel.UserRoles.Select(role => $"{role.Id}:{role.Name}").ToList());
                }

                dto.UserName = viewModel.UserName;
                var userRoles = string.Join(",", userRolesList);

                // Update the user claims
                this.AddOrUpdateUserClaims(
                    dto.AspNetUserClaims,
                    AppConstants.SipInterfacesClaimType + this.defaultNodeId,
                    JsonConvert.SerializeObject(viewModel.SipInterfaces));

                this.AddOrUpdateUserClaims(
                    dto.AspNetUserClaims,
                    AppConstants.MediaInterfacesClaimType + this.defaultNodeId,
                    JsonConvert.SerializeObject(viewModel.MediaInterfaces));

                this.userRepository.Edit(dto, userRoles);
                this.unitOfWork.Commit();

                // Ensure a new get is done on the cache next time
                this.cacheService.Remove(CacheServiceKeys.UserIdentityKey, originalUserName);
            }

            return this.GetUserById(viewModel.UserId);
        }

        /// <summary>
        /// The add user role.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        public void AddUserRole(string userName, string role)
        {
            if (!Roles.IsUserInRole(userName, role))
            {
                Roles.AddUserToRole(userName, role);

                // Ensure a new get is done on the cache next time
                this.cacheService.Remove(CacheServiceKeys.UserIdentityKey, userName);
            }
        }

        /// <summary>
        /// The delete user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        public void DeleteUser(Guid userId)
        {
            var user = this.userRepository.Get(userId.ToString());

            if (user != null)
            {
                // Ensure a new get is done on the cache next time
                this.cacheService.Remove(CacheServiceKeys.UserIdentityKey, user.UserName);

                this.userRepository.Delete(user);
                this.unitOfWork.NonTrackingCommit();
            }
        }

        /// <summary>
        /// The delete user.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        public void DeleteUser(string userName)
        {
            var user = this.userRepository.GetByName(userName);

            if (user != null)
            {
                // Ensure a new get is done on the cache next time
                this.cacheService.Remove(CacheServiceKeys.UserIdentityKey, user.UserName);

                this.userRepository.Delete(user);
                this.unitOfWork.NonTrackingCommit();
            }
        }

        /// <summary>
        /// The get roles.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetRoles(string userName)
        {
            var userRoles = Roles.GetRolesForUser(userName);
            return string.Join(",", userRoles);
        }

        /// <summary>
        /// The get roles for logged in user.
        /// </summary>
        /// <returns>
        /// The list of roles.
        /// </returns>
        public IEnumerable<string> GetRolesForLoggedInUser()
        {
            var userRoles = Roles.GetRolesForUser();
            return userRoles.ToList();
        }

        /// <summary>
        /// The remove user role.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        public void RemoveUserRole(string userName, string role)
        {
            if (Roles.IsUserInRole(userName, role))
            {
                Roles.RemoveUserFromRole(userName, role);

                // Ensure a new get is done on the cache next time
                this.cacheService.Remove(CacheServiceKeys.UserIdentityKey, userName);
            }
        }

        public IQueryable<UserViewModel> GetAllUsers()
        {
            var userList = this.userRepository.GetAllUsersInRoles().ToList();

            var retVal = userList.Select(user => new UserViewModel
            {
                UserId = user.UserId,
                UserName = user.UserName,
                LastActivityDate = user.LastActivityDate,
                UserRoles = this.GetUserRoleViewModel(user.UserRoles),
                SipInterfaces = !string.IsNullOrEmpty(user.SipInterfaces) ? JsonConvert.DeserializeObject<IEnumerable<FilterListViewModel>>(user.SipInterfaces) : new List<FilterListViewModel>(),
                MediaInterfaces = !string.IsNullOrEmpty(user.MediaInterfaces) ? JsonConvert.DeserializeObject<IEnumerable<FilterListViewModel>>(user.MediaInterfaces) : new List<FilterListViewModel>()
            }).ToList();

            return retVal.AsQueryable();
        }

        /// <summary>
        /// The get all roles.
        /// </summary>
        /// <returns>
        /// The roles.
        /// </returns>
        public IEnumerable<AspNetRole> GetAllRoles()
        {
            return this.roleRepository.GetAll();
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        public UserViewModel GetUserByName(string userName)
        {
            var user = this.userRepository.GetAll().FirstOrDefault(u => string.Equals(u.UserName, userName, StringComparison.CurrentCultureIgnoreCase));
            UserViewModel userViewModel = null;

            if (user != null)
            {
                userViewModel = new UserViewModel
                {
                    UserId = user.UserId,
                    UserName = user.UserName,
                    LastActivityDate = user.LastActivityDate.GetValueOrDefault(),
                    UserRoles = this.GetUserRoleViewModel(user.UserRoles)
                };
            }

            return userViewModel;
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="UserViewModel"/>.
        /// </returns>
        public UserViewModel GetUserById(string id)
        {
            var user = this.userRepository.GetAll().FirstOrDefault(u => u.UserId == id);
            UserViewModel userViewModel = null;

            if (user != null)
            {
                userViewModel = new UserViewModel
                                    {
                                        UserId = user.UserId,
                                        UserName = user.UserName,
                                        LastActivityDate = user.LastActivityDate.GetValueOrDefault(),
                                        UserRoles = this.GetUserRoleViewModel(user.UserRoles)
                                    };
            }

            return userViewModel;
        }

        /// <summary>
        /// The is user in role.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsUserInRole(string userName, string role)
        {
            return Roles.IsUserInRole(userName, role);
        }

        public IQueryable<UserClaimViewModel> GetUserClaims(string userId)
        {
            var user = this.userRepository.Get(userId);
            return user.AspNetUserClaims
                .Where(c => c.ClaimType.Equals(
                                AppConstants.CataleyaDefaultNodeId, StringComparison.InvariantCultureIgnoreCase) || 
                              c.ClaimType.Equals(AppConstants.CataleyaPasswordKey, StringComparison.InvariantCultureIgnoreCase) || 
                              c.ClaimType.Equals(AppConstants.CataleyaUsernameKey, StringComparison.InvariantCultureIgnoreCase) || 
                              c.ClaimType.Equals(AppConstants.CataleyaApiUrlKey, StringComparison.InvariantCultureIgnoreCase))
                .Select(claim => new UserClaimViewModel { UserId = userId, ClaimType = claim.ClaimType, ClaimValue = claim.ClaimValue }).AsQueryable();
        }

        /// <summary>
        /// Updates the user claim.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>UserClaimViewModel.</returns>
        public UserClaimViewModel UpdateUserClaim(UserClaimViewModel viewModel)
        {
            var dto = this.userRepository.Get(viewModel.UserId);

            if (dto != null)
            {
                // Update the user claims
                this.AddOrUpdateUserClaims(
                    dto.AspNetUserClaims,
                    viewModel.ClaimType,
                    viewModel.ClaimValue);
                
                this.userRepository.Edit(dto);
                this.unitOfWork.Commit();

                // Ensure a new get is done on the cache next time
                this.cacheService.Remove(CacheServiceKeys.UserIdentityKey, viewModel.UserId);
            }

            return viewModel;
        }

        /// <summary>
        /// The get user role view model.
        /// </summary>
        /// <param name="userRoles">
        /// The user roles.
        /// </param>
        /// <returns>
        /// The view models.
        /// </returns>
        private IEnumerable<UserRoleViewModel> GetUserRoleViewModel(string userRoles)
        {
            IEnumerable<UserRoleViewModel> retVal;
            if (!string.IsNullOrEmpty(userRoles))
            {
                var splitRoles = userRoles.Split(',');

                retVal =
                    splitRoles.Select(role => role.Split(':'))
                        .Select(split => new UserRoleViewModel { Id = split[0], Name = split[1] })
                        .ToList();
            }
            else
            {
                retVal = new List<UserRoleViewModel>();
            }

            return retVal;
        }

        /// <summary>
        /// Adds the or update user claims.
        /// </summary>
        /// <param name="userClaims">The user claims.</param>
        /// <param name="claimType">Type of the claim.</param>
        /// <param name="claimValue">The claim value.</param>
        private void AddOrUpdateUserClaims(ICollection<AspNetUserClaim> userClaims, string claimType, string claimValue)
        {
            var userClaim = userClaims
                .FirstOrDefault(c => c.ClaimType.Equals(claimType, StringComparison.InvariantCultureIgnoreCase));

            if (userClaim != null)
            {
                userClaim.ClaimValue = claimValue;
            }
            else
            {
                userClaim = new AspNetUserClaim { ClaimType = claimType, ClaimValue = claimValue };
                userClaims.Add(userClaim);
            }
        }
    }
}
