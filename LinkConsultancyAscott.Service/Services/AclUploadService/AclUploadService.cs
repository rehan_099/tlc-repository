﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclUploadService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Static Data Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.AclUploadService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Extensions;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;
    using LinkConsultancyAscott.Service.Models.Cataleya.Response;
    using LinkConsultancyAscott.Service.Repository.AclHistoryRepository;
    using LinkConsultancyAscott.Service.Repository.AclRepository;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels;

    using Newtonsoft.Json;

    using InterfaceType = LinkConsultancyAscott.Service.Models.InterfaceType;

    /// <summary>
    /// Class StaticDataService.
    /// </summary>
    public class AclUploadService : ServiceBase, IAclUploadService
    {
        /// <summary>
        /// The application configuration repository
        /// </summary>
        private readonly ICurrentUserService currentUserService;

        /// <summary>
        /// The ACL History repository
        /// </summary>
        private readonly IAclHistoryRepository aclHistoryRepository;

        /// <summary>
        /// The role repository
        /// </summary>
        private readonly IAclRepository aclRepository;

        /// <summary>
        /// The user repository
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// The cataleya service
        /// </summary>
        private readonly ICataleyaService cataleyaService;

        /// <summary>
        /// The application configuration repository
        /// </summary>
        private readonly IAppConfigRepository appConfigRepository;

        /// <summary>
        /// The default node identifier
        /// </summary>
        private readonly int defaultNodeId;

        /// <summary>
        /// The unit of work
        /// </summary>
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="AclUploadService" /> class.
        /// </summary>
        /// <param name="appConfigRepository">The application configuration repository.</param>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="aclHistoryRepository">The ACL History repository.</param>
        /// <param name="aclRepository">The acl repository.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="cataleyaService">The cataleya service.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public AclUploadService(
            IAppConfigRepository appConfigRepository,
            ICurrentUserService currentUserService,
            IAclHistoryRepository aclHistoryRepository,
            IAclRepository aclRepository,
            IUserRepository userRepository,
            ICataleyaService cataleyaService,
            IUnitOfWork unitOfWork)
        {
            this.appConfigRepository = appConfigRepository;
            this.currentUserService = currentUserService;
            this.aclHistoryRepository = aclHistoryRepository;
            this.aclRepository = aclRepository;
            this.userRepository = userRepository;
            this.cataleyaService = cataleyaService;
            this.unitOfWork = unitOfWork;

            this.defaultNodeId = this.currentUserService.CataleyaDefaultNodeId;

            if (this.defaultNodeId == 0)
            {
                this.defaultNodeId =
                    Convert.ToInt32(this.appConfigRepository.GetValue(AppConstants.CataleyaDefaultNodeId));
            }
        }

        /// <summary>
        /// Uploads the acl file.
        /// </summary>
        /// <param name="aclFileUploadRequest">The acl file upload request.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public async Task<ProcessIpAddressResponse> UploadAclFileAsync(AclFileUploadRequest aclFileUploadRequest)
        {
            var ipAddresses = this.GetIpAddresses(aclFileUploadRequest.FileData);

            var retVal = await this.ProcessIpAddresses(ipAddresses, aclFileUploadRequest).ConfigureAwait(false);
            
            return retVal;
        }

        /// <summary>
        /// Creates the specified acl create request.
        /// </summary>
        /// <param name="aclCreateRequest">The acl create request.</param>
        /// <returns>Returns Task&lt;System.Boolean&gt;.</returns>
        public async Task<ProcessIpAddressResponse> CreateAsync(AclCreateRequest aclCreateRequest)
        {
            var ipAddresses = new List<string> { aclCreateRequest.IpAddress + "/" + aclCreateRequest.Prefix };

            var retVal = await this.ProcessIpAddresses(ipAddresses, aclCreateRequest).ConfigureAwait(false);

            return retVal;
        }

        /// <summary>
        /// Processes the IP address.
        /// </summary>
        /// <param name="entityModel">The entity model.</param>
        /// <returns>Returns Task&lt;ProcessIpAddressResponse&gt;.</returns>
        public async Task<ProcessIpAddressResponse> ProcessIpAddressAsync(AclEntityModel entityModel)
        {
            var response = new ProcessIpAddressResponse();

            // Check if this IP address already exists
            if (!this.aclRepository.IpAddressExistsInList(entityModel.IpAddress, entityModel.AclType))
            {
                // Get IP interfaces for the Request type
                var ipInterfaces = await this.GetIpInterfacesForRequest(entityModel).ConfigureAwait(false);

                // Loop through the IP Interfaces that are required
                foreach (var ipInterface in ipInterfaces)
                {
                    // Assign the IP Interface
                    entityModel.IpInterface = ipInterface;

                    var createdDate = DateTime.UtcNow;

                    var newAcl = new Acl
                                     {
                                         Name = entityModel.IpAddress,
                                         Prefix = entityModel.Prefix,
                                         AclListTypeId = (int)entityModel.AclType,
                                         AclEntityTypeId = (int)Models.AclEntityType.IpAddress,
                                         AclSourceTypeId = (int)entityModel.AclSource,
                                         CreatedAt = createdDate,
                                         CreatedBy = entityModel.UserData.UserName,
                                         LastModifiedAt = createdDate,
                                         LastModifiedBy = entityModel.UserData.UserName,
                                         Visible = true,
                                         AspNetUserId = entityModel.UserData.Id,
                                         InterfaceId = entityModel.IpInterface.id,
                                         InterfaceName = entityModel.IpInterface.vlanName,
                                         InterfaceTypeId = (int)entityModel.InterfaceType
                                     };

                    // Add a history item
                    newAcl.AclHistories.Add(
                        new AclHistory
                            {
                                AclHistoryTypeId = (int)Models.AclHistoryType.Created,
                                CreatedAt = createdDate,
                                LastModifiedAt = createdDate,
                                CreatedBy = entityModel.UserData.UserName,
                                LastModifiedBy = entityModel.UserData.UserName,
                                Name = "Added " + entityModel.AclType.GetDescription() + " List entry"
                            });

                    // Add to the repository
                    this.aclRepository.Add(newAcl);

                    // Save the changes
                    await this.unitOfWork.CommitAsync().ConfigureAwait(false);

                    response.DtoId = newAcl.Id;

                    // Send the IP address to the Cataleya
                    var result = await this.cataleyaService.CreateSecurityAcl(entityModel).ConfigureAwait(false);

                    if (result.success)
                    {
                        // update the database with the new catalya id
                        newAcl.CataleyaSecurityAclObjId = result.data.id;
                        newAcl.CataleyaSecurityAclNodeId = result.data.nodeId;
                        this.aclRepository.Edit(newAcl);
                        await this.unitOfWork.CommitAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        Log.Error("Cataleya Update Failed!!!");
                    }
                }
            }
            else
            {
                // Add a log entry but could
                Log.Info("Skipping adding [" + entityModel.IpAddress + "] To [" + entityModel.AclType + "] already exists");
            }

            return response;
        }

        /// <summary>
        /// Gets the IP interfaces for request.
        /// </summary>
        /// <param name="aclEntityModel">The acl entity model.</param>
        /// <returns>Returns Task&lt;IEnumerable&lt;IpInterface&gt;&gt;.</returns>
        private async Task<IEnumerable<IpInterface>> GetIpInterfacesForRequest(AclEntityModel aclEntityModel)
        {
            IEnumerable<IpInterface> ipInterfaces = null;

            if (aclEntityModel.AclType.Equals(Models.AclListType.WhiteList))
            {
                // Covert the selected Interface to IP Interface
                var targetInterface = await this.cataleyaService.GetTargetInterface(
                    aclEntityModel.InterfaceType,
                    this.defaultNodeId,
                    aclEntityModel.InterfaceId).ConfigureAwait(false);

                ipInterfaces = new List<IpInterface> { targetInterface };
            }
            else
            {
                // This is a Black list entry so get all the public interfaces
                aclEntityModel.InterfaceType = InterfaceType.Sip;
                ipInterfaces = await this.cataleyaService.PublicIpInterfacesFromZonesListAsync(null).ConfigureAwait(false);
            }

            return ipInterfaces;
        }

        /// <summary>
        /// Processes the IP addresses.
        /// </summary>
        /// <param name="ipAddresses">The IP addresses.</param>
        /// <param name="aclFileUploadRequest">The acl file upload request.</param>
        /// <returns>Returns Task&lt;ProcessIpAddressResponse&gt;.</returns>
        private async Task<ProcessIpAddressResponse> ProcessIpAddresses(
            List<string> ipAddresses,
            AclRequestBase aclFileUploadRequest)
        {
            var retVal = new ProcessIpAddressResponse();
            var userData = this.userRepository.Get(aclFileUploadRequest.WebToken.ToString());

            if (userData != null)
            {
                var isTokenValid = await this.cataleyaService.ValidateAccessToken(aclFileUploadRequest.AccessToken)
                                       .ConfigureAwait(false);

                if (isTokenValid.Equals(HttpStatusCode.OK))
                {
                    Log.Info("Received [" + ipAddresses.Count + "] IP Addresses");
                    int? interfaceId = this.GetInterfaceIdFromUserData(userData, aclFileUploadRequest.InterfaceName);

                    // Loop through the IP addresses and add them to the appropriate list
                    foreach (var ipAddress in ipAddresses)
                    {
                        var splitIpAddressPrefix = ipAddress.Split("/".ToCharArray());
                        int splitIpAddressPrefixLength = splitIpAddressPrefix.Length;

                        if (splitIpAddressPrefixLength == 1 || splitIpAddressPrefixLength == 2)
                        {
                            string ipAdd;
                            int prefix = 32;
                            if (splitIpAddressPrefix.Length == 1)
                            {
                                ipAdd = splitIpAddressPrefix[0];
                            }
                            else
                            {
                                ipAdd = splitIpAddressPrefix[0];
                                prefix = Convert.ToInt32(splitIpAddressPrefix[1]);
                            }

                            var entityModel = new AclEntityModel
                                                  {
                                                      IpAddress = ipAdd,
                                                      AclType = aclFileUploadRequest.AclType,
                                                      AclSource = aclFileUploadRequest.AclSource,
                                                      UserData = userData,
                                                      @InterfaceType = InterfaceType.Sip,
                                                      InterfaceName = aclFileUploadRequest.InterfaceName,
                                                      InterfaceId = interfaceId.GetValueOrDefault(0),
                                                      Prefix = prefix
                            };

                            retVal = await this.ProcessIpAddressAsync(entityModel).ConfigureAwait(false);
                        }
                    }
                }
                else
                {
                    retVal.DtoId = null;
                }
            }
            else
            {
                retVal.DtoId = null;
            }

            return retVal;
        }

        /// <summary>
        /// Gets the interface identifier from user data.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="interfaceName">Name of the interface.</param>
        /// <returns>Returns System.Nullable&lt;System.Int32&gt;.</returns>
        private int? GetInterfaceIdFromUserData(AspNetUser user, string interfaceName)
        {
            var nodeId =
                user.AspNetUserClaims
                 .FirstOrDefault(r => r.ClaimType.Equals(AppConstants.CataleyaDefaultNodeId, StringComparison.InvariantCultureIgnoreCase));

            int? interfaceId = null;
            if (nodeId != null)
            {
                var interfaceListForUser = user.AspNetUserClaims.FirstOrDefault(r => r.ClaimType.Equals(AppConstants.SipInterfacesClaimType + nodeId.ClaimValue));

                if (interfaceListForUser != null)
                {
                    var interfaceList =
                        JsonConvert.DeserializeObject<IEnumerable<FilterListViewModel>>(
                            interfaceListForUser.ClaimValue);

                    // Should have an interface list now
                    var sipInterface = interfaceList.FirstOrDefault(
                        r => r.Name.Equals(interfaceName, StringComparison.InvariantCultureIgnoreCase));

                    if (sipInterface != null)
                    {
                        interfaceId = sipInterface.Id;
                    }
                }
            }

            return interfaceId;
        }

        /// <summary>
        /// Gets the ip addresses.
        /// </summary>
        /// <param name="fileData">The file data.</param>
        /// <returns>Returns List&lt;System.String&gt;.</returns>
        private List<string> GetIpAddresses(MemoryStream fileData)
        {
            var ipAddresses = new List<string>();
            fileData.Position = 0;

            try
            {
                using (StreamReader reader = new StreamReader(fileData, Encoding.UTF8))
                {
                    var rawString = reader.ReadToEnd();

                    StringReader strReader = new StringReader(rawString);
                    while (true)
                    {
                        var ipAddress = strReader.ReadLine();
                        if (!string.IsNullOrEmpty(ipAddress))
                        {
                            ipAddresses.Add(ipAddress);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }

            return ipAddresses;
        }
    }
}