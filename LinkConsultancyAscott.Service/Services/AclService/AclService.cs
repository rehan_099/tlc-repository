﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the ACL Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.AclService
{
    using System;
    using System.IO;

    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;

    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.Models.Cataleya.Common;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.MediaInterface;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
    using LinkConsultancyAscott.Service.Repository.AclHistoryRepository;
    using LinkConsultancyAscott.Service.Repository.AclRepository;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.AclUploadService;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels;
    using LinkConsultancyAscott.Service.ViewModels.Acl;
    
    /// <summary>
    /// Class StaticDataService.
    /// </summary>
    public class AclService : ServiceBase, IAclService
    {
        /// <summary>
        /// The application configuration repository
        /// </summary>
        private readonly ICurrentUserService currentUserService;

        /// <summary>
        /// The ACL History repository
        /// </summary>
        private readonly IAclHistoryRepository aclHistoryRepository;
        
        /// <summary>
        /// The role repository
        /// </summary>
        private readonly IAclRepository aclRepository;

        /// <summary>
        /// The ACL upload service
        /// </summary>
        private readonly IAclUploadService aclUploadService;

        /// <summary>
        /// The CATALEYA service
        /// </summary>
        private readonly ICataleyaService cataleyaService;

        /// <summary>
        /// The user repository
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// The unit of work
        /// </summary>
        private readonly IUnitOfWork unitOfWork;

        /// <summary>
        /// The default node identifier
        /// </summary>
        private readonly int defaultNodeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="AclService" /> class.
        /// </summary>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="aclHistoryRepository">The ACL History repository.</param>
        /// <param name="aclRepository">The acl repository.</param>
        /// <param name="aclUploadService">The acl upload service.</param>
        /// <param name="cataleyaService">The cataleya service.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public AclService(
            ICurrentUserService currentUserService,
            IAclHistoryRepository aclHistoryRepository,
            IAclRepository aclRepository,
            IAclUploadService aclUploadService,
            ICataleyaService cataleyaService,
            IUserRepository userRepository,
            IUnitOfWork unitOfWork)
        {
            this.currentUserService = currentUserService;
            this.aclHistoryRepository = aclHistoryRepository;
            this.aclRepository = aclRepository;
            this.aclUploadService = aclUploadService;
            this.cataleyaService = cataleyaService;
            this.userRepository = userRepository;
            this.unitOfWork = unitOfWork;
            this.defaultNodeId = this.currentUserService.CataleyaDefaultNodeId;
        }

        /// <summary>
        /// Reads the ACL history.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="showAllRows">if set to <c>true</c> [show all rows].</param>
        /// <returns>Returns IQueryable&lt;AclHistoryViewModel&gt;.</returns>
        public IQueryable<AclHistoryViewModel> ReadAclHistory(string user, bool showAllRows)
        {
            var result = this.aclHistoryRepository.GetAllQueryableExpanded()
              .Select(
                  details => new AclHistoryViewModel
                  {
                      Id = details.Id,
                      AclListType = details.AclListType,
                      Name = details.Name,
                      InterfaceName = details.InterfaceName,
                      InterfaceTypeName = details.InterfaceTypeName,
                      InterfaceTypeId = details.InterfaceTypeId,
                      AclHistoryCreatedAt = details.AclHistoryCreatedAt,
                      AclHistoryCreatedBy = details.AclHistoryCreatedBy,
                      AclSourceType = details.AclSourceType,
                      AclHistoryName = details.AclHistoryName,
                      CreatedAt = details.CreatedAt,
                      CreatedBy = details.CreatedBy,
                      LastModifiedAt = details.LastModifiedAt,
                      LastModifiedBy = details.LastModifiedBy,
                      AclEntityType = details.AclEntityType,
                      InterfaceId = details.InterfaceId,
                      DisplayOrder = details.DisplayOrder,
                      CataleyaSecurityAclNodeId = details.CataleyaSecurityAclNodeId,
                      Visible = details.Visible,
                      CataleyaSecurityAclObjId = details.CataleyaSecurityAclObjId,
                      Prefix = details.Prefix,
                      AclHistoryTypeId = details.AclHistoryTypeId,
                      AclHistoryType = details.AclHistoryType,
                      AclEntityTypeId = details.AclEntityTypeId,
                      AclHistoryId = details.AclHistoryId,
                      AclListTypeId = details.AclListTypeId,
                      AclSourceTypeId = details.AclSourceTypeId,
                      AspNetUserId = details.AspNetUserId,
                      OwnerEmail = details.OwnerEmail,
                      OwnerUserName = details.OwnerUserName
                  });

            if (!showAllRows)
            {
                result = result.Where(r => r.CreatedBy.Equals(user, StringComparison.InvariantCultureIgnoreCase));
            }

            result = result.Where(r => r.CataleyaSecurityAclNodeId == this.defaultNodeId);
            return result;
        }

        /// <summary>
        /// Reads the cataleya acl list.
        /// </summary>
        /// <returns>IQueryable&lt;CataleyaAclListViewModel&gt;.</returns>
        public async Task<IQueryable<CataleyaAclListViewModel>> ReadCataleyaAclList()
        {
            var cataleyaData = await this.cataleyaService.SecurityAclListAsync(null).ConfigureAwait(false);

            var retVal = cataleyaData.data.Select(data => 
                new CataleyaAclListViewModel
                    {
                        Id = data.id,
                        Action = data.action,
                        AppType = data.appType,
                        CompoundKeyId = data.compoundKey.id,
                        CompoundKeyNodeId = data.compoundKey.nodeid,
                        IpinterfaceId = data.ipinterfaceId,
                        LocalPort = data.localPort,
                        NodeId = data.nodeId,
                        OperatorId = data.operatorId,
                        RemoteIpAddress = data.remoteIpAddress,
                        RemotePort = data.remotePort,
                        RemotePrefix = data.remotePrefix,
                        Transport = data.transport,
                        Ver = data.ver,
                        IpInterfaceCompoundKeyId = data.ipInterface.compoundKey.id,
                        IpInterfaceCompoundKeyNodeId = data.ipInterface.compoundKey.nodeid,
                        IpInterfaceId = data.ipInterface.id,
                        IpInterfaceAddressType = data.ipInterface.addressType,
                        IpInterfaceIpAddress = data.ipInterface.ipaddress,
                        IpInterfaceIpSubnetId = data.ipInterface.ipSubnetId,
                        IpInterfaceNodeId = data.ipInterface.nodeId,
                        IpInterfaceOperatorId = data.ipInterface.operatorId,
                        IpInterfaceVlanId = data.ipInterface.vlanId,
                        IpInterfaceVer = data.ipInterface.ver,
                        IpInterfaceVlanName = data.ipInterface.vlanName
                }).ToList();

            return retVal.AsQueryable();
        }

        /// <summary>
        /// Reads the cataleya media interface list.
        /// </summary>
        /// <returns>Returns Task&lt;IQueryable&lt;CataleyaMediaInterfaceListViewModel&gt;&gt;.</returns>
        public async Task<IQueryable<CataleyaMediaInterfaceListViewModel>> ReadCataleyaMediaInterfaceList()
        {
            var cataleyaData = await this.cataleyaService.InterfaceList<MediaInterface>(InterfaceType.Media, null).ConfigureAwait(false);

            var retVal = cataleyaData.data.Select(data =>
                new CataleyaMediaInterfaceListViewModel
                {
                    Id = data.id,
                    CompoundKeyId = data.compoundKey.id,
                    CompoundKeyNodeId = data.compoundKey.nodeid,
                    NodeId = data.nodeId,
                    OperatorId = data.operatorId,
                    Ver = data.ver,
                    IpInterfaceId = data.ipInterfaceId,
                    IpInterfaceCompoundKeyId = data.ipInterface.compoundKey.id,
                    IpInterfaceCompoundKeyNodeId = data.ipInterface.compoundKey.nodeid,
                    IpInterface_Id = data.ipInterface.id,
                    IpInterfaceAddressType = data.ipInterface.addressType,
                    IpInterfaceIpAddress = data.ipInterface.ipaddress,
                    IpInterfaceIpSubnetId = data.ipInterface.ipSubnetId,
                    IpInterfaceNodeId = data.ipInterface.nodeId,
                    IpInterfaceOperatorId = data.ipInterface.operatorId,
                    IpInterfaceVlanId = data.ipInterface.vlanId,
                    IpInterfaceVer = data.ipInterface.ver,
                    IpInterfaceVlanName = data.ipInterface.vlanName,
                    OperatorName = data.@operator.name,
                    Bandwidth = data.bandwidth,
                    BandwidthReserved = data.bandwidthReserved,
                    EndPort = data.endPort,
                    StartPort = data.startPort,
                    Name = data.name,
                    Description = data.description
                }).ToList();

            return retVal.AsQueryable();
        }

        /// <summary>
        /// Reads the cataleya sip interface list.
        /// </summary>
        /// <returns>Returns Task&lt;IQueryable&lt;CataleyaSipInterfaceListViewModel&gt;&gt;.</returns>
        public async Task<IQueryable<CataleyaSipInterfaceListViewModel>> ReadCataleyaSipInterfaceList()
        {
            var cataleyaData = await this.cataleyaService.InterfaceList<SipInterface>(InterfaceType.Sip, null).ConfigureAwait(false);

            var retVal = cataleyaData.data.Select(data =>
                new CataleyaSipInterfaceListViewModel
                {
                    Id = data.id,
                    CompoundKeyId = data.compoundKey.id,
                    CompoundKeyNodeId = data.compoundKey.nodeid,
                    NodeId = data.nodeId,
                    OperatorId = data.operatorId,
                    Ver = data.ver,
                    IpInterfaceId = data.ipInterfaceId,
                    IpInterfaceCompoundKeyId = data.ipInterface.compoundKey.id,
                    IpInterfaceCompoundKeyNodeId = data.ipInterface.compoundKey.nodeid,
                    IpInterface_Id = data.ipInterface.id,
                    IpInterfaceAddressType = data.ipInterface.addressType,
                    IpInterfaceIpAddress = data.ipInterface.ipaddress,
                    IpInterfaceIpSubnetId = data.ipInterface.ipSubnetId,
                    IpInterfaceNodeId = data.ipInterface.nodeId,
                    IpInterfaceOperatorId = data.ipInterface.operatorId,
                    IpInterfaceVlanId = data.ipInterface.vlanId,
                    IpInterfaceVer = data.ipInterface.ver,
                    IpInterfaceVlanName = data.ipInterface.vlanName,
                    OperatorName = data.@operator.name,
                    Transport = data.transport,
                    OperatorDescription = string.Empty,
                    Port = data.port,
                    Shared = data.shared,
                    Name = data.name,
                    Description = data.description
                }).ToList();

            return retVal.AsQueryable();
        }

        public async Task<IQueryable<CataleyaIpInterfaceListViewModel>> ReadCataleyaIpInterfaceList()
        {
            var cataleyaData = await this.cataleyaService.InterfaceList<IpInterface>(InterfaceType.Ip, null).ConfigureAwait(false);

            var retVal = cataleyaData.data.Select(data =>
                new CataleyaIpInterfaceListViewModel
                {
                    Id = data.id,
                    CompoundKeyId = data.compoundKey.id,
                    CompoundKeyNodeId = data.compoundKey.nodeid,
                    NodeId = data.nodeId,
                    OperatorId = data.operatorId,
                    Ver = data.ver,
                    IpInterfaceId = data.id,
                    IpInterfaceCompoundKeyId = data.compoundKey.id,
                    IpInterfaceCompoundKeyNodeId = data.compoundKey.nodeid,
                    IpInterface_Id = data.id,
                    IpInterfaceAddressType = data.addressType,
                    IpInterfaceIpAddress = data.ipaddress,
                    IpInterfaceIpSubnetId = data.ipSubnetId,
                    IpInterfaceNodeId = data.nodeId,
                    IpInterfaceOperatorId = data.operatorId,
                    IpInterfaceVlanId = data.vlanId,
                    IpInterfaceVer = data.ver,
                    IpInterfaceVlanName = data.vlanName,
                }).ToList();

            return retVal.AsQueryable();
        }

        /// <summary>
        /// Updates the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void UpdateAclHistory(AclHistoryViewModel viewModel)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Destroys the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void DestroyAclHistory(AclHistoryViewModel viewModel)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Reads the acl list.
        /// </summary>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <param name="user">The user.</param>
        /// <param name="showAllRows">if set to <c>true</c> [show all rows].</param>
        /// <returns>IQueryable&lt;AclListViewModel&gt;.</returns>
        public IQueryable<AclListViewModel> ReadAclList(AclListType aclListType, string user, bool showAllRows)
        {
            var list = this.aclRepository.GetAllQueryable();

            var result = list.Where(a => a.AclListTypeId.Equals((int)aclListType)).Select(
                    details => new AclListViewModel
                    {
                        Id = details.Id,
                        Name = details.Name,
                        Prefix = details.Prefix,
                        AclEntityType = new FilterListViewModel
                        {
                            Id = details.AclEntityTypeId,
                            Name = details.AclEntityType.Name
                        },

                        AclListType = new FilterListViewModel
                        {
                            Id = details.AclListTypeId,
                            Name = details.AclListType.Name
                        },

                        AclSourceType = new FilterListViewModel
                        {
                            Id = details.AclSourceTypeId,
                            Name = details.AclSourceType.Name
                        },
                        CreatedAt = details.CreatedAt,
                        CreatedBy = details.CreatedBy,
                        LastModifiedAt = details.LastModifiedAt,
                        LastModifiedBy = details.LastModifiedBy,
                        CataleyaSecurityAclNodeId = details.CataleyaSecurityAclNodeId,
                        Interface = new InterfaceFilterListViewModel
                        {
                            Id = details.InterfaceId,
                            Name = details.InterfaceName,
                            InterfaceType = (InterfaceType)details.InterfaceTypeId,
                            InterfaceTypeName = details.InterfaceType.Name
                        }
                });

            // Filter by user if necessary
            if (!showAllRows)
            {
                result = result.Where(r => r.CreatedBy.Equals(user, StringComparison.InvariantCultureIgnoreCase));
            }

            result = result.Where(r => r.CataleyaSecurityAclNodeId == this.defaultNodeId);

            return result;
        }

        /// <summary>
        /// Creates the acl list.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="user">The user.</param>
        /// <returns>AclListViewModel.</returns>
        public async Task<AclListViewModel> CreateAclList(AclListViewModel viewModel, string user)
        {
            AclListViewModel newViewModel = null;

            var userData = this.userRepository.GetByName(user);

            var aclListType = (AclListType)viewModel.AclListType.Id;


                // Process White List
                var aclEntityModel = new AclEntityModel
                                         {
                                             IpAddress = viewModel.Name,
                                             AclType = aclListType,
                                             AclSource = (AclSourceType)viewModel.AclSourceType.Id,
                                             UserData = userData,
                                             Prefix = viewModel.Prefix,
                                             InterfaceType = viewModel.Interface.InterfaceType,
                                             InterfaceId = viewModel.Interface.Id,
                                             InterfaceName = viewModel.Interface.Name
                                         };

                var response = await this.aclUploadService.ProcessIpAddressAsync(aclEntityModel).ConfigureAwait(false);

                if (response.DtoId.HasValue)
                {
                    var newDto = this.aclRepository.Get(response.DtoId.Value);
                    newViewModel = new AclListViewModel(newDto);
                }

            return newViewModel;
        }

        /// <summary>
        /// Updates the acl list.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>AclListViewModel.</returns>
        public AclListViewModel UpdateAclList(AclListViewModel viewModel)
        {
            AclListViewModel retVal;
            var dto = this.aclRepository.Get(viewModel.Id);

            viewModel.Details = dto;

            viewModel.UpdateDataObject(dto);

            this.aclRepository.Edit(dto);
            this.unitOfWork.Commit();

            var updatedDto = this.aclRepository.Get(dto.Id);
            retVal = new AclListViewModel(updatedDto);
            return retVal;
        }

        /// <summary>
        /// Destroys the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        public void DestroyAclList(AclListViewModel viewModel)
        {
            var dto = this.aclRepository.Get(viewModel.Id);
            this.aclRepository.Delete(dto);
            this.unitOfWork.Commit();
        }

        public void SaveIpAddressFile(UploadIpAddressFileViewModel viewModel, string user, string baseUrl)
        {
            if (viewModel.IpAddressFiles.Count() == 1)
            {
                var ipAddressFile = viewModel.IpAddressFiles.First();
                var memoryStreamer = ReadFileContents(ipAddressFile);

                byte[] fileData = memoryStreamer.GetBuffer();

                // Some browsers send file names with full path.
                // We are only interested in the file name.
                var fileName = Path.GetFileName(ipAddressFile.FileName);

                if (!string.IsNullOrEmpty(fileName))
                {
                    /* var fileExtension = Path.GetExtension(fileName);
                    var fileType = MimeMapping.GetMimeMapping(fileName);

                    var cdrDataPackId = this.cdrDataPackRepository.SaveCdrDataPack(
                        viewModel.ProjectId,
                        viewModel.EquipmentSupplierId,
                        viewModel.ReviewCategoryId,
                        viewModel.ReviewTypeId,
                        viewModel.ReviewDate,
                        viewModel.RidsSubmissionDeadline,
                        fileData,
                        user,
                        fileName,
                        fileType,
                        fileExtension);*/
                }
            }
        }

        /// <summary>
        /// Reads the file contents.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>Returns System.Byte[].</returns>
        private static MemoryStream ReadFileContents(HttpPostedFileBase file)
        {
            var fileStream = file.InputStream;
            var memoryStream = new MemoryStream();
            memoryStream.SetLength(fileStream.Length);
            fileStream.Read(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);
            return memoryStream;
        }
    }
}