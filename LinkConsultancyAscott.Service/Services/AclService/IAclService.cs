﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Static Data Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.AclService
{
    using System.Linq;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.Models;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    /// <summary>
    /// Interface IStaticDataService
    /// </summary>
    public interface IAclService
    {
        /// <summary>
        /// Reads the acl history.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="showAllRows">if set to <c>true</c> [show all rows].</param>
        /// <returns>Returns IQueryable&lt;AclHistoryViewModel&gt;.</returns>
        IQueryable<AclHistoryViewModel> ReadAclHistory(string user, bool showAllRows);

        /// <summary>
        /// Reads the cataleya acl list.
        /// </summary>
        /// <returns>IQueryable&lt;CataleyaAclListViewModel&gt;.</returns>
        Task<IQueryable<CataleyaAclListViewModel>> ReadCataleyaAclList();

        /// <summary>
        /// Reads the cataleya sip interface list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;CataleyaSipInterfaceListViewModel&gt;&gt;.</returns>
        Task<IQueryable<CataleyaSipInterfaceListViewModel>> ReadCataleyaSipInterfaceList();

        /// <summary>
        /// Reads the cataleya media interface list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;CataleyaMediaInterfaceListViewModel&gt;&gt;.</returns>
        Task<IQueryable<CataleyaMediaInterfaceListViewModel>> ReadCataleyaMediaInterfaceList();

        /// <summary>
        /// Reads the cataleya ip interface list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;CataleyaIpInterfaceListViewModel&gt;&gt;.</returns>
        Task<IQueryable<CataleyaIpInterfaceListViewModel>> ReadCataleyaIpInterfaceList();

        /// <summary>
        /// Updates the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        void UpdateAclHistory(AclHistoryViewModel viewModel);

        /// <summary>
        /// Destroys the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        void DestroyAclHistory(AclHistoryViewModel viewModel);


        /// <summary>
        /// Reads the acl list asynchronous.
        /// </summary>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <param name="user">The user.</param>
        /// <param name="showAllRows">if set to <c>true</c> [show all rows].</param>
        /// <returns>Task&lt;IQueryable&lt;AclListViewModel&gt;&gt;.</returns>
        IQueryable<AclListViewModel> ReadAclList(AclListType aclListType, string user, bool showAllRows);

        /// <summary>
        /// Creates the acl list.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="user">The user.</param>
        /// <returns>AclListViewModel.</returns>
        Task<AclListViewModel> CreateAclList(AclListViewModel viewModel, string user);

        /// <summary>
        /// Updates the acl list.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>AclListViewModel.</returns>
        AclListViewModel UpdateAclList(AclListViewModel viewModel);

        /// <summary>
        /// Destroys the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        void DestroyAclList(AclListViewModel viewModel);

        /// <summary>
        /// Saves the ip address file.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="user">The user.</param>
        /// <param name="baseUrl">The base URL.</param>
        void SaveIpAddressFile(UploadIpAddressFileViewModel viewModel, string user, string baseUrl);
    }
}
