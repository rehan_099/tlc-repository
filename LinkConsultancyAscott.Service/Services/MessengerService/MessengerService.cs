﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessengerService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Class MessengerService.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.MessengerService
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using EmailService;

    using Humanizer;
    
    using Models.User;

    using Repository.EmailTemplateRepository;
    using Repository.UserRepository;

    /// <summary>
    /// Class MessengerService.
    /// </summary>
    /// <seealso cref="ServiceBase" />
    /// <seealso cref="IMessengerService" />
    public class MessengerService : ServiceBase, IMessengerService
    {
        /// <summary>
        /// The user repository
        /// </summary>
        private readonly IUserRepository userRepository;


        /// <summary>
        /// The requirement email repository
        /// </summary>
        private readonly IEmailTemplateRepository emailTemplateRepository;
        
        /// <summary>
        /// The email service
        /// </summary>
        private readonly IEmailService emailService;

        /// <summary>
        /// The email token start
        /// </summary>
        private readonly string emailTokenStart = @"&lt;&lt;";

        /// <summary>
        /// The email token end
        /// </summary>
        private readonly string emailTokenEnd = @"&gt;&gt;";

        /// <summary>
        /// Initializes a new instance of the <see cref="MessengerService" /> class.
        /// </summary>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="emailTemplateRepository">The requirement email repository.</param>
        /// <param name="emailService">The email service.</param>
        public MessengerService(
            IUserRepository userRepository,
            IEmailTemplateRepository emailTemplateRepository,
            IEmailService emailService)
        {
            this.userRepository = userRepository;
            this.emailTemplateRepository = emailTemplateRepository;
            this.emailService = emailService;
        }
        
        /// <summary>
        /// Updates the token dictionary.
        /// </summary>
        /// <param name="tokenDictionary">The token dictionary.</param>
        /// <param name="emailRecipient">The email recipient.</param>
        private void UpdateTokenDictionary(Dictionary<string, string> tokenDictionary, UserMembershipRole emailRecipient)
        {
            tokenDictionary["ContactName"] = emailRecipient.ContactName;
            tokenDictionary["ContactEmailAddress"] = emailRecipient.ContactEmailAddress;
            tokenDictionary["UserDocuments"] = this.BuildDocumentHtmlTag(emailRecipient.RoleToDocumentsMap);
            var roles = emailRecipient.RoleToDocumentsMap.Keys.ToList();
            tokenDictionary["RoleGroupList"] = roles.Humanize();
        }

        /// <summary>
        /// Builds the document HTML tag.
        /// </summary>
        /// <param name="roleToDocumentsMap">The role to documents map.</param>
        /// <returns>Returns System.String.</returns>
        private string BuildDocumentHtmlTag(Dictionary<string, List<string>> roleToDocumentsMap)
        {
            var sb = new StringBuilder();
            foreach (var roleToDocument in roleToDocumentsMap)
            {
                sb.AppendLine("<hr />");
                sb.AppendFormat("<p><b>Role: {0}</b></p>", roleToDocument.Key);

                foreach (var document in roleToDocument.Value)
                {
                    sb.AppendFormat("<p>{0}</p>", document);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Updates the email data tokens.
        /// </summary>
        /// <param name="emailBody">The email body.</param>
        /// <param name="dataTokens">The data tokens.</param>
        /// <returns>System String.</returns>
        private string UpdateEmailDataTokens(string emailBody, Dictionary<string, string> dataTokens)
        {
            foreach (var dataToken in dataTokens)
            {
                var searchToken = this.emailTokenStart + dataToken.Key + this.emailTokenEnd;
                emailBody = emailBody.Replace(searchToken, dataToken.Value);
            }

            return emailBody;
        }
    }
}