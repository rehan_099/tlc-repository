﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMessengerService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the IMessengerService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.MessengerService
{
    /// <summary>
    /// Interface IMessengerService
    /// </summary>
    public interface IMessengerService
    {

    }
}
