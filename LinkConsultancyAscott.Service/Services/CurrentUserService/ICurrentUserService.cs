﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICurrentUserService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The ICurrentUserService Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.CurrentUserService
{

    /// <summary>
    /// Interface ICurrentUserService
    /// </summary>
    public interface ICurrentUserService
    {
        /// <summary>
        /// Users the name.
        /// </summary>
        /// <returns>System.String.</returns>
        string UserName();

        /// <summary>
        /// Gets the cataleya API URL.
        /// </summary>
        /// <value>The cataleya API URL.</value>
        string CataleyaApiUrl { get; }

        /// <summary>
        /// Gets the cataleya username.
        /// </summary>
        /// <value>The cataleya username.</value>
        string CataleyaUsername { get; }

        /// <summary>
        /// Gets the cataleya password.
        /// </summary>
        /// <value>The cataleya password.</value>
        string CataleyaPassword { get; }

        /// <summary>
        /// Gets the cataleya default node identifier.
        /// </summary>
        /// <value>The cataleya default node identifier.</value>
        int CataleyaDefaultNodeId { get; }
    }
}