﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuditLogService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Site Activity Log Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.AuditLogService
{
    using System.Linq;

    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// Interface IAuditLogService
    /// </summary>
    public interface IAuditLogService
    {
        /// <summary>
        /// Reads the specified data source request.
        /// </summary>
        /// <returns>Data Source Result.</returns>
        IQueryable<AuditLogViewModel> Read();
    }
}
