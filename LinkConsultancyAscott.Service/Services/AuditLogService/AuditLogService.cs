﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditLogService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Site Activity Log Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.AuditLogService
{
    using System.Linq;

    using LinkConsultancyAscott.Service.Repository.AuditLogRepository;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// Class AuditLogService.
    /// </summary>
    public class AuditLogService : IAuditLogService
    {
        /// <summary>
        /// The repository
        /// </summary>
        private readonly IAuditLogRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditLogService"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public AuditLogService(IAuditLogRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Reads the specified request.
        /// </summary>
        /// <returns>
        /// The lit of view models.
        /// </returns>
        public IQueryable<AuditLogViewModel> Read()
        {
            return this.repository.GetAll().Select(
                    dto => new AuditLogViewModel
                    {
                        Id = dto.Id,
                        Source = dto.Source,
                        Timestamp = dto.Timestamp,
                        EventType = dto.EventType,
                        EntityId = dto.EntityId,
                        EventDescription = dto.EventDescription,
                        EntityName = dto.EntityName,
                        /*EventDetail = dto.EventDetail,*/
                        UserName = dto.UserName
                    });
        }
    }
}