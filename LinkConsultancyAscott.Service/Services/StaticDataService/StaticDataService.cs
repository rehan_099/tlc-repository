﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StaticDataService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   Defines the Static Data Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.StaticDataService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Extensions;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.MediaInterface;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SipInterface;
    using LinkConsultancyAscott.Service.Repository.AppConfigRepository;
    using LinkConsultancyAscott.Service.Repository.UserRepository;
    using LinkConsultancyAscott.Service.Services.CataleyaService;
    using LinkConsultancyAscott.Service.Services.CurrentUserService;
    using LinkConsultancyAscott.Service.Types;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    using Newtonsoft.Json;

    using ViewModels;

    using InterfaceType = LinkConsultancyAscott.Service.Models.InterfaceType;

    /// <summary>
    /// Class StaticDataService.
    /// </summary>
    public class StaticDataService : ServiceBase, IStaticDataService
    {
        /// <summary>
        /// The role repository
        /// </summary>
        private readonly ICataleyaService cataleyaService;

        /// <summary>
        /// The user repository
        /// </summary>
        private readonly IUserRepository userRepository;

        /// <summary>
        /// The application configuration repository
        /// </summary>
        private readonly ICurrentUserService currentUserService;

        /// <summary>
        /// The default node identifier
        /// </summary>
        private readonly int defaultNodeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticDataService" /> class.
        /// </summary>
        /// <param name="currentUserService">The current user service.</param>
        /// <param name="cataleyaService">The role repository.</param>
        /// <param name="userRepository">The user repository.</param>
        public StaticDataService(
            ICurrentUserService currentUserService,
            ICataleyaService cataleyaService,
            IUserRepository userRepository)
        {
            this.currentUserService = currentUserService;
            this.cataleyaService = cataleyaService;
            this.userRepository = userRepository;
            this.defaultNodeId = this.currentUserService.CataleyaDefaultNodeId;
        }

        /// <summary>
        /// Gets the sip interfaces list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;FilterListViewModel&gt;&gt;.</returns>
        public async Task<IQueryable<FilterListViewModel>> GetSipInterfacesList()
        {
            var interfaceList = await this.cataleyaService.InterfaceList<SipInterface>(InterfaceType.Sip, null).ConfigureAwait(false);

            var retVal = interfaceList.data
                .Select(interfaceItem =>
                    new FilterListViewModel { Id = interfaceItem.id, Name = interfaceItem.name }).ToList();

            return retVal.AsQueryable();
        }

        /// <summary>
        /// Gets the media interfaces list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;FilterListViewModel&gt;&gt;.</returns>
        public async Task<IQueryable<FilterListViewModel>> GetMediaInterfacesList()
        {
            var interfaceList = await this.cataleyaService.InterfaceList<MediaInterface>(InterfaceType.Media, null).ConfigureAwait(false);

            var retVal = interfaceList.data
                           .Select(interfaceItem =>
                               new FilterListViewModel
                                   {
                                       Id = interfaceItem.id,
                                       Name = interfaceItem.name
                                   }).ToList();

            return retVal.AsQueryable();
        }

        /// <summary>
        /// Gets the user interfaces list.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Task&lt;IQueryable&lt;InterfaceFilterListViewModel&gt;&gt;.</returns>
        public async Task<IQueryable<InterfaceFilterListViewModel>> GetUserInterfacesListAsync(string user)
        {
            var userData = await this.userRepository.GetByNameAsync(user).ConfigureAwait(false);
            var retVal = new List<InterfaceFilterListViewModel>();

            if (userData == null)
            {
                return retVal.AsQueryable();
            }

            retVal.AddRange(
                this.Convert(
                    InterfaceType.Sip, 
                    userData.AspNetUserClaims.FirstOrDefault(
                        c => c.ClaimType.Equals(AppConstants.SipInterfacesClaimType + this.defaultNodeId, StringComparison.InvariantCultureIgnoreCase))));

            retVal.AddRange(
                this.Convert(
                    InterfaceType.Media, 
                    userData.AspNetUserClaims.FirstOrDefault(
                        c => c.ClaimType.Equals(AppConstants.MediaInterfacesClaimType + this.defaultNodeId, StringComparison.InvariantCultureIgnoreCase))));

            return retVal.AsQueryable();
        }

        /// <summary>
        /// Converts the specified interface type.
        /// </summary>
        /// <param name="interfaceType">Type of the interface.</param>
        /// <param name="userClaim">The user claim.</param>
        /// <returns>IEnumerable&lt;InterfaceFilterListViewModel&gt;.</returns>
        private IEnumerable<InterfaceFilterListViewModel> Convert(
            InterfaceType interfaceType,
            AspNetUserClaim userClaim)
        {
            var retVal = new List<InterfaceFilterListViewModel>();

            if (!string.IsNullOrEmpty(userClaim?.ClaimValue))
            {
                retVal.AddRange(
                    JsonConvert.DeserializeObject<IEnumerable<FilterListViewModel>>(userClaim?.ClaimValue)
                        .Select(filterViewModel => new InterfaceFilterListViewModel
                                                       {
                                                           Id = filterViewModel.Id,
                                                           Name = filterViewModel.Name,
                                                           InterfaceType = interfaceType,
                                                           InterfaceTypeName = interfaceType.GetDescription()
                                                       }));
            }

            return retVal;
        }
    }
}