﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStaticDataService.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The Static Data Service Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Services.StaticDataService
{
    using System.Linq;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.ViewModels;
    using LinkConsultancyAscott.Service.ViewModels.Acl;

    /// <summary>
    /// Interface IStaticDataService
    /// </summary>
    public interface IStaticDataService
    {
        /// <summary>
        /// Gets the sip interfaces list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;FilterListViewModel&gt;&gt;.</returns>
        Task<IQueryable<FilterListViewModel>> GetSipInterfacesList();

        /// <summary>
        /// Gets the media interfaces list.
        /// </summary>
        /// <returns>Task&lt;IQueryable&lt;FilterListViewModel&gt;&gt;.</returns>
        Task<IQueryable<FilterListViewModel>> GetMediaInterfacesList();

        /// <summary>
        /// Gets the user interfaces list.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Task&lt;IQueryable&lt;InterfaceFilterListViewModel&gt;&gt;.</returns>
        Task<IQueryable<InterfaceFilterListViewModel>> GetUserInterfacesListAsync(string user);
    }
}
