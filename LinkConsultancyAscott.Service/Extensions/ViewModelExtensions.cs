﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewModelExtensions.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2015
// </copyright>
// <summary>
//   The enum extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Extensions
{
    using System;

    using Newtonsoft.Json;


    /// <summary>
    /// The enumeration type extensions.
    /// </summary>
    public static class ViewModelExtensions
    {

        /// <summary>
        /// Gets the full description.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>Returns System.String.</returns>
        public static string GetFullDescription(object obj)
        {
            string description;
            try
            {
                description = JsonConvert.SerializeObject(
                    obj,
                    Formatting.Indented,
                                new JsonSerializerSettings
                                {
                                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                });
            }
            catch (Exception ex)
            {
                description = "Error serialising view model " + ex.Message;
            }

            return description;
        }
    }
}
