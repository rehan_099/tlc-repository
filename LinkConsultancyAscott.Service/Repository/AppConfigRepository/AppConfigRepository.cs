﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfigRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The AppConfig repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AppConfigRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The AppConfig Repository.
    /// </summary>
    public class AppConfigRepository : LookUpRepository<int, AppConfig>, IAppConfigRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AppConfigRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// Gets the default claim types.
        /// </summary>
        /// <returns>Returns IEnumerable&lt;System.String&gt;.</returns>
        public IEnumerable<string> GetDefaultClaimTypes()
        {
            return this.Dbset.Select(c => c.Name).ToList();
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Returns System.String.</returns>
        public string GetValue(string key)
        {
            var item = this.Dbset
                .FirstOrDefault(t => t.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase));
            var retVal = string.Empty;

            if (item != null)
            {
                retVal = item.Value;
            }

            return retVal;
        }
    }
}