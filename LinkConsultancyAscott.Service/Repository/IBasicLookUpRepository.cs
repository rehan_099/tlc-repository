﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILookUpRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The LookUpRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;
    using System.Linq;

    using Data;
    using System.Collections.Generic;

    /// <summary>
    /// Interface ILookUpRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.IAuditedEntityRepositoryBase{T, TM}" />
    public interface IBasicLookUpRepository<T, TM> : IAuditedEntityRepositoryBase<T, TM> where TM : class, IBasicLookUp<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// The get all visible.
        /// </summary>
        /// <returns>
        /// The list of visible items.
        /// </returns>
        IQueryable<TM> GetAllVisible();

        /// <summary>
        /// Gets all visible list.
        /// </summary>
        /// <returns>IEnumerable&lt;TM&gt;.</returns>
        IEnumerable<TM> GetAllVisibleList();
    }
}
