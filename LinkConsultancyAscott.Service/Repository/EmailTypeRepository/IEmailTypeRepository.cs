﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The RequirementType Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.EmailTypeRepository
{
    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The EmailType Repository interface.
    /// </summary>
    public interface IEmailTypeRepository : ILookUpRepository<int, EmailType>
    {
    }
}
