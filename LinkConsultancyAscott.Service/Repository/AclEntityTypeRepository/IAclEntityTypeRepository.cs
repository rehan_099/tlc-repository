﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclEntityTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The RequirementType Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclEntityTypeRepository
{
    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The AclEntityType Repository interface.
    /// </summary>
    public interface IAclEntityTypeRepository : ILookUpRepository<int, AclEntityType>
    {
    }
}
