﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclEntityTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The AclEntityType repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclEntityTypeRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The AclEntityType Repository.
    /// </summary>
    public class AclEntityTypeRepository : LookUpRepository<int, AclEntityType>, IAclEntityTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclEntityTypeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AclEntityTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}