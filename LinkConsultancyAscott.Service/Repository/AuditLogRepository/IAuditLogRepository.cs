﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuditLogRepository.cs" company="The Link Consultancy - Ascott">
//    The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the IAuditLogRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AuditLogRepository
{
    using System.Linq;

    using LinkConsultancyAscott.Data;

    /// <summary>
    /// Interface IAuditLogRepository
    /// </summary>
    public interface IAuditLogRepository : IDataEntityRepositoryBase<long, AuditLog>
    {
        /// <summary>
        /// Gets the summary.
        /// </summary>
        /// <returns>IEnumerable&lt;vw_AuditLog&gt;.</returns>
        IQueryable<vw_AuditLog> GetSummary();
    }
}
