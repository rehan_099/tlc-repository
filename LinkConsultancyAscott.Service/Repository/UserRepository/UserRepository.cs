﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The site activity log repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.UserRepository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Models.User;
    using LinkConsultancyAscott.Service.Repository;

    /// <summary>
    /// The User Repository.
    /// </summary>
    public class UserRepository : RepositoryBase, IUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The List of Users.
        /// </returns>
        public IEnumerable<vw_UsersInRoles> GetAll()
        {
            return this.Database.vw_UsersInRoles.ToList();
        }

        /// <summary>
        /// Gets all users in roles.
        /// </summary>
        /// <returns>IQueryable&lt;vw_UsersInRoles&gt;.</returns>
        public IQueryable<vw_UsersInRoles> GetAllUsersInRoles()
        {
            return this.Database.vw_UsersInRoles;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The users.
        /// </returns>
        public AspNetUser Get(string userId)
        {
            return this.Database.AspNetUsers
                .Include(t => t.AspNetUserClaims)
                .FirstOrDefault(u => u.Id == userId);
        }

        /// <summary>
        /// Gets the name of the by.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>Returns aspnet_Users.</returns>
        public AspNetUser GetByName(string userName)
        {
            return this.Database.AspNetUsers
                    .Include(t => t.AspNetUserClaims)
                    .FirstOrDefault(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Gets the name of the by.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>Returns aspnet_Users.</returns>
        public async Task<AspNetUser> GetByNameAsync(string userName)
        {
            return await this.Database.AspNetUsers
                    .Include(t => t.AspNetUserClaims)
                    .FirstOrDefaultAsync(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).ConfigureAwait(false);
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="userRoles">The user roles.</param>
        public void Add(AspNetUser user, string userRoles)
        {
            var roles = this.Database.AspNetRoles.ToList();

            if (!string.IsNullOrEmpty(userRoles))
            {
                foreach (
                    var roleId in
                    userRoles.Split(",".ToCharArray()).Select(role => role.Split(":".ToCharArray())[0]))
                {
                    user.AspNetRoles.Add(roles.First(r => r.Id == roleId));
                }
            }

            user.Id = Guid.NewGuid().ToString();
            
            this.Database.AspNetUsers.Add(user);
        }

        public void Edit(AspNetUser user)
        {
            this.Entities.Entry(user).State = EntityState.Modified;
        }

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="userRoles">
        /// The user roles.
        /// </param>
        public void Edit(AspNetUser user, string userRoles)
        {
            var roles = this.Database.AspNetRoles.ToList();

            if (user != null)
            {
                user.AspNetRoles.Clear();
                if (!string.IsNullOrEmpty(userRoles))
                {
                    foreach (
                        var roleId in
                        userRoles.Split(",".ToCharArray()).Select(role => role.Split(":".ToCharArray())[0]))
                    {
                        user.AspNetRoles.Add(roles.First(r => r.Id == roleId));
                    }
                }
            }

            this.Entities.Entry(user).State = EntityState.Modified;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="p">The p.</param>
        public void Delete(AspNetUser p)
        {
            if (p != null)
            {
                p.AspNetRoles.Clear();
                this.Entities.Entry(p).State = EntityState.Modified;
                this.Entities.Entry(p).State = EntityState.Deleted;
            }
        }
    }
}
