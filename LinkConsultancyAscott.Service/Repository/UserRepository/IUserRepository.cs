﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The User Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.UserRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Data;

    using LinkConsultancyAscott.Service.Models.User;

    using Repository;

    /// <summary>
    /// The User Repository interface.
    /// </summary>
    public interface IUserRepository : IRepositoryBase
    {
        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The list of users.
        /// </returns>
        IEnumerable<vw_UsersInRoles> GetAll();


        /// <summary>
        /// Gets all users in roles.
        /// </summary>
        /// <returns>IQueryable&lt;vw_UsersInRoles&gt;.</returns>
        IQueryable<vw_UsersInRoles> GetAllUsersInRoles();

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="AspNetUser"/>.
        /// </returns>
        AspNetUser Get(string userId);

        /// <summary>
        /// Gets the name of the by.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>aspnet_Users.</returns>
        AspNetUser GetByName(string userName);

        /// <summary>
        /// Gets the by name asynchronous.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>Task&lt;AspNetUser&gt;.</returns>
        Task<AspNetUser> GetByNameAsync(string userName);

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="userRoles">
        /// The user roles.
        /// </param>
        void Add(AspNetUser user, string userRoles);

        /// <summary>
        /// Edits the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        void Edit(AspNetUser user);

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="userRoles">
        /// The user roles.
        /// </param>
        void Edit(AspNetUser user, string userRoles);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        void Delete(AspNetUser p);
    }
}
