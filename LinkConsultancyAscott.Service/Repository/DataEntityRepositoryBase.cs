﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataEntityRepositoryBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the DataEntityRepositoryBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Validation;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    using Data;

    using Infrastructure;

    /// <summary>
    /// Class DataEntityRepositoryBase.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="DTO">The type of the dto.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.RepositoryBase" />
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.IDataEntityRepositoryBase{T, DTO}" />
    public abstract class DataEntityRepositoryBase<T, DTO> : RepositoryBase, IDataEntityRepositoryBase<T, DTO>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where DTO : class, IDataEntityBase<T>
    {
        /// <summary>
        /// The  database set.
        /// </summary>
        protected readonly IDbSet<DTO> Dbset;

        protected DataEntityRepositoryBase(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
            this.Dbset = this.Database.Set<DTO>();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The list.
        /// </returns>
        public virtual IQueryable<DTO> GetAll()
        {
            try
            {
                IQueryable<DTO> retVal = this.Dbset;
                return retVal;
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets all list.
        /// </summary>
        /// <returns>List of DTOs.</returns>
        public virtual IEnumerable<DTO> GetAllList()
        {
            try
            {
                IEnumerable<DTO> retVal = this.Dbset.ToList();
                return retVal;
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                throw;
            }
        }

        public virtual IQueryable<DTO> GetAllQueryable()
        {
            try
            {
                return this.Dbset;
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                throw;
            }
        }
        
        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="DTO"/>.
        /// </returns>
        public virtual DTO Get(T id)
        {
            var dto = this.Dbset.Find(id);
            this.Entities.Entry(dto).Reload();
            return dto;
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <exception cref="Exception"> Thrown on database error </exception>
        public virtual void Add(DTO p)
        {
            try
            {
                this.Dbset.Add(p);
            }
            catch (Exception ex)
            {
                if (ex is DbEntityValidationException)
                {
                    var deve = ex as DbEntityValidationException;
                    var r = deve.EntityValidationErrors.First<DbEntityValidationResult>();
                }

                throw;
            }
        }

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        public virtual void Edit(DTO p)
        {
            try
            {
                this.Entities.Entry(p).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                if (ex is DbEntityValidationException)
                {
                    var deve = ex as DbEntityValidationException;
                    var r = deve.EntityValidationErrors.First<DbEntityValidationResult>();
                }

                throw;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        public virtual void Delete(DTO p)
        {
           this.Dbset.Remove(p);
        }

        /// <summary>
        /// The serialize all to file.
        /// </summary>
        /// <param name="filename">
        /// The s filename.
        /// </param>
        public void SerialiseAllToFile(string filename)
        {
            var x = new XmlSerializer(typeof(DTO));
            using (StreamWriter streamWriter = System.IO.File.CreateText(filename))
            {
                x.Serialize(streamWriter, this.GetAll());
            }
        }

        /// <summary>
        /// The get new data object.
        /// </summary>
        /// <returns>The <see cref="!:TM" />.</returns>
        public DTO GetNewDataObject()
        {
            return (DTO)Activator.CreateInstance(typeof(DTO));
        }
    }
}
