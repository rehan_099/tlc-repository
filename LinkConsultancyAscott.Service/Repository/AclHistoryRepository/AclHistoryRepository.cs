﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclHistoryRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ACL History repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclHistoryRepository
{
    using System.Linq;

    using Data;
    using Infrastructure;

    /// <summary>
    /// The ACL History Repository.
    /// </summary>
    public class AclHistoryRepository : AuditedEntityRepositoryBase<int, AclHistory>, IAclHistoryRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclHistoryRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AclHistoryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public IQueryable<vw_AclHistory> GetAllQueryableExpanded()
        {
            return this.Database.vw_AclHistory;
        }
    }
}