﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclHistoryRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ACl History Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclHistoryRepository
{
    using System.Linq;

    using Data;

    /// <summary>
    /// The AclHistory Repository interface.
    /// </summary>
    public interface IAclHistoryRepository : IAuditedEntityRepositoryBase<int, AclHistory>
    {
        IQueryable<vw_AclHistory> GetAllQueryableExpanded();
    }
}
