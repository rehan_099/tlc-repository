﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Acl repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclRepository
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    using AclListType = LinkConsultancyAscott.Service.Models.AclListType;

    /// <summary>
    /// The Acl Repository.
    /// </summary>
    public class AclRepository : AuditedEntityRepositoryBase<int, Acl>, IAclRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AclRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>The <see cref="!:DTO" />.</returns>
        public override Acl Get(int id)
        {
            return this.Dbset
                .Include(t => t.AclEntityType)
                .Include(t => t.AclListType)
                .Include(t => t.AclSourceType).FirstOrDefault(i => i.Id.Equals(id));
        }

        /// <summary>
        /// Ips the address exists in list.
        /// </summary>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="aclListType">Type of the acl list.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool IpAddressExistsInList(string ipAddress, AclListType aclListType)
        {
            return this.Dbset.Any(a => a.Name.Equals(ipAddress, StringComparison.InvariantCultureIgnoreCase) &&
                                a.AclListTypeId.Equals((int)aclListType));
        }
    }
}