﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Log Repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.LogRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository;

    /// <summary>
    /// The user data repository.
    /// </summary>
    public class LogRepository : DataEntityRepositoryBase<long, Log>, ILogRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public LogRepository(IDatabaseFactory databaseFactory) 
            : base(databaseFactory)
        {
        }
    }
}