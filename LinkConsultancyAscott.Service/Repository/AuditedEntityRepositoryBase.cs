﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditedEntityRepositoryBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AuditedEntityRepositoryBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// Class AuditedEntityRepositoryBase.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="DTO">The type of the DTO.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.DataEntityRepositoryBase{T, DTO}" />
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.IAuditedEntityRepositoryBase{T, DTO}" />
    public abstract class AuditedEntityRepositoryBase<T, DTO> : DataEntityRepositoryBase<T, DTO>, IAuditedEntityRepositoryBase<T, DTO>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where DTO : class, IAuditedEntity<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuditedEntityRepositoryBase{T, DTO}"/> class.
        /// </summary>
        /// <param name="databaseFactory">The database factory.</param>
        protected AuditedEntityRepositoryBase(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The list.
        /// </returns>
        public override IQueryable<DTO> GetAll()
        {
            return base.GetAll().Where(i => !i.DeletedAt.HasValue);
        }

        /// <summary>
        /// Gets all list.
        /// </summary>
        /// <returns>List of DTOs.</returns>
        public override IEnumerable<DTO> GetAllList()
        {
            try
            {
                IEnumerable<DTO> retVal = this.Dbset.Where(t => !t.DeletedAt.HasValue).ToList();
                return retVal;
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                throw;
            }
        }
    }
}
