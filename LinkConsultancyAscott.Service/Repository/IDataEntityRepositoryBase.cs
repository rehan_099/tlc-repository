﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataEntityRepositoryBase.cs" company="The Link Consultancy - Ascott">
//  The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The DataEntityRepositoryBase interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Data;

    /// <summary>
    /// Interface IDataEntityRepositoryBase
    /// </summary>
    /// <typeparam name="T"> Type of DTO </typeparam>
    /// <typeparam name="TDto">The type of the t dto.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.IRepositoryBase" />
    public interface IDataEntityRepositoryBase<T, TDto> : IRepositoryBase
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where TDto : class, IDataEntityBase<T>
    {
        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The list of items.
        /// </returns>
        IQueryable<TDto> GetAll();

        /// <summary>
        /// Gets all list.
        /// </summary>
        /// <returns> List of DTOs.</returns>
        IEnumerable<TDto> GetAllList();

        /// <summary>
        /// Gets all queryable.
        /// </summary>
        /// <returns>IQueryable&lt;TDto&gt;.</returns>
        IQueryable<TDto> GetAllQueryable();

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="TDto"/>.
        /// </returns>
        TDto Get(T id);

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        void Add(TDto p);

        /// <summary>
        /// The edit.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        void Edit(TDto p);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="p">
        /// The p.
        /// </param>
        void Delete(TDto p);

        /// <summary>
        /// Gets the new data object.
        /// </summary>
        /// <returns>TDto.</returns>
        TDto GetNewDataObject();
    }
}
