﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The RepositoryBase interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    /// <summary>
    /// The RepositoryBase interface.
    /// </summary>
    public interface IRepositoryBase
    {
        string ConnectionString { get;  }
    }
}
