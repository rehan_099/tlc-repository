﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteActivityLogRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The site activity log repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.SiteActivityLogRepository
{
    using System.Linq;
    using System.Transactions;

    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;
    using LinkConsultancyAscott.Service.Repository;
    using LinkConsultancyAscott.Service.ViewModels.Administration;

    /// <summary>
    /// The site activity log repository.
    /// </summary>
    public class SiteActivityLogRepository : DataEntityRepositoryBase<long, SiteActivityLog>, ISiteActivityLogRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteActivityLogRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public SiteActivityLogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        public override IQueryable<SiteActivityLog> GetAll()
        {
            return this.Database.SiteActivityLogs;
        }

        public void AddViaTransaction(SiteActivityLog log)
        {
            using (var transaction = new TransactionScope())
            {
                using (var context = new LinkConsultancyAscottEntities(DatabaseFactory.ConnectionString()))
                {
                    context.SiteActivityLogs.Add(log);
                    context.SaveChanges();
                }
                transaction.Complete();
            }
        }
    }
}
