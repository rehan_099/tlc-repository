﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclListTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The AclListType repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclListTypeRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The AclListType Repository.
    /// </summary>
    public class AclListTypeRepository : LookUpRepository<int, AclListType>, IAclListTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclListTypeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AclListTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}