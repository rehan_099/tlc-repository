﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclListTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The RequirementType Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclListTypeRepository
{
    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The AclListType Repository interface.
    /// </summary>
    public interface IAclListTypeRepository : ILookUpRepository<int, AclListType>
    {
    }
}
