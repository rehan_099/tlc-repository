﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclSourceTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The AclSourceType repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclSourceTypeRepository
{
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Infrastructure;

    /// <summary>
    /// The AclSourceType Repository.
    /// </summary>
    public class AclSourceTypeRepository : LookUpRepository<int, AclSourceType>, IAclSourceTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclSourceTypeRepository"/> class.
        /// </summary>
        /// <param name="databaseFactory">
        /// The database factory.
        /// </param>
        public AclSourceTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}