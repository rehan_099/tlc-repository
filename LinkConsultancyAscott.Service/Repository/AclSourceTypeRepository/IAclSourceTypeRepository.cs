﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAclSourceTypeRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The RequirementType Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.AclSourceTypeRepository
{
    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The AclSourceType Repository interface.
    /// </summary>
    public interface IAclSourceTypeRepository : ILookUpRepository<int, AclSourceType>
    {
    }
}
