﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRoleRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Role Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository.RoleRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Data;

    /// <summary>
    /// The Role Repository interface.
    /// </summary>
    public interface IRoleRepository : IRepositoryBase
    {

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>IEnumerable&lt;aspnet_Roles&gt;.</returns>
        IEnumerable<AspNetRole> GetAll();


        /// <summary>
        /// Gets all queryable.
        /// </summary>
        /// <returns>IQueryable&lt;aspnet_Roles&gt;.</returns>
        IQueryable<AspNetRole> GetAllQueryable();

        /// <summary>
        /// Gets the specified user identifier.
        /// </summary>
        /// <param name="roleId">The user identifier.</param>
        /// <returns>aspnet_Roles.</returns>
        AspNetRole Get(string roleId);


        /// <summary>
        /// Gets the name of the role identifier from.
        /// </summary>
        /// <param name="roleName">Name of the role.</param>
        /// <returns>Guid.</returns>
        string GetRoleIdFromRoleName(string roleName);

        /// <summary>
        /// Adds the specified p.
        /// </summary>
        /// <param name="p">The p.</param>
        void Add(AspNetRole p);

        /// <summary>
        /// Edits the specified p.
        /// </summary>
        /// <param name="p">The p.</param>
        void Edit(AspNetRole p);

        /// <summary>
        /// Deletes the specified p.
        /// </summary>
        /// <param name="p">The p.</param>
        void Delete(AspNetRole p);
    }
}
