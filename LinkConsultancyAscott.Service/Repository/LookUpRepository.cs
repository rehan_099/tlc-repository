﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LookUpRepository.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The look up repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Repository
{
    using System;

    using System.Linq;

    using Data;

    using Infrastructure;

    /// <summary>
    /// Class LookUpRepository.
    /// </summary>
    /// <typeparam name="T"> The key type</typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.BasicLookUpRepository{T, TM}" />
    /// <seealso cref="LinkConsultancyAscott.Service.Repository.ILookUpRepository{T, TM}" />
    public abstract class LookUpRepository<T, TM> : BasicLookUpRepository<T, TM>, ILookUpRepository<T, TM>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where TM : class, ILookUp<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LookUpRepository{T, TM}"/> class.
        /// </summary>
        /// <param name="databaseFactory">The database factory.</param>
        protected LookUpRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The s name.
        /// </param>
        /// <returns>
        /// The <see cref="TM"/>.
        /// </returns>
        public virtual TM GetByName(string name)
        {
            return (from n in this.Dbset
                    where n.Name == name
                    select n).FirstOrDefault();
        }
    }
}