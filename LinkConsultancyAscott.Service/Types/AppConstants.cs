﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConstants.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the AppConstants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Types
{
    /// <summary>
    /// Class AppConstants.
    /// </summary>
    public static class AppConstants
    {
        /// <summary>
        /// The acl type name
        /// </summary>
        public const string AclTypeName = "AclType";

        /// <summary>
        /// The acl source name
        /// </summary>
        public const string AclSourceName = "AclSource";

        /// <summary>
        /// The request identifier name
        /// </summary>
        public const string ApiTokenName = "ApiToken";

        /// <summary>
        /// The access token name
        /// </summary>
        public const string AccessTokenName = "AccessToken";

        /// <summary>
        /// The interface name
        /// </summary>
        public const string InterfaceName = "InterfaceName";

        /// <summary>
        /// The ip address name
        /// </summary>
        public const string IpAddressName = "IpAddress";

        /// <summary>
        /// The prefix name
        /// </summary>
        public const string PrefixName = "Prefix";

        /// <summary>
        /// The ip interfaces claim type
        /// </summary>
        public const string SipInterfacesClaimType = "SipInterfaces";

        /// <summary>
        /// The media interfaces claim type
        /// </summary>
        public const string MediaInterfacesClaimType = "MediaInterfaces";

        /// <summary>
        /// The cataleya API URL key
        /// </summary>
        public const string CataleyaApiUrlKey = "CataleyaApiUrl";

        /// <summary>
        /// The cataleya username key
        /// </summary>
        public const string CataleyaUsernameKey = "CataleyaUsername";

        /// <summary>
        /// The cataleya password key
        /// </summary>
        public const string CataleyaPasswordKey = "CataleyaPassword";

        /// <summary>
        /// The default node identifier key
        /// </summary>
        public const string CataleyaDefaultNodeId = "CataleyaDefaultNodeId";

        /// <summary>
        /// The access public key
        /// </summary>
        public const string AccessPublicKey = "AccessPublic";

        /// <summary>
        /// The application title
        /// </summary>
        public const string AppTitle = "TLC Portal - ";        
    }
}
