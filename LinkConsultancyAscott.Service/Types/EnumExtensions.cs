﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumExtensions.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Class1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.Types
{
    using System;
    using System.ComponentModel;

    public static class EnumExtensions
    {
        public static string Description(this Enum enumeration)
        {
            string value = enumeration.ToString();
            Type type = enumeration.GetType();
            //Use reflection to try and get the description attribute for the enumeration
            DescriptionAttribute[] descAttribute = (DescriptionAttribute[])type.GetField(value).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return descAttribute.Length > 0 ? descAttribute[0].Description : value;
        }
    }
}
