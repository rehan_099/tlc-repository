﻿namespace LinkConsultancyAscott.Service.Types
{
    using System.ComponentModel;

    public enum RenderingExtensions
    {
        /// <summary>
        /// The pdf.
        /// </summary>
        [Description("PDF")]
        Pdf,

        /// <summary>
        /// The xls.
        /// </summary>
        [Description("XLS")]
        Xls,

        /// <summary>
        /// The csv.
        /// </summary>
        [Description("CSV")]
        Csv,

        /// <summary>
        /// The rtf.
        /// </summary>
        [Description("RTF")]
        Rtf,

        /// <summary>
        /// The xps.
        /// </summary>
        [Description("XPS")]
        Xps,

        /// <summary>
        /// The docx.
        /// </summary>
        [Description("DOCX")]
        Docx,

        /// <summary>
        /// The xslx.
        /// </summary>
        [Description("XLSX")]
        Xlsx,

        /// <summary>
        /// The pptx.
        /// </summary>
        [Description("")]
        Pptx
    }
}
