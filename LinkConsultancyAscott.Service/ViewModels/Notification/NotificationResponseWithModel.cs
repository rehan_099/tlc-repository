﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotificationResponseWithModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 
// </copyright>
// <summary>
//   The Notification Response With Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Notification
{
    /// <summary>
    /// The notification response.
    /// </summary>
    public class NotificationResponseWithModel : NotificationResponse
    {
        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        public ViewModelBase ViewModel { get; set; }
    }
}
