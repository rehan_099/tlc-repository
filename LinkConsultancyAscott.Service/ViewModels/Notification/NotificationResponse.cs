﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotificationResponse.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The notification response.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Notification
{
    /// <summary>
    /// The notification response.
    /// </summary>
    public class NotificationResponse
    {
        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        public string ResponseStatus { get; set; }

        /// <summary>
        /// Gets or sets the response message.
        /// </summary>
        public string ResponseMessage { get; set; }
    }
}
