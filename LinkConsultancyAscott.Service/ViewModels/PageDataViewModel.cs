﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PageDataViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Page Data View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The Page Data View Model.
    /// </summary>
    public class PageDataViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the area.
        /// </summary>
        /// <value>The area.</value>
        public string Area { get; set; }

        /// <summary>
        /// Gets or sets the controller.
        /// </summary>
        /// <value>The controller.</value>
        public string Controller { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the title text.
        /// </summary>
        /// <value>The title text.</value>
        public string TitleText { get; set; }

        /// <summary>
        /// Gets or sets the page heading text.
        /// </summary>
        /// <value>The page heading text.</value>
        public string PageHeadingText { get; set; }

        /// <summary>
        /// Gets or sets the page sub heading text.
        /// </summary>
        /// <value>The page sub heading text.</value>
        public string PageSubHeadingText { get; set; }

        /// <summary>
        /// Gets or sets the page text.
        /// </summary>
        /// <value>The page text.</value>
        public string PageText { get; set; }
    }
}
