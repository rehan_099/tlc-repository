﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicLookUpViewModelBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The look up view model base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    using Data;

    using Types;

    /// <summary>
    /// Class LookUpViewModelBase.
    /// </summary>
    /// <typeparam name="T"> The database type</typeparam>
    /// <typeparam name="TM">The type of the tm.</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.ViewModels.AuditableViewModelBase{T, TM}" />
    /// Resharper 8 fails to resolve templates in abstract classes.
    /// https://youtrack.jetbrains.com/issue/RSRP-373171
    /// ReSharper disable Mvc.TemplateNotResolved
    public abstract class BasicLookUpViewModelBase<T, TM> : AuditableViewModelBase<T, TM>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where TM : IBasicLookUp<T>, IAuditedEntity<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BasicLookUpViewModelBase{T, TM}"/> class.
        /// </summary>
        protected BasicLookUpViewModelBase()
        {
            this.Visible = true;
            this.DisplayOrder = 10;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicLookUpViewModelBase{T, TM}"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        protected BasicLookUpViewModelBase(TM details) : base(details)
        {
            this.Visible = details.Visible;
            this.DisplayOrder = details.DisplayOrder;
        }

        /// <summary>
        /// Sets the data object.
        /// </summary>
        /// <value>The data object.</value>
        public override TM DataObject
        {
            set
            {
                base.DataObject = value;
                this.DisplayOrder = this.details.DisplayOrder;
                this.Visible = this.details.Visible;
            }
        }

        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        /// <value>The display order.</value>
        [Required(ErrorMessage = "Display Order is required")]
        [DisplayName("Display Order")]
        [Display(Order = 990)]
        [UIHint("DisplayOrder")]
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="LookUpViewModelBase{T, TM}"/> is visible.
        /// </summary>
        /// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
        [Required(ErrorMessage = "Visible is required")]
        [DisplayName("Visible in Drop Down")]
        [Display(Order = 999)]
        public bool Visible { get; set; }

        /// <summary>
        /// Updates the data object.
        /// </summary>
        /// <param name="dto">The details.</param>
        public override void UpdateDataObject(TM dto)
        {
            base.UpdateDataObject(dto);
            dto.Visible = this.Visible;
            dto.DisplayOrder = this.DisplayOrder;
        }

        /// <summary>
        /// Converts the specified filter ListView model.
        /// </summary>
        /// <param name="filterListViewModel">The filter ListView model.</param>
        /// <returns> Return id.</returns>
        protected int? Convert(FilterListViewModel filterListViewModel)
        {
            int? retVal = filterListViewModel?.Id;
           
            return retVal;
        }
    }
}
