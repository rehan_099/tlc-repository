﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaAclListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the ACL History View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    /// <summary>
    /// The ACL History View Model.
    /// </summary>
    public class CataleyaAclListViewModel : CataleyaViewModelBase
    {
        /// <summary>
        /// Gets or sets the type of the application.
        /// </summary>
        /// <value>The type of the application.</value>
        public string AppType { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the ipinterface identifier.
        /// </summary>
        /// <value>The ipinterface identifier.</value>
        public int IpinterfaceId { get; set; }

        /// <summary>
        /// Gets or sets the local port.
        /// </summary>
        /// <value>The local port.</value>
        public int LocalPort { get; set; }

        /// <summary>
        /// Gets or sets the remote ip address.
        /// </summary>
        /// <value>The remote ip address.</value>
        public string RemoteIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the remote prefix.
        /// </summary>
        /// <value>The remote prefix.</value>
        public int RemotePrefix { get; set; }

        /// <summary>
        /// Gets or sets the remote port.
        /// </summary>
        /// <value>The remote port.</value>
        public int RemotePort { get; set; }

        /// <summary>
        /// Gets or sets the transport.
        /// </summary>
        /// <value>The transport.</value>
        public string Transport { get; set; }

        /// <summary>
        /// Gets or sets the ver.
        /// </summary>
        /// <value>The ver.</value>
        public int Ver { get; set; }

        /// <summary>
        /// Gets or sets the IP interface compound key identifier.
        /// </summary>
        /// <value>The ip interface compound key identifier.</value>
        public int IpInterfaceCompoundKeyId { get; set; }

        /// <summary>
        /// Gets or sets the IP interface compound key node identifier.
        /// </summary>
        /// <value>The ip interface compound key node identifier.</value>
        public int IpInterfaceCompoundKeyNodeId { get; set; }

        /// <summary>
        /// Gets or sets the IP interface identifier.
        /// </summary>
        /// <value>The ip interface identifier.</value>
        public int IpInterfaceId { get; set; }

        /// <summary>
        /// Gets or sets the type of the ip interface address.
        /// </summary>
        /// <value>The type of the ip interface address.</value>
        public string IpInterfaceAddressType { get; set; }

        /// <summary>
        /// Gets or sets the ip interface ip address.
        /// </summary>
        /// <value>The ip interface ip address.</value>
        public string IpInterfaceIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the ip interface ip subnet identifier.
        /// </summary>
        /// <value>The ip interface ip subnet identifier.</value>
        public int IpInterfaceIpSubnetId { get; set; }

        /// <summary>
        /// Gets or sets the ip interface node identifier.
        /// </summary>
        /// <value>The ip interface node identifier.</value>
        public int IpInterfaceNodeId { get; set; }

        /// <summary>
        /// Gets or sets the ip interface operator identifier.
        /// </summary>
        /// <value>The ip interface operator identifier.</value>
        public int? IpInterfaceOperatorId { get; set; }

        /// <summary>
        /// Gets or sets the ip interface ver.
        /// </summary>
        /// <value>The ip interface ver.</value>
        public int IpInterfaceVer { get; set; }

        /// <summary>
        /// Gets or sets the ip interface vlan identifier.
        /// </summary>
        /// <value>The ip interface vlan identifier.</value>
        public int? IpInterfaceVlanId { get; set; }

        /// <summary>
        /// Gets or sets the name of the ip interface vlan.
        /// </summary>
        /// <value>The name of the ip interface vlan.</value>
        public string IpInterfaceVlanName { get; set; }
        
    }
}
