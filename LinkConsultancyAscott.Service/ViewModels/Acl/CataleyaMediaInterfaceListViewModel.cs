﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaMediaInterfaceListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the CataleyaMediaInterfaceListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    /// <summary>
    /// The CataleyaMediaInterfaceListViewModel.
    /// </summary>
    public class CataleyaMediaInterfaceListViewModel : CataleyaInterfaceViewModelBase
    {
        /// <summary>
        /// Gets or sets the start port.
        /// </summary>
        /// <value>The start port.</value>
        public int StartPort { get; set; }

        /// <summary>
        /// Gets or sets the end port.
        /// </summary>
        /// <value>The end port.</value>
        public int EndPort { get; set; }

        /// <summary>
        /// Gets or sets the bandwidth.
        /// </summary>
        /// <value>The bandwidth.</value>
        public int Bandwidth { get; set; }

        /// <summary>
        /// Gets or sets the bandwidth reserved.
        /// </summary>
        /// <value>The bandwidth reserved.</value>
        public string BandwidthReserved { get; set; }
    }
}
