﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AclListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the Acl List View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
   
    using LinkConsultancyAscott.Data;
    using LinkConsultancyAscott.Service.Extensions;

    /// <summary>
    /// The Acl List View Model.
    /// </summary>
    public class AclListViewModel : AuditableViewModelBase<int, Acl>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AclListViewModel"/> class.
        /// </summary>
        public AclListViewModel()
        {
            this.AclListType = new FilterListViewModel();
            this.AclEntityType = new FilterListViewModel();
            this.AclSourceType = new FilterListViewModel();
            this.Interface = new InterfaceFilterListViewModel();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AclListViewModel"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        public AclListViewModel(Acl details)
            : base(details)
        {
            this.Name = details.Name;
            this.Prefix = details.Prefix;
            this.AclEntityType = new FilterListViewModel
            {
                Id = details.AclEntityTypeId,
                Name = details.AclEntityType.Name
            };

            this.AclListType = new FilterListViewModel
            {
                Id = details.AclListTypeId,
                Name = details.AclListType.Name
            };

            this.AclSourceType = new FilterListViewModel
            {
                Id = details.AclSourceTypeId,
                Name = details.AclSourceType.Name
            };

            this.Interface = new InterfaceFilterListViewModel
            {
                Id = details.InterfaceId,
                Name = details.InterfaceName,
                InterfaceType = (Models.InterfaceType)details.InterfaceTypeId,
                InterfaceTypeName = ((Models.InterfaceType)details.InterfaceTypeId).GetDescription()
            };

            this.CataleyaSecurityAclNodeId = details.CataleyaSecurityAclNodeId;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Required]
        [RegularExpression(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", ErrorMessage = "IP Address must be a valid format")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        /// <value>The prefix.</value>
        [Required]
        [UIHint("Integer")]
        public int Prefix { get; set; }

        /// <summary>
        /// Gets or sets the type of the acl list.
        /// </summary>
        /// <value>The type of the acl list.</value>
        [DisplayName("Acl List Type")]
        [Display(Order = 700)]
        [UIHint("AclListTypeEditor")]
        [Required]
        public FilterListViewModel AclListType { get; set; }

        /// <summary>
        /// Gets or sets the type of the acl entity.
        /// </summary>
        /// <value>The type of the acl entity.</value>
        [DisplayName("Acl Entity Type")]
        [Display(Order = 700)]
        [UIHint("AclEntityTypeEditor")]
        [Required]
        public FilterListViewModel AclEntityType { get; set; }


        /// <summary>
        /// Gets or sets the type of the acl source.
        /// </summary>
        /// <value>The type of the acl source.</value>
        [DisplayName("Acl Source Type")]
        [Display(Order = 700)]
        [UIHint("AclSourceTypeEditor")]
        [Required]
        public FilterListViewModel AclSourceType { get; set; }


        [DisplayName("Interface")]
        [Display(Order = 700)]
        [UIHint("InterfaceEditor")]
        public InterfaceFilterListViewModel Interface { get; set; }

        public int? CataleyaSecurityAclNodeId { get; set; }

        /// <summary>
        /// Sets the data object.
        /// </summary>
        /// <value>The data object.</value>
        public override Acl DataObject
        {
            set
            {
                base.DataObject = value;

                this.Name = this.details.Name;
                this.Prefix = this.details.Prefix;
                this.AclEntityType = new FilterListViewModel
                {
                    Id = this.details.AclEntityTypeId,
                    Name = this.details.AclEntityType.Name
                };

                this.AclListType = new FilterListViewModel
                {
                    Id = this.details.AclListTypeId,
                    Name = this.details.AclListType.Name
                };


                this.AclSourceType = new FilterListViewModel
                {
                    Id = this.details.AclSourceTypeId,
                    Name = this.details.AclSourceType.Name
                };

                this.Interface = new InterfaceFilterListViewModel
                {
                    Id = this.details.InterfaceId,
                    Name = this.details.InterfaceName,
                    InterfaceType = (Models.InterfaceType)this.details.InterfaceTypeId,
                    InterfaceTypeName = ((Models.InterfaceType)this.details.InterfaceTypeId).GetDescription()
                };

                this.CataleyaSecurityAclNodeId = this.details.CataleyaSecurityAclNodeId;
            }
        }

        /// <summary>
        /// Updates the data object.
        /// </summary>
        /// <param name="dto">The details.</param>
        public override void UpdateDataObject(Acl dto)
        {
            base.UpdateDataObject(dto);
            dto.Name = this.Name;
            dto.Prefix = this.Prefix;
            dto.AclEntityTypeId = this.AclEntityType.Id;
            dto.AclListTypeId = this.AclListType.Id;
            dto.AclSourceTypeId = this.AclSourceType.Id;
            dto.InterfaceId = this.Interface.Id;
            dto.InterfaceName = this.Interface.Name;
            dto.InterfaceTypeId = (int)this.Interface.InterfaceType;
        }
    }
}
