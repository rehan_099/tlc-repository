﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaViewModelBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the CataleyaViewModelBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    /// <summary>
    /// The CataleyaViewModelBase.
    /// </summary>
    public abstract class CataleyaViewModelBase
    {
        /// <summary>
        /// Gets or sets the compound key identifier.
        /// </summary>
        /// <value>The compound key identifier.</value>
        public int CompoundKeyId { get; set; }

        /// <summary>
        /// Gets or sets the compound key node identifier.
        /// </summary>
        /// <value>The compound key node identifier.</value>
        public int CompoundKeyNodeId { get; set; }

        /// <summary>
        /// Gets or sets the node identifier.
        /// </summary>
        /// <value>The node identifier.</value>
        public int NodeId { get; set; }

        /// <summary>
        /// Gets or sets the operator identifier.
        /// </summary>
        /// <value>The operator identifier.</value>
        public int? OperatorId { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
    }
}
