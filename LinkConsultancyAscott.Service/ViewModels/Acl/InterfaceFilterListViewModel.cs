﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilterListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The filter list view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    /// <summary>
    /// The filter list view model.
    /// </summary>
    public class InterfaceFilterListViewModel : FilterListViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Models.InterfaceType InterfaceType { get; set; }

        /// <summary>
        /// Gets or sets the name of the interface type.
        /// </summary>
        /// <value>The name of the interface type.</value>
        public string InterfaceTypeName { get; set; }
    }
}