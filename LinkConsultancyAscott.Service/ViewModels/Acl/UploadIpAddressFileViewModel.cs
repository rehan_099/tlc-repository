﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UploadIpAddressFileViewModel.cs" company="Airbus Defence & Space">
//   Airbus Defence & Space
// </copyright>
// <summary>
//   Defines the UploadIpAddressFileViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Acl
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;
    using System.Web;

    using LinkConsultancyAscott.Service.Models;

    /// <summary>
    /// The UploadIpAddressFileViewModel.
    /// </summary>
    public class UploadIpAddressFileViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the type of the list.
        /// </summary>
        /// <value>The type of the list.</value>
        public AclListType ListType { get; set; }

        /// <summary>
        /// Gets or sets the name of the list type.
        /// </summary>
        /// <value>The name of the list type.</value>
        public string ListTypeName { get; set; }

        /// <summary>
        /// Gets or sets the Documents.
        /// </summary>
        [Display(Order = 160)]
        public IEnumerable<HttpPostedFileBase> IpAddressFiles { get; set; }
    }
}
