﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Log View Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    /// <summary>
    /// The Log View Model.
    /// </summary>
    public class LogViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public System.DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the thread.
        /// </summary>
        public string Thread { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        public string Logger { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the exception.
        /// </summary>
        public string Exception { get; set; }
    }
}
