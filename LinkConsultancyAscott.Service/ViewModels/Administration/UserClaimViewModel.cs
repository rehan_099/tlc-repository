﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserClaimViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the User View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// The User view model.
    /// </summary>
    public class UserClaimViewModel : ViewModelBase
    { 
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string ClaimType { get; set; }

        /// <summary>
        /// Gets or sets the claim value.
        /// </summary>
        /// <value>The claim value.</value>
        public string ClaimValue { get; set; }
    }
}
