﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteActivityLogViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Site Activity Log View Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System;

    /// <summary>
    /// The Log View Model.
    /// </summary>
    public class SiteActivityLogViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the activity timestamp.
        /// </summary>
        public System.DateTime ActivityTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the controller.
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the area.
        /// </summary>
        /// <value>The area.</value>
        public string Area { get; set; }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the browser.
        /// </summary>
        public string Browser { get; set; }

        /// <summary>
        /// Gets or sets the action duration.
        /// </summary>
        public Nullable<TimeSpan> ActionDuration { get; set; }

        /// <summary>
        /// Gets or sets the route info.
        /// </summary>
        public string RouteInfo { get; set; }

        /// <summary>
        /// Gets or sets the is mobile device.
        /// </summary>
        public Nullable<bool> IsMobileDevice { get; set; }

        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets the HTTP method.
        /// </summary>
        /// <value>The HTTP method.</value>
        public string HttpMethod { get; set; }

        /// <summary>
        /// Gets or sets the response status description.
        /// </summary>
        /// <value>The response status description.</value>
        public string ResponseStatusDescription { get; set; }

        /// <summary>
        /// Gets or sets the response data.
        /// </summary>
        /// <value>The response data.</value>
        public string ResponseData { get; set; }
    }
}
