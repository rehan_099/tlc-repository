﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the User View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// The User view model.
    /// </summary>
    public class UserViewModel : ViewModelBase
    {
        public UserViewModel()
        {
            this.SipInterfaces = new List<FilterListViewModel>();
            this.MediaInterfaces = new List<FilterListViewModel>();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the last activity date.
        /// </summary>
        /// <value>The last activity date.</value>
        [HiddenInput(DisplayValue = false)]
        public System.DateTime? LastActivityDate { get; set; }

        /// <summary>
        /// Gets or sets the user roles.
        /// </summary>
        /// <value>The user roles.</value>
        [UIHint("RolesEditor")]
        public IEnumerable<UserRoleViewModel> UserRoles { get; set; }

        /// <summary>
        /// Gets or sets the sip interfaces.
        /// </summary>
        /// <value>The sip interfaces.</value>
        [UIHint("SipInterfacesEditor")]
        public IEnumerable<FilterListViewModel> SipInterfaces { get; set; }

        /// <summary>
        /// Gets or sets the media interfaces.
        /// </summary>
        /// <value>The media interfaces.</value>
        [UIHint("MediaInterfacesEditor")]
        public IEnumerable<FilterListViewModel> MediaInterfaces { get; set; }
    }
}
