﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTemplateViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Defines the EmailTemplate View Model type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels.Administration
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The EmailTemplate View Model.
    /// </summary>
    public class EmailTemplateViewModel : LookUpViewModelBase<int, EmailTemplate>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTemplateViewModel"/> class.
        /// </summary>
        public EmailTemplateViewModel()
        {
            this.EmailText = string.Empty;
            this.EmailSubject = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTemplateViewModel"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        public EmailTemplateViewModel(EmailTemplate details)
            : base(details)
        {
            this.EmailText = details.EmailText;
            this.EmailSubject = details.EmailSubject;
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        [DisplayName("Type")]
        [Display(Order = 90)]
        [UIHint("EmailTypeEditor")]
        [Required]
        public FilterListViewModel EmailType { get; set; }

        /// <summary>
        /// Gets or sets the name of the icon.
        /// </summary>
        /// <value>The name of the icon.</value>
        [DisplayName("Email Subject")]
        [Display(Order = 800)]
        [AllowHtml]
        public string EmailSubject { get; set; }

        /// <summary>
        /// Gets or sets the name of the icon.
        /// </summary>
        /// <value>The name of the icon.</value>
        [DisplayName("Email Text")]
        [Display(Order = 900)]
        [UIHint("Editor")]
        [AllowHtml]
        public string EmailText { get; set; }

        /// <summary>
        /// Sets the data object.
        /// </summary>
        /// <value>The data object.</value>
        public override EmailTemplate DataObject
        {
            set
            {
                base.DataObject = value;
                this.EmailSubject = this.details.EmailSubject;
                this.EmailText = this.details.EmailText;
                this.EmailType = new FilterListViewModel
                       { 
                            Id = this.details.EmailTypeId, 
                            Name = this.details.EmailType.Name
                       };
            }
        }

        /// <summary>
        /// Updates the data object.
        /// </summary>
        /// <param name="dto">The details.</param>
        public override void UpdateDataObject(EmailTemplate dto)
        {
            base.UpdateDataObject(dto);
            dto.EmailText = this.EmailText;
            dto.EmailSubject = this.EmailSubject;
            dto.EmailTypeId = this.EmailType.Id;
        }
    }
}
