﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewModelBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The ViewModelBase.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The abstract view model.
    /// </summary>
    public abstract class ViewModelBase
    {
        /// <summary>
        /// The object to dictionary.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The Dictionary of properties.
        /// </returns>
        public static Dictionary<string, string> ObjectToDictionary(object value)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            if (value != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(value))
                {
                    if (descriptor?.Name != null)
                    {
                        object propValue = descriptor.GetValue(value);
                        if (propValue != null)
                        {
                            dictionary.Add(descriptor.Name, $"{propValue}");
                        }
                    }
                }
            }

            return dictionary;
        }

        /// <summary>
        /// The get property values.
        /// </summary>
        /// <returns>
        /// The List of Properties.
        /// </returns>
        public IEnumerable<string> GetPropertyValues()
        {
                return (from PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this) 
                        let name = descriptor.Name 
                        let type = descriptor.PropertyType.ToString() 
                        let value = descriptor.GetValue(this) 
                        select $"Prop [{name}] Type [{type}] Value [{value}]").ToList();
        }

        /// <summary>
        /// The get property values string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPropertyValuesString()
        {
            IEnumerable<string> props = this.GetPropertyValues();

            StringBuilder sb = new StringBuilder();
            foreach (var prop in props)
            {
                sb.AppendLine(prop);
            }

            return sb.ToString();
        }
    }
}
