﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlertStatus.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   Enum Alert Status Type
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System.ComponentModel;

    /// <summary>
    /// Alert Status Type
    /// </summary>
    public enum AlertStatus
    {
        /// <summary>
        /// The success
        /// </summary>
        [Description("alert-success")]
        Success = 1,

        /// <summary>
        /// The information
        /// </summary>
        [Description("alert-info")]
        Info = 2,

        /// <summary>
        /// The warning
        /// </summary>
        [Description("alert-warning")]
        Warning = 3,

        /// <summary>
        /// The danger
        /// </summary>
        [Description("alert-danger")]
        Danger = 4
    }
}
