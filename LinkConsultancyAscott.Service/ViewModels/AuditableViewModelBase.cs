﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditableViewModelBase.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott
// </copyright>
// <summary>
//   The Auditable View Model Base class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using Newtonsoft.Json;

    using LinkConsultancyAscott.Data;

    /// <summary>
    /// The Auditable View Model Base class.
    /// </summary>
    /// <typeparam name="T"> Data Model Type </typeparam>
    /// <typeparam name="TM">The overall type</typeparam>
    /// <seealso cref="LinkConsultancyAscott.Service.ViewModels.ViewModelBase" />
    /// <seealso cref="LinkConsultancyAscott.Service.ViewModels.IAuditableViewModel{T}" />
    public abstract class AuditableViewModelBase<T, TM> : ViewModelBase, IAuditableViewModel<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        where TM : IAuditedEntity<T>
    {
        /// <summary>
        /// The _details.
        /// </summary>
        protected TM details;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditableViewModelBase{T, TM}"/> class.
        /// </summary>
        protected AuditableViewModelBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditableViewModelBase{T, TM}"/> class.
        /// </summary>
        /// <param name="details">The details.</param>
        protected AuditableViewModelBase(TM details)
        {
            this.details = details;
            this.Id = this.details.Id;

            this.CreatedBy = this.details.CreatedBy;
            this.CreatedAt = this.details.CreatedAt;
            this.LastModifiedBy = this.details.LastModifiedBy;
            this.LastModifiedAt = this.details.LastModifiedAt;
            this.DeletedBy = this.details.DeletedBy;
            this.DeletedAt = this.details.DeletedAt;
        }

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>The details.</value>
        [JsonIgnore]
        public TM Details
        {
            protected get
            {
                return this.details;
            }

            set
            {
                this.details = value;
            }
        }

        /// <summary>
        /// Sets the data object.
        /// </summary>
        /// <value>The data object.</value>
        [JsonIgnore]
        public virtual TM DataObject
        {
            set
            {
                this.details = value;
                this.Id = this.details.Id;
                this.CreatedBy = this.details.CreatedBy;
                this.CreatedAt = this.details.CreatedAt;
                this.LastModifiedBy = this.details.LastModifiedBy;
                this.LastModifiedAt = this.details.LastModifiedAt;
                this.DeletedBy = this.details.DeletedBy;
                this.DeletedAt = this.details.DeletedAt;
            }
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [HiddenInput(DisplayValue = false)]
        [Display(Order = 10)]
        public T Id { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>The created by.</value>
        [HiddenInput(DisplayValue = false)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created at.
        /// </summary>
        /// <value>The created at.</value>
        [HiddenInput(DisplayValue = false)]
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the last modified by.
        /// </summary>
        /// <value>The last modified by.</value>
        [HiddenInput(DisplayValue = false)]
        [ScaffoldColumn(false)]
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the last modified at.
        /// </summary>
        /// <value>The last modified at.</value>
        [HiddenInput(DisplayValue = false)]
        [ScaffoldColumn(false)]
        public DateTime? LastModifiedAt { get; set; }

        /// <summary>
        /// Gets or sets the deleted by.
        /// </summary>
        /// <value>The deleted by.</value>
        [HiddenInput(DisplayValue = false)]
        [ScaffoldColumn(false)]
        public string DeletedBy { get; set; }

        /// <summary>
        /// Gets or sets the deleted at.
        /// </summary>
        /// <value>The deleted at.</value>
        [HiddenInput(DisplayValue = false)]
        [ScaffoldColumn(false)]
        public DateTime? DeletedAt { get; set; }

        /// <summary>
        /// Updates the data object.
        /// </summary>
        /// <param name="dto">The DTO.</param>
        public virtual void UpdateDataObject(TM dto)
        {
            dto.Id = this.Id;
            dto.CreatedBy = this.CreatedBy;
            dto.CreatedAt = this.CreatedAt;
            dto.LastModifiedBy = this.LastModifiedBy;
            dto.LastModifiedAt = this.LastModifiedAt;
            dto.DeletedBy = this.DeletedBy;
            dto.DeletedAt = this.DeletedAt;
        }
    }
}
