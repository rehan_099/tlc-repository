﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilterListViewModel.cs" company="The Link Consultancy - Ascott">
//   The Link Consultancy - Ascott 2018
// </copyright>
// <summary>
//   The filter list view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Service.ViewModels
{
    using System;

    /// <summary>
    /// The filter list view model.
    /// </summary>
    public class FilterListViewModel : IComparable<FilterListViewModel>
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="other">The object.</param>
        /// <returns>System Int32.</returns>
        public int CompareTo(FilterListViewModel other)
        {
            return string.Compare(this.Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
        }
        
    }
}