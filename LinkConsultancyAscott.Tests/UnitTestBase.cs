﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitTestBase.cs" company="">
//   
// </copyright>
// <summary>
//   Class UnitTestBase.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Class UnitTestBase.
    /// </summary>
    public abstract class UnitTestBase
    {
        /// <summary>
        /// Gets the embedded stream.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns>MemoryStream.</returns>
        protected MemoryStream GetEmbeddedStream(string resourceId)
        {
            var asm = Assembly.GetExecutingAssembly();

            //// Schema is embedded resource in our assembly.
            var res = asm.GetManifestResourceNames();

            var containsResult = res.Contains(resourceId, StringComparer.InvariantCultureIgnoreCase);

            if (!containsResult)
            {
                throw new InvalidOperationException();
            }

            MemoryStream data = null;
            using (var stream = asm.GetManifestResourceStream(resourceId))
            {
                if (stream != null)
                {
                    data = new MemoryStream();
                    stream.CopyTo(data);
                }
            }

            return data;
        }

    }
}
