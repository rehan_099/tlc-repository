﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SbcWebApiTests.cs" company="">
//   
// </copyright>
// <summary>
//   Class SbcWebApiTests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Tests.Integration
{

    using System.Configuration;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl;
    using LinkConsultancyAscott.Service.Models.Cataleya.Token;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Newtonsoft.Json;

    using Should;

    /// <summary>
    /// Class SbcWebApiTests.
    /// </summary>
    [TestClass]
    public class SbcWebApiTests
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
     /*   [TestMethod]
        public void Index()
        {
            // Arrange
            var equipmentWebApiUrl = "https://sbc.tlchostinguk.com:8443/cataleya/configuration/security_acl/get";

            // Act
            var response = this.GetAsync(equipmentWebApiUrl).Result;
            dynamic result;

            if (response.IsSuccessStatusCode)
            {
                // Get the response
                // var customerJsonString = await response.Content.ReadAsStringAsync();
                var customerJsonString = response.Content.ReadAsStringAsync().Result;


                // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                result = JsonConvert.DeserializeObject(customerJsonString);
            }

            // Assert
            response.ShouldBeNull();
        }*/

        [TestMethod, Ignore]
        public void TestGetToken()
        {
            var cataleyaUsername = ConfigurationManager.AppSettings["cataleyaUsername"];
            var cataleyaPassword = ConfigurationManager.AppSettings["cataleyaPassword"];
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                // Arrange
                var equipmentWebApiUrl =
                    "https://sbc.tlchostinguk.com:8443/cataleya/oauth/token?grant_type=password&client_id=restapp&client_secret=restapp&username=" + cataleyaUsername + "&password=" + cataleyaPassword;

                // Act
                var response = this.PostAsync(equipmentWebApiUrl).Result;
                Authentication result;

                if (response.IsSuccessStatusCode)
                {
                    // Get the response
                    // var customerJsonString = await response.Content.ReadAsStringAsync();
                    var customerJsonString = response.Content.ReadAsStringAsync().Result;


                    // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                    result = JsonConvert.DeserializeObject<Authentication>(customerJsonString);
                    result.ShouldNotBeNull();

                    // 
                    var getWebApiUrl =
                        "https://sbc.tlchostinguk.com:8443/cataleya/api/configuration/security_acl/get?access_token="
                        + result.access_token;
                    var getResponse = this.GetAsync(getWebApiUrl).Result;

                    if (getResponse.IsSuccessStatusCode)
                    {
                        // Get the response
                        // var customerJsonString = await response.Content.ReadAsStringAsync();
                        var getJsonString = getResponse.Content.ReadAsStringAsync().Result;


                        // Deserialise the data (include the Newtonsoft JSON Nuget package if you don't already have it)
                        SecurityAcl getResult = JsonConvert.DeserializeObject<SecurityAcl>(getJsonString);
                        getResult.ShouldNotBeNull();
                    }


                // Assert
                response.ShouldNotBeNull();
            }
        }

        protected async Task<HttpResponseMessage> PostAsync(string requestString)
        {
            using (var client = this.GetHttpClient())
            {
                var result = await client.PostAsync(requestString, null);

                return result;
            }
        }

        protected async Task<HttpResponseMessage> GetAsync(string requestString)
        {
            using (var client = this.GetHttpClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestString);
                requestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                var result = await client.SendAsync(requestMessage);

                return result;
            }
        }

        private HttpClient GetHttpClient()
        {
            HttpClientHandler handler = new HttpClientHandler()
            {
                MaxConnectionsPerServer = 20,
                Proxy = null,
                UseDefaultCredentials = true
            };

            // Set the default connection limit
            ServicePointManager.DefaultConnectionLimit = 20;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            var client = new HttpClient(handler, true);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
