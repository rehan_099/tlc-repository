﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CataleyaJsonTests.cs" company="">
//   
// </copyright>
// <summary>
//   Class CataleyaJsonTests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinkConsultancyAscott.Tests.Services
{
    using System.IO;
    using System.Linq;

    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.SecurityAcl;
    using LinkConsultancyAscott.Service.Models.Cataleya.Configuration.Zone;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Newtonsoft.Json;

    using Should;

    /// <summary>
    /// Class CataleyaJsonTests.
    /// </summary>
    [TestClass]
    public class CataleyaJsonTests
    {
        /// <summary>
        /// Gets or sets the test context.
        /// </summary>
        /// <value>The test context.</value>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// Tests the get token.
        /// </summary>
        [DeploymentItem(@"TestFiles\CataleyaSecurityAccessControlList.json", "TestFiles")]
        [TestMethod]
        public void TestGetToken()
        {
            // Arrange
            var path = Path.Combine(
                this.TestContext.DeploymentDirectory, 
                "TestFiles",
                "CataleyaSecurityAccessControlList.json");
            File.Exists(path).ShouldBeTrue();
            SecurityAcl securityAcl;

            // Act
            using (var r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                securityAcl = JsonConvert.DeserializeObject<SecurityAcl>(json);
            }


            // Assert
            securityAcl.ShouldNotBeNull();
            securityAcl.total.ShouldEqual(36);
            securityAcl.data.Count.ShouldEqual(36);
            var firstDatum = securityAcl.data[0];
            firstDatum.id.ShouldEqual(138);
            firstDatum.operatorId.ShouldEqual(1);
            firstDatum.nodeId.ShouldEqual(1);
            firstDatum.ipInterface.operatorId.HasValue.ShouldBeFalse();
        }



        /// <summary>
        /// Tests the get token.
        /// </summary>
        [DeploymentItem(@"TestFiles\CataleyaZoneList.json", "TestFiles")]
        [TestMethod]
        public void CataleyaZoneListTest()
        {
            // Arrange
            var path = Path.Combine(
                this.TestContext.DeploymentDirectory,
                "TestFiles",
                "CataleyaZoneList.json");
            File.Exists(path).ShouldBeTrue();
            Zones zones;

            // Act
            using (var r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                zones = JsonConvert.DeserializeObject<Zones>(json);
            }


            // Assert
            zones.ShouldNotBeNull();
            zones.total.ShouldEqual(37);
            zones.data.Count.ShouldEqual(37);
            var publicAccessZones = zones.data.Where(t => t.zoneType.Equals("AccessPublic")).ToList();
            publicAccessZones.Count.ShouldEqual(4);
        }
    }
}
